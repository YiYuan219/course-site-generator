package tam.file;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonReader;
import djf.components.AppDataComponent;
import djf.components.AppFileComponent;
import static djf.settings.AppPropertyType.LOAD_ERROR_MESSAGE;
import static djf.settings.AppPropertyType.LOAD_ERROR_TITLE;
import static djf.settings.AppStartupConstants.EXPORT_TITLE;
import static djf.settings.AppStartupConstants.PATH_WORK;
import djf.ui.AppMessageDialogSingleton;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import javafx.collections.ObservableList;
import javafx.stage.DirectoryChooser;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObjectBuilder;
import javax.json.JsonWriter;
import javax.json.JsonWriterFactory;
import javax.json.stream.JsonGenerator;
import org.apache.commons.io.FileUtils;
import properties_manager.PropertiesManager;
import tam.CSGApp;
import tam.data.CSGData;
import tam.data.CourseDetail;
import tam.data.Team;
import tam.data.Recitation;
import tam.data.Schedule;
import tam.data.Student;
import tam.data.TAData;
import tam.data.TeachingAssistant;
import tam.workspace.TAWorkspace;

/**
 * This class serves as the file component for the TA manager app. It provides
 * all saving and loading services for the application.
 *
 * @author Richard McKenna
 */
public class TAFiles implements AppFileComponent {

    // THIS IS THE APP ITSELF
    CSGApp app;

    // THESE ARE USED FOR IDENTIFYING JSON TYPES
    static final String JSON_START_HOUR = "startHour";
    static final String JSON_END_HOUR = "endHour";
    static final String JSON_OFFICE_HOURS = "officeHours";
    static final String JSON_DAY = "day";
    static final String JSON_TIME = "time";
    static final String JSON_NAME = "name";
    static final String JSON_UNDERGRAD_TAS = "undergrad_tas";
    static final String JSON_GRAD_TAS="grad_tas";
    static final String JSON_EMAIL = "email";
    static final String JSON_CHECK_TA="check undergradta";

    //These are used for course detail data
    static final String JSON_SUBJECT = "subject";
    static final String JSON_NUMBER = "number";
    static final String JSON_SEMESTER = "semester";
    static final String JSON_YEAR = "year";
    static final String JSON_TITLE_COURSE = "title";
    static final String JSON_INSTRUCTOR_NAME = "instructorName";
    static final String JSON_INSTRUCTOR_HOME = "instructorHome";
    static final String JSON_EXPORT = "exportDir";
    static final String JSON_TEMPLATE="scrDir";
    static final String JSON_BANNER = "bannerSchoolImage";
    static final String JSON_LEFT = "leftFooterImage";
    static final String JSON_RIGHT = "rightFooterImage";
    static final String JSON_STYLESHEET = "stylesheet";
    static final String JSON_NAVBAR = "navbarTitle";
    static final String JSON_FILE = "fileName";
    static final String JSON_SCRIPT = "script";
    static final String JSON_USE = "use";
    static final String JSON_SITE = "sitepages";

    //These are used for recitation data
    static final String JSON_SECTION = "section";
    static final String JSON_INSTRUCTOR = "instructor";
    static final String JSON_DAYTIME = "day_time";
    static final String JSON_LOCATION = "location";
    static final String JSON_TA1 = "ta_1";
    static final String JSON_TA2 = "ta_2";
    static final String JSON_RECITATION = "recitations";

    //These are used for schedule data
    static final String JSON_TYPE_HOLIDAY = "holidays";
    static final String JSON_TYPE_LECTURE = "lectures";
    static final String JSON_TYPE_REFERENCE = "references";
    static final String JSON_TYPE_RECITATION = "recitations";
    static final String JSON_TYPE = "type";
    static final String JSON_TYPE_HW = "hws";
    static final String JSON_MONTH = "month";
    static final String JSON_DAY_SCHEDULE = "day";
    static final String JSON_DATE = "date";
    static final String JSON_TITLE = "title";
    static final String JSON_TOPIC = "topic";
    static final String JSON_LINK = "link";
    static final String JSON_CRITERIA = "criteria";
    static final String JSON_SCHEDULE = "schedule";
    static final String JSON_START_MONTH = "startingMondayMonth";
    static final String JSON_START_DAY = "startingMondayDay";
    static final String JSON_END_MONTH = "endingFridayMonth";
    static final String JSON_END_DAY = "endingFridayDay";
    static final String JSON_SCHEDULE_YEAR="year";

    //These are used for project data
    static final String JSON_NAME_PROJECT = "name";
    static final String JSON_COLOR = "color";
    static final String JSON_TEXT_COLOR = "text_color";
    static final String JSON_LINK_PROJECT = "link";
    static final String JSON_FIRST_NAME = "firstName";
    static final String JSON_LAST_NAME = "lastName";
    static final String JSON_TEAM = "team";
    static final String JSON_ROLE = "role";
    static final String JSON_BIG_TEAM = "teams";
    static final String JSON_BIG_STUDENT = "students";
    static final String JSON_EMPTY="";
    static final String JSON_PROJECT="projects";
    static final String JSON_WORK="work";
    static final String JSON_RED="red";
    static final String JSON_BLUE="blue";
    static final String JSON_GREEN="green";
   

    public TAFiles(CSGApp initApp) {
        app = initApp;
    }

    @Override
    public void loadData(AppDataComponent data, String filePath) throws IOException {
        // CLEAR THE OLD DATA OUT
        CSGData dataManager = (CSGData) data;    //
        TAWorkspace space = null;
        if(app!=null){
         space=(TAWorkspace)app.getWorkspaceComponent();}
            

        // LOAD THE JSON FILE WITH ALL THE DATA
        JsonObject json = loadJSONFile(filePath);

        // LOAD THE START AND END HOURS
        String startHour = json.getString(JSON_START_HOUR);
        String endHour = json.getString(JSON_END_HOUR);
        dataManager.getTaData().initHours(startHour, endHour);

        // NOW RELOAD THE WORKSPACE WITH THE LOADED DATA
        if (app != null) {
            app.getWorkspaceComponent().reloadWorkspace(app.getDataComponent());
        }

        //for the course info
        
        String subject = json.getString(JSON_SUBJECT);
        dataManager.getCdData().setSubject(subject);
        space.getSubjectBox().setValue(subject);
        String number = json.getString(JSON_NUMBER);
        dataManager.getCdData().setNumber(number);
        space.getNumberBox().setValue(number);
        dataManager.getCdData().setSemester(json.getString(JSON_SEMESTER));
        space.getSemesterBox().setValue(json.getString(JSON_SEMESTER));
        dataManager.getCdData().setYear(json.getString(JSON_YEAR));
        space.getYearBox().setValue(json.getString(JSON_YEAR));
        dataManager.getCdData().setTitle(json.getString(JSON_TITLE_COURSE));
        space.getTitleField().setText(json.getString(JSON_TITLE_COURSE));
        dataManager.getCdData().setInstructorName(json.getString(JSON_INSTRUCTOR_NAME));
        space.getInstructionNField().setText(json.getString(JSON_INSTRUCTOR_NAME));
        dataManager.getCdData().setInstructorHome(json.getString(JSON_INSTRUCTOR_HOME));
        space.getInstructionHField().setText(json.getString(JSON_INSTRUCTOR_HOME));
        
        dataManager.getCdData().setExportDir(json.getString(JSON_EXPORT));
        space.getDirLabel().setText(json.getString(JSON_EXPORT));
        dataManager.getCdData().setSrcDir(json.getString(JSON_TEMPLATE));
        space.getSelectTemple().setText(json.getString(JSON_TEMPLATE));
        dataManager.getCdData().setBanner(json.getString(JSON_BANNER));
        dataManager.getCdData().setLeft(json.getString(JSON_LEFT));
        dataManager.getCdData().setRight(json.getString(JSON_RIGHT));
        dataManager.getCdData().setStylesheet(json.getString(JSON_STYLESHEET));
        space.getStyleCombo().setValue(json.getString(JSON_STYLESHEET));
        
        space.getController().StyleSheetOption(json.getString(JSON_TEMPLATE));

        JsonArray jsonCourseDetailArray = json.getJsonArray(JSON_SITE);
        for (int i = 0; i < jsonCourseDetailArray.size(); i++) {
            JsonObject jsonCD = jsonCourseDetailArray.getJsonObject(i);
            Boolean use = jsonCD.getBoolean(JSON_USE);
            String navbar = jsonCD.getString(JSON_NAVBAR);
            String file = jsonCD.getString(JSON_FILE);
            String script = jsonCD.getString(JSON_SCRIPT);
            dataManager.getCdData().addCoursedetail(navbar, file, script, use);
        }

        // NOW LOAD ALL THE UNDERGRAD TAs
        JsonArray jsonTAArray = json.getJsonArray(JSON_UNDERGRAD_TAS);
        for (int i = 0; i < jsonTAArray.size(); i++) {
            JsonObject jsonTA = jsonTAArray.getJsonObject(i);
            String name = jsonTA.getString(JSON_NAME);
            String email = jsonTA.getString(JSON_EMAIL);
            Boolean b=jsonTA.getBoolean(JSON_CHECK_TA);
            dataManager.getTaData().addTA(name, email,b);
            
        }
        
        ObservableList<TeachingAssistant> teachingAssistants=dataManager.getTaData().getTeachingAssistants();
        for(TeachingAssistant tas:teachingAssistants){
            if(app!=null){
            space.getTeacher1().add(tas.getName());}
        }
        

        // AND THEN ALL THE OFFICE HOURS
        
        String st=json.getString(JSON_START_HOUR);
        int s=Integer.parseInt(st);
        dataManager.getTaData().setStartHour(s);
        String et=json.getString(JSON_END_HOUR);
        int e=Integer.parseInt(et);
        dataManager.getTaData().setEndHour(e);
        if(s>12){
            s=s-12;
            st=s+":00pm";
        }
        else{
            st=st+":00am";
        }
        if(e>12){
            e=e-12;
            et=e+":00pm";
        }
        else{
            et=et+":00pm";
        }
        if(app!=null){
        space.getComboBox1().setValue(st);
        space.getComboBox2().setValue(et);
        JsonArray jsonOfficeHoursArray = json.getJsonArray(JSON_OFFICE_HOURS);
        for (int i = 0; i < jsonOfficeHoursArray.size(); i++) {
            JsonObject jsonOfficeHours = jsonOfficeHoursArray.getJsonObject(i);
            String day = jsonOfficeHours.getString(JSON_DAY);
            String time = jsonOfficeHours.getString(JSON_TIME);
            String name = jsonOfficeHours.getString(JSON_NAME);
            dataManager.getTaData().addOfficeHoursReservation(day, time, name);
        }}
        
        
        //for the recitation data
        JsonArray recitationArray = json.getJsonArray(JSON_RECITATION);
        for (int i = 0; i < recitationArray.size(); i++) {
            JsonObject jsonRecit = recitationArray.getJsonObject(i);
            String section = jsonRecit.getString(JSON_SECTION);
            String instructor = jsonRecit.getString(JSON_INSTRUCTOR);
            String daytime = jsonRecit.getString(JSON_DAYTIME);
            String location = jsonRecit.getString(JSON_LOCATION);
            String ta1 = jsonRecit.getString(JSON_TA1);
            String ta2 = jsonRecit.getString(JSON_TA2);
            dataManager.getRdData().addRecitation(section, instructor, daytime, location, ta1, ta2);
        }

        //for the schedule data
        //for the schedule
        String year=json.getString(JSON_SCHEDULE_YEAR);
        String startM=json.getString(JSON_START_MONTH);
        String startD=json.getString(JSON_START_DAY);
        String m=startM+"/"+startD+"/"+year;
        dataManager.getSdData().setStartM(m);
        String endM=json.getString(JSON_END_MONTH);
        String endD=json.getString(JSON_END_DAY);
        String f=endM+"/"+endD+"/"+year;
        dataManager.getSdData().setEndF(f);
        DateTimeFormatter format=DateTimeFormatter.ofPattern("M/d/y");
        if(app!=null){
        space.getEfPciker().setValue(LocalDate.parse(f, format));
        space.getSmPicker().setValue(LocalDate.parse(m, format));}
        
        JsonArray scheduleArray = json.getJsonArray(JSON_SCHEDULE);
        for (int i = 0; i < scheduleArray.size(); i++) {
            JsonObject jsonSchedule = scheduleArray.getJsonObject(i);
            String type = jsonSchedule.getString(JSON_TYPE);
            String date = jsonSchedule.getString(JSON_DATE);
            String time = jsonSchedule.getString(JSON_TIME);
            String title = jsonSchedule.getString(JSON_TITLE);
            String topic = jsonSchedule.getString(JSON_TOPIC);
            String link = jsonSchedule.getString(JSON_LINK);
            String criteria = jsonSchedule.getString(JSON_CRITERIA);
            dataManager.getSdData().addSchedules(type, date, time, title, topic, link, criteria);
        }

        //for the project data
        JsonArray teamArray = json.getJsonArray(JSON_BIG_TEAM);
        for (int i = 0; i < teamArray.size(); i++) {
            JsonObject jsonSchedule = teamArray.getJsonObject(i);
            String name = jsonSchedule.getString(JSON_NAME_PROJECT);
            String color = jsonSchedule.getString(JSON_COLOR);
            String textcolor = jsonSchedule.getString(JSON_TEXT_COLOR);
            String link = jsonSchedule.getString(JSON_LINK);
            //String topic=jsonSchedule.getString(JSON_TOPIC);
            //String link=jsonSchedule.getString(JSON_LINK);
            //String criteria=jsonSchedule.getString(JSON_CRITERIA);
            dataManager.getPdData().addTeam(name, color, textcolor, link);
        }
        
        ObservableList<Team> teams=dataManager.getPdData().getTeams();
        for(Team ts:teams){
            if(app!=null){
            space.getTeamOption().add(ts.getName());}
        }

        JsonArray studentsArray = json.getJsonArray(JSON_BIG_STUDENT);
        for (int i = 0; i < studentsArray.size(); i++) {
            JsonObject jsonSchedule = studentsArray.getJsonObject(i);
            String firstname = jsonSchedule.getString(JSON_FIRST_NAME);
            String lastname = jsonSchedule.getString(JSON_LAST_NAME);
            String team = jsonSchedule.getString(JSON_TEAM);
            String role = jsonSchedule.getString(JSON_ROLE);
            //String topic=jsonSchedule.getString(JSON_TOPIC);
            //String link=jsonSchedule.getString(JSON_LINK);
            //String criteria=jsonSchedule.getString(JSON_CRITERIA);
            dataManager.getPdData().addStudent(firstname, lastname, team, role);
        }

    }

    // HELPER METHOD FOR LOADING DATA FROM A JSON FORMAT
    private JsonObject loadJSONFile(String jsonFilePath) throws IOException {
        InputStream is = new FileInputStream(jsonFilePath);
        JsonReader jsonReader = Json.createReader(is);
        JsonObject json = jsonReader.readObject();
        jsonReader.close();
        is.close();
        return json;
    }

    @Override
    public void saveData(AppDataComponent data, String filePath) throws IOException {
        // GET THE DATA
        CSGData dataManager = (CSGData) data;
        TAWorkspace workspace = (TAWorkspace)app.getWorkspaceComponent();

        //course detail data
        
        String title=workspace.getTitleField().getText();
        String instructorName=workspace.getInstructionNField().getText();
        String intructorHome=workspace.getInstructionHField().getText();
        dataManager.getCdData().setTitle(title);
        dataManager.getCdData().setInstructorName(instructorName);
        dataManager.getCdData().setInstructorHome(intructorHome);
        
        
        
        JsonArrayBuilder coursedetailArrayBuilder = Json.createArrayBuilder();
        ObservableList<CourseDetail> coursedetails = dataManager.getCdData().getCoursedetail();
        for (CourseDetail cd : coursedetails) {
            JsonObject taJson = Json.createObjectBuilder()
                    .add(JSON_USE, cd.getCheck())
                    .add(JSON_NAVBAR, cd.getNavbar())
                    .add(JSON_FILE, cd.getFilename())
                    .add(JSON_SCRIPT, cd.getScript())
                    .build();
            coursedetailArrayBuilder.add(taJson);
        }
        JsonArray coursedetailArray = coursedetailArrayBuilder.build();

        // teaching assistant data
        // NOW BUILD THE TA JSON OBJCTS TO SAVE
        JsonArrayBuilder taArrayBuilder = Json.createArrayBuilder();
        ObservableList<TeachingAssistant> tas = dataManager.getTaData().getTeachingAssistants();
        for (TeachingAssistant ta : tas) {
            JsonObject taJson = Json.createObjectBuilder()
                    .add(JSON_NAME, ta.getName())
                    .add(JSON_EMAIL, ta.getEmail())
                    .add(JSON_CHECK_TA, ta.getUndercheck())
                    .build();
            taArrayBuilder.add(taJson);
        }
        JsonArray undergradTAsArray = taArrayBuilder.build();

        // NOW BUILD THE TIME SLOT JSON OBJCTS TO SAVE
	JsonArrayBuilder timeSlotArrayBuilder = Json.createArrayBuilder();
	ArrayList<TimeSlot> officeHours = TimeSlot.buildOfficeHoursList(dataManager.getTaData());
	for (TimeSlot ts : officeHours) {	    
	    JsonObject tsJson = Json.createObjectBuilder()
		    .add(JSON_DAY, ts.getDay())
		    .add(JSON_TIME, ts.getTime())
		    .add(JSON_NAME, ts.getName()).build();
	    timeSlotArrayBuilder.add(tsJson);
	}
	JsonArray timeSlotsArray = timeSlotArrayBuilder.build();
        //for the recitation 
        JsonArrayBuilder reciationArrayBuilder = Json.createArrayBuilder();
        ObservableList<Recitation> recitations = dataManager.getRdData().getRecitations();
        for (Recitation recit : recitations) {
            JsonObject reJson = Json.createObjectBuilder()
                    .add(JSON_SECTION, recit.getSection())
                    .add(JSON_INSTRUCTOR, recit.getInstructor())
                    .add(JSON_DAYTIME, recit.getDaytime())
                    .add(JSON_LOCATION, recit.getLocation())
                    .add(JSON_TA1, recit.getTa1())
                    .add(JSON_TA2, recit.getTa2())
                    .build();
            reciationArrayBuilder.add(reJson);
        }
        JsonArray reciationArray = reciationArrayBuilder.build();

        //for the schedule
        JsonArrayBuilder scheduleArrayBuilder = Json.createArrayBuilder();
        ObservableList<Schedule> schedules = dataManager.getSdData().getSchedules();
        for (Schedule sch : schedules) {
            JsonObject reJson = Json.createObjectBuilder()
                    .add(JSON_TYPE, sch.getType())
                    .add(JSON_DATE, sch.getDate())
                    .add(JSON_TIME, sch.getTime())
                    .add(JSON_TITLE, sch.getTitle())
                    .add(JSON_TOPIC, sch.getTopic())
                    .add(JSON_LINK, sch.getLink())
                    .add(JSON_CRITERIA, sch.getCriteria())
                    .build();
            scheduleArrayBuilder.add(reJson);
        }
        JsonArray scheduleArray = scheduleArrayBuilder.build();

        //for the project
        JsonArrayBuilder teamArrayBuilder = Json.createArrayBuilder();
        ObservableList<Team> project1 = dataManager.getPdData().getTeams();
        for (Team pro : project1) {
            JsonObject proJson = Json.createObjectBuilder()
                    .add(JSON_NAME_PROJECT, pro.getName())
                    .add(JSON_COLOR, pro.getColor())
                    .add(JSON_TEXT_COLOR, pro.getTextcolor())
                    .add(JSON_LINK_PROJECT, pro.getLink())
                    .build();
            teamArrayBuilder.add(proJson);
        }
        JsonArray teamArray = teamArrayBuilder.build();

        JsonArrayBuilder studentsArrayBuilder = Json.createArrayBuilder();
        ObservableList<Student> project2 = dataManager.getPdData().getStudents();
        for (Student pro : project2) {
            JsonObject proJson = Json.createObjectBuilder()
                    .add(JSON_FIRST_NAME, pro.getFirstname())
                    .add(JSON_LAST_NAME, pro.getLastname())
                    .add(JSON_TEAM, pro.getTeam())
                    .add(JSON_ROLE, pro.getRole())
                    .build();
            studentsArrayBuilder.add(proJson);
        }
        JsonArray studentArray = studentsArrayBuilder.build();

        // THEN PUT IT ALL TOGETHER IN A JsonObject
        JsonObject dataManagerJSO = Json.createObjectBuilder()
                .add(JSON_SUBJECT, "" + dataManager.getCdData().getSubject())
                .add(JSON_NUMBER, "" + dataManager.getCdData().getNumber())
                .add(JSON_SEMESTER, "" + dataManager.getCdData().getSemester())
                .add(JSON_YEAR, "" + dataManager.getCdData().getYear())
                .add(JSON_TITLE_COURSE, "" + dataManager.getCdData().getTitle())
                .add(JSON_INSTRUCTOR_NAME, "" + dataManager.getCdData().getInstructorName())
                .add(JSON_INSTRUCTOR_HOME, "" + dataManager.getCdData().getInstructorHome())
                .add(JSON_EXPORT, "" + dataManager.getCdData().getExportDir())
                .add(JSON_TEMPLATE,""+dataManager.getCdData().getSrcDir())
                .add(JSON_BANNER, "" + dataManager.getCdData().getBanner())
                .add(JSON_LEFT, "" + dataManager.getCdData().getLeft())
                .add(JSON_RIGHT, "" + dataManager.getCdData().getRight())
                .add(JSON_STYLESHEET, "" + dataManager.getCdData().getStylesheet())
                .add(JSON_SITE, coursedetailArray)
                //for ta
                .add(JSON_START_HOUR, "" + dataManager.getTaData().getStartHour())
                .add(JSON_END_HOUR, "" + dataManager.getTaData().getEndHour())
                .add(JSON_UNDERGRAD_TAS, undergradTAsArray)
                .add(JSON_OFFICE_HOURS, timeSlotsArray)
                //for recitation
                .add(JSON_RECITATION, reciationArray)
                //for schedule
                .add(JSON_START_MONTH, "" + dataManager.getSdData().getMonth(dataManager.getSdData().getStartM()))
                .add(JSON_START_DAY, "" + dataManager.getSdData().getDate(dataManager.getSdData().getStartM()))
                .add(JSON_END_MONTH, "" + dataManager.getSdData().getMonth(dataManager.getSdData().getEndF()))
                .add(JSON_END_DAY, "" + dataManager.getSdData().getDate(dataManager.getSdData().getEndF()))
                .add(JSON_SCHEDULE_YEAR,""+dataManager.getSdData().getYear(dataManager.getSdData().getEndF()))
                .add(JSON_SCHEDULE, scheduleArray)
                //for project       
                .add(JSON_BIG_TEAM, teamArray)
                .add(JSON_BIG_STUDENT, studentArray)
                .build();

        // AND NOW OUTPUT IT TO A JSON FILE WITH PRETTY PRINTING
        Map<String, Object> properties = new HashMap<>(1);
        properties.put(JsonGenerator.PRETTY_PRINTING, true);
        JsonWriterFactory writerFactory = Json.createWriterFactory(properties);
        StringWriter sw = new StringWriter();
        JsonWriter jsonWriter = writerFactory.createWriter(sw);
        jsonWriter.writeObject(dataManagerJSO);
        jsonWriter.close();

        // INIT THE WRITER
        OutputStream os = new FileOutputStream(filePath);
        JsonWriter jsonFileWriter = Json.createWriter(os);
        jsonFileWriter.writeObject(dataManagerJSO);
        String prettyPrinted = sw.toString();
        PrintWriter pw = new PrintWriter(filePath);
        pw.write(prettyPrinted);
        pw.close();

    }

    // IMPORTING/EXPORTING DATA IS USED WHEN WE READ/WRITE DATA IN AN
    // ADDITIONAL FORMAT USEFUL FOR ANOTHER PURPOSE, LIKE ANOTHER APPLICATION
    @Override
    public void importData(AppDataComponent data, String filePath) throws IOException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void exportData(AppDataComponent data, String path) throws IOException {
        CSGData d=(CSGData)app.getDataComponent();
        
        String filePath=d.getCdData().getSrcDir();
        System.out.println(filePath);
        
        String taPath=filePath+"\\js\\OfficeHoursGridData.json";
        System.out.println(taPath);
        String scPath=filePath+"\\js\\ScheduleData.json";
        String proPath=filePath+"\\js\\ProjectsData.json";
        String teamstuPath=filePath+"\\js\\TeamsAndStudents.json";
        String recitationPath=filePath+"\\js\\RecitationsData.json";
        String coursedetailPath=filePath+"\\js\\CourseDetailData.json";
        
        
        exportTaData(data,taPath);
        exportSchedleData(data,scPath);
        exportProjectData(data,proPath);
        exportTeamAndStudentData(data,teamstuPath);
        exportRecitationData(data,recitationPath);
        exportCourseDetailData(data,coursedetailPath);
        
        File srcFolder=new File(d.getCdData().getSrcDir());
        File selectedFile = new File(d.getCdData().getExportDir());
        
        FileUtils.copyDirectoryToDirectory(srcFolder,selectedFile); 
        
        
        
    }
    
    public void exportCourseDetailData(AppDataComponent data, String filePath) throws FileNotFoundException{
        CSGData dataManager = (CSGData) data;
        TAWorkspace workspace = (TAWorkspace)app.getWorkspaceComponent();
        
        JsonArrayBuilder coursedetailArrayBuilder = Json.createArrayBuilder();
        ObservableList<CourseDetail> coursedetails = dataManager.getCdData().getCoursedetail();
        for (CourseDetail cd : coursedetails) {
            JsonObject taJson = Json.createObjectBuilder()
                    .add(JSON_USE, cd.getCheck())
                    .add(JSON_NAVBAR, cd.getNavbar())
                    .add(JSON_FILE, cd.getFilename())
                    .add(JSON_SCRIPT, cd.getScript())
                    .build();
            coursedetailArrayBuilder.add(taJson);
        }
        JsonArray coursedetailArray = coursedetailArrayBuilder.build();
        
        String title=workspace.getTitleField().getText();
        String instructorName=workspace.getInstructionNField().getText();
        String intructorHome=workspace.getInstructionHField().getText();
        dataManager.getCdData().setTitle(title);
        dataManager.getCdData().setInstructorName(instructorName);
        dataManager.getCdData().setInstructorHome(intructorHome);
        
        JsonObject dataManagerJSO = Json.createObjectBuilder()
                .add(JSON_SUBJECT, "" + dataManager.getCdData().getSubject())
                .add(JSON_NUMBER, "" + dataManager.getCdData().getNumber())
                .add(JSON_SEMESTER, "" + dataManager.getCdData().getSemester())
                .add(JSON_YEAR, "" + dataManager.getCdData().getYear())
                .add(JSON_TITLE_COURSE, "" + dataManager.getCdData().getTitle())
                .add(JSON_INSTRUCTOR_NAME, "" + dataManager.getCdData().getInstructorName())
                .add(JSON_INSTRUCTOR_HOME, "" + dataManager.getCdData().getInstructorHome())
                .add(JSON_EXPORT, "" + dataManager.getCdData().getExportDir())
                .add(JSON_BANNER, "" + dataManager.getCdData().getBanner())
                .add(JSON_LEFT, "" + dataManager.getCdData().getLeft())
                .add(JSON_RIGHT, "" + dataManager.getCdData().getRight())
                .add(JSON_STYLESHEET, "" + dataManager.getCdData().getStylesheet())
                .add(JSON_SITE, coursedetailArray)
                .build();
        
        // AND NOW OUTPUT IT TO A JSON FILE WITH PRETTY PRINTING
        Map<String, Object> properties = new HashMap<>(1);
        properties.put(JsonGenerator.PRETTY_PRINTING, true);
        JsonWriterFactory writerFactory = Json.createWriterFactory(properties);
        StringWriter sw = new StringWriter();
        JsonWriter jsonWriter = writerFactory.createWriter(sw);
        jsonWriter.writeObject(dataManagerJSO);
        jsonWriter.close();

        // INIT THE WRITER
        OutputStream os = new FileOutputStream(filePath);
        JsonWriter jsonFileWriter = Json.createWriter(os);
        jsonFileWriter.writeObject(dataManagerJSO);
        String prettyPrinted = sw.toString();
        PrintWriter pw = new PrintWriter(filePath);
        pw.write(prettyPrinted);
        pw.close();
        
 
    }
    
    public void exportTaData(AppDataComponent data, String filePath) throws IOException{
        CSGData dataManager = (CSGData) data;
        //for undergrad ta
        JsonArrayBuilder undertaArrayBuilder = Json.createArrayBuilder();
        ObservableList<TeachingAssistant> undertas = dataManager.getTaData().getTeachingAssistants();
        for (TeachingAssistant ta : undertas) {
            System.out.println(ta.getUndercheck());
            if(ta.getUndercheck()){
            JsonObject taJson = Json.createObjectBuilder()
                    .add(JSON_NAME, ta.getName())
                    .add(JSON_EMAIL, ta.getEmail()).build();
            undertaArrayBuilder.add(taJson);
            }
        }
        JsonArray undergradTAsArray = undertaArrayBuilder.build();
        //for grad ta
        JsonArrayBuilder gradtaArrayBuilder = Json.createArrayBuilder();
        ObservableList<TeachingAssistant> gradtas = dataManager.getTaData().getTeachingAssistants();
        for (TeachingAssistant ta : gradtas) {
            if(!ta.getUndercheck()){
            JsonObject taJson = Json.createObjectBuilder()
                    .add(JSON_NAME, ta.getName())
                    .add(JSON_EMAIL, ta.getEmail()).build();
            gradtaArrayBuilder.add(taJson);
            }
        }
        JsonArray gradTAsArray = gradtaArrayBuilder.build();
        

        // NOW BUILD THE TIME SLOT JSON OBJCTS TO SAVE
	JsonArrayBuilder timeSlotArrayBuilder = Json.createArrayBuilder();
	ArrayList<TimeSlot> officeHours = TimeSlot.buildOfficeHoursList(dataManager.getTaData());
	for (TimeSlot ts : officeHours) {	    
	    JsonObject tsJson = Json.createObjectBuilder()
		    .add(JSON_DAY, ts.getDay())
		    .add(JSON_TIME, ts.getTime())
		    .add(JSON_NAME, ts.getName()).build();
	    timeSlotArrayBuilder.add(tsJson);
	}
	JsonArray timeSlotsArray = timeSlotArrayBuilder.build();
        
        //now put all things into a json object
        JsonObject dataManagerJSO = Json.createObjectBuilder()
                .add(JSON_START_HOUR, "" + dataManager.getTaData().getStartHour())
                .add(JSON_END_HOUR, "" + dataManager.getTaData().getEndHour())
                .add(JSON_UNDERGRAD_TAS, undergradTAsArray)
                .add(JSON_GRAD_TAS,gradTAsArray)
                .add(JSON_OFFICE_HOURS, timeSlotsArray)
                .build();
        
        // AND NOW OUTPUT IT TO A JSON FILE WITH PRETTY PRINTING
        Map<String, Object> properties = new HashMap<>(1);
        properties.put(JsonGenerator.PRETTY_PRINTING, true);
        JsonWriterFactory writerFactory = Json.createWriterFactory(properties);
        StringWriter sw = new StringWriter();
        JsonWriter jsonWriter = writerFactory.createWriter(sw);
        jsonWriter.writeObject(dataManagerJSO);
        jsonWriter.close();

        // INIT THE WRITER
        OutputStream os = new FileOutputStream(filePath);
        JsonWriter jsonFileWriter = Json.createWriter(os);
        jsonFileWriter.writeObject(dataManagerJSO);
        String prettyPrinted = sw.toString();
        PrintWriter pw = new PrintWriter(filePath);
        pw.write(prettyPrinted);
        pw.close();
        
        
    }

    public void exportSchedleData(AppDataComponent data, String filePath) throws IOException {
        CSGData dataManager = (CSGData) data;
        //for the schedule
        //for the scheduleholiday
        JsonArrayBuilder scheduleHolidayArrayBuilder = Json.createArrayBuilder();
        ObservableList<Schedule> schedulesholidays = dataManager.getSdData().getSchedules();
        for (Schedule sch : schedulesholidays) {
            if (sch.getType().equals("Holiday")) {
                System.out.println(sch.getTime());
                JsonObject scJson = Json.createObjectBuilder()
                        .add(JSON_MONTH, sch.getMonth(sch.getDate()))
                        .add(JSON_DAY, sch.getDay(sch.getDate()))
                        .add(JSON_TITLE, sch.getTitle())
                        .add(JSON_LINK, sch.getLink())
                        .build();
                scheduleHolidayArrayBuilder.add(scJson);
            }
        }
        JsonArray scheduleHolidayArray = scheduleHolidayArrayBuilder.build();

        //for the schedulelectre
        JsonArrayBuilder scheduleLectureArrayBuilder = Json.createArrayBuilder();
        ObservableList<Schedule> schedulelecture = dataManager.getSdData().getSchedules();
        for (Schedule sch : schedulelecture) {
            System.out.println(sch.getType());
            if (sch.getType().equals("Lecture")) {
                JsonObject scJson = Json.createObjectBuilder()
                        .add(JSON_MONTH, sch.getMonth(sch.getDate()))
                        .add(JSON_DAY, sch.getDay(sch.getDate()))
                        .add(JSON_TITLE, sch.getTitle())
                        .add(JSON_TOPIC,sch.getTopic())
                        .add(JSON_LINK, sch.getLink())
                        .build();
                scheduleLectureArrayBuilder.add(scJson);
            }
        }
        JsonArray scheduleLectuerArray = scheduleLectureArrayBuilder.build();

        //for the schedule references
        JsonArrayBuilder scheduleReferenceArrayBuilder = Json.createArrayBuilder();
        ObservableList<Schedule> schedulereference = dataManager.getSdData().getSchedules();
        for (Schedule sch : schedulereference) {
            if (sch.getType().equals("Reference")) {
                JsonObject scJson = Json.createObjectBuilder()
                        .add(JSON_MONTH, sch.getMonth(sch.getDate()))
                        .add(JSON_DAY, sch.getDay(sch.getDate()))
                        .add(JSON_TITLE, sch.getTitle())
                        .add(JSON_TOPIC, sch.getTopic())
                        .build();
                scheduleReferenceArrayBuilder.add(scJson);
            }
        }
        JsonArray scheduleReferenceArray = scheduleReferenceArrayBuilder.build();

        //for the scedule recitation
        JsonArrayBuilder scheduleRecitationArrayBuilder = Json.createArrayBuilder();
        ObservableList<Schedule> schedulerecitation = dataManager.getSdData().getSchedules();
        for (Schedule sch :schedulerecitation) {
            if(sch.getType().equals("Recitation")){
                JsonObject scJson = Json.createObjectBuilder()
                    .add(JSON_MONTH, sch.getMonth(sch.getDate()))
                    .add(JSON_DAY, sch.getDay(sch.getDate()))
                    .add(JSON_TITLE, sch.getTitle())
                    .add(JSON_TOPIC, sch.getTopic())
                    .add(JSON_LINK, sch.getLink())
                    .build();
                scheduleRecitationArrayBuilder.add(scJson);
            }
        }
        JsonArray scheduleRecitationArray = scheduleRecitationArrayBuilder.build();
        
        //for the schedule hw
        JsonArrayBuilder scheduleHwArrayBuilder = Json.createArrayBuilder();
        ObservableList<Schedule> schedulehw = dataManager.getSdData().getSchedules();
        for (Schedule sch :schedulehw) {
            if(sch.getType().equals("HW")){
                JsonObject scJson = Json.createObjectBuilder()
                    .add(JSON_MONTH, sch.getMonth(sch.getDate()))
                    .add(JSON_DAY, sch.getDay(sch.getDate()))
                    .add(JSON_TITLE, sch.getTitle())
                    .add(JSON_TOPIC, sch.getTopic())
                    .add(JSON_LINK, sch.getLink())
                    .add(JSON_TIME, sch.getTime())
                    .add(JSON_CRITERIA,sch.getCriteria())
                    .build();
                scheduleHwArrayBuilder.add(scJson);
            }
        }
        JsonArray scheduleHwArray = scheduleHwArrayBuilder.build();

        //now put all things into a json object
        JsonObject dataManagerJSO = Json.createObjectBuilder()
            .add(JSON_START_MONTH, "" + dataManager.getSdData().getMonth(dataManager.getSdData().getStartM()))
            .add(JSON_START_DAY, "" + dataManager.getSdData().getDate(dataManager.getSdData().getStartM()))
            .add(JSON_END_MONTH, "" + dataManager.getSdData().getMonth(dataManager.getSdData().getEndF()))
            .add(JSON_END_DAY, "" + dataManager.getSdData().getDate(dataManager.getSdData().getEndF()))
            .add(JSON_TYPE_HOLIDAY, scheduleHolidayArray)
            .add(JSON_TYPE_LECTURE, scheduleLectuerArray)
            .add(JSON_TYPE_REFERENCE, scheduleReferenceArray)
            .add(JSON_TYPE_RECITATION, scheduleRecitationArray)
            .add(JSON_TYPE_HW, scheduleHwArray)
            .build();
        
        
        // AND NOW OUTPUT IT TO A JSON FILE WITH PRETTY PRINTING
        Map<String, Object> properties = new HashMap<>(1);
        properties.put(JsonGenerator.PRETTY_PRINTING, true);
        JsonWriterFactory writerFactory = Json.createWriterFactory(properties);
        StringWriter sw = new StringWriter();
        JsonWriter jsonWriter = writerFactory.createWriter(sw);
        jsonWriter.writeObject(dataManagerJSO);
        jsonWriter.close();

        // INIT THE WRITER
        OutputStream os = new FileOutputStream(filePath);
        JsonWriter jsonFileWriter = Json.createWriter(os);
        jsonFileWriter.writeObject(dataManagerJSO);
        String prettyPrinted = sw.toString();
        PrintWriter pw = new PrintWriter(filePath);
        pw.write(prettyPrinted);
        pw.close();

    }
    public void exportProjectData(AppDataComponent data, String filePath) throws IOException{
        CSGData dataManager = (CSGData) data;
        //for the students array
        
        JsonArrayBuilder workArrayBuilder = Json.createArrayBuilder();
        ObservableList<Team> teams = dataManager.getPdData().getTeams();
        ObservableList<Student> students = dataManager.getPdData().getStudents();
        JsonArrayBuilder teamArrayBuilder = Json.createArrayBuilder();
        for (Team t : teams) {
            JsonArrayBuilder studentsArrayBuilder = Json.createArrayBuilder();
            
            ArrayList st=new ArrayList();
            for(Student s:students){
                if(s.getTeam().equals(t.getName())){
                        String name=s.getFirstname()+s.getLastname();
                        st.add(name);
                }  
            }
            String ob=st.toString();
            ob=ob.replace("[", "");
            ob=ob.replaceAll("]", "");
            //JsonObject scJson=Json.createObjectBuilder()
                            //.add(JSON_EMPTY,)
                            //.build();
                            
                        
            JsonArray studentsArray = studentsArrayBuilder.build();
            JsonObject scJson = Json.createObjectBuilder()
                    .add(JSON_NAME, t.getName())
                    .add(JSON_BIG_STUDENT,ob )
                    .add(JSON_LINK, t.getLink())
                    .build();
            teamArrayBuilder.add(scJson);
        }
        JsonArray teamArray = teamArrayBuilder.build();
        
        String h=dataManager.getCdData().getSemester()+" "+dataManager.getCdData().getYear();
        JsonObject scJson = Json.createObjectBuilder()
                    .add(JSON_SEMESTER,h)
                    .add(JSON_PROJECT,teamArray )
                    .build();
            workArrayBuilder.add(scJson);
        JsonArray workArray=workArrayBuilder.build();
        

        //now put all things into a json object
        JsonObject dataManagerJSO = Json.createObjectBuilder()
            .add(JSON_WORK, workArray)
            .build();
        
        
        // AND NOW OUTPUT IT TO A JSON FILE WITH PRETTY PRINTING
        Map<String, Object> properties = new HashMap<>(1);
        properties.put(JsonGenerator.PRETTY_PRINTING, true);
        JsonWriterFactory writerFactory = Json.createWriterFactory(properties);
        StringWriter sw = new StringWriter();
        JsonWriter jsonWriter = writerFactory.createWriter(sw);
        jsonWriter.writeObject(dataManagerJSO);
        jsonWriter.close();

        // INIT THE WRITER
        OutputStream os = new FileOutputStream(filePath);
        JsonWriter jsonFileWriter = Json.createWriter(os);
        jsonFileWriter.writeObject(dataManagerJSO);
        String prettyPrinted = sw.toString();
        PrintWriter pw = new PrintWriter(filePath);
        pw.write(prettyPrinted);
        pw.close();
        
    }
    public void exportTeamAndStudentData(AppDataComponent data, String filePath) throws IOException{
        CSGData dataManager = (CSGData) data;
        
        //for the team
        JsonArrayBuilder teamArrayBuilder = Json.createArrayBuilder();
        ObservableList<Team> teams = dataManager.getPdData().getTeams();
        
        for (Team t : teams) {
            String colortext="#"+t.getTextcolor();
            JsonObject scJson = Json.createObjectBuilder()
                    .add(JSON_NAME, t.getName())
                    .add(JSON_RED, t.getRed(t.getColor()))
                    .add(JSON_GREEN, t.getGreen(t.getColor()))
                    .add(JSON_BLUE, t.getBlue(t.getColor()))
                    .add(JSON_TEXT_COLOR,colortext)
                    .build();
            teamArrayBuilder.add(scJson);
            
        }
        JsonArray teamArray = teamArrayBuilder.build();
        
        JsonArrayBuilder studentArrayBuilder = Json.createArrayBuilder();
        ObservableList<Student> students = dataManager.getPdData().getStudents();
        for (Student s : students) {
            JsonObject scJson = Json.createObjectBuilder()
                    .add(JSON_LAST_NAME,s.getLastname())
                    .add(JSON_FIRST_NAME, s.getFirstname())
                    .add(JSON_TEAM, s.getTeam())
                    .add(JSON_ROLE,s.getRole())
                    .build();
            studentArrayBuilder.add(scJson);
        }
        JsonArray studentArray = studentArrayBuilder.build();
        
        //now put all things into a json object
        JsonObject dataManagerJSO = Json.createObjectBuilder()
            .add(JSON_BIG_TEAM, teamArray)
            .add(JSON_BIG_STUDENT,studentArray)
            .build();
        
        
        // AND NOW OUTPUT IT TO A JSON FILE WITH PRETTY PRINTING
        Map<String, Object> properties = new HashMap<>(1);
        properties.put(JsonGenerator.PRETTY_PRINTING, true);
        JsonWriterFactory writerFactory = Json.createWriterFactory(properties);
        StringWriter sw = new StringWriter();
        JsonWriter jsonWriter = writerFactory.createWriter(sw);
        jsonWriter.writeObject(dataManagerJSO);
        jsonWriter.close();

        // INIT THE WRITER
        OutputStream os = new FileOutputStream(filePath);
        JsonWriter jsonFileWriter = Json.createWriter(os);
        jsonFileWriter.writeObject(dataManagerJSO);
        String prettyPrinted = sw.toString();
        PrintWriter pw = new PrintWriter(filePath);
        pw.write(prettyPrinted);
        pw.close();
        
    }
    
    public void exportRecitationData(AppDataComponent data, String filePath) throws IOException{
        CSGData dataManager = (CSGData) data;
        
        //for the team
        JsonArrayBuilder recitationArrayBuilder = Json.createArrayBuilder();
        ObservableList<Recitation> recitations = dataManager.getRdData().getRecitations();
        for (Recitation t : recitations) {
            String r=t.getSection();
            r=t.getSection()+"("+t.getInstructor()+")";
            JsonObject scJson = Json.createObjectBuilder()
                    .add(JSON_SECTION, r)
                    .add(JSON_DAYTIME, t.getDaytime())
                    .add(JSON_LOCATION, t.getLocation())
                    .add(JSON_TA1, t.getTa1())
                    .add(JSON_TA2,t.getTa2())
                    .build();
            recitationArrayBuilder.add(scJson);
            
        }
        JsonArray recitationArray = recitationArrayBuilder.build();
        
        //now put all things into a json object
        JsonObject dataManagerJSO = Json.createObjectBuilder()
            .add(JSON_RECITATION,recitationArray)
            .build();
        
        // AND NOW OUTPUT IT TO A JSON FILE WITH PRETTY PRINTING
        Map<String, Object> properties = new HashMap<>(1);
        properties.put(JsonGenerator.PRETTY_PRINTING, true);
        JsonWriterFactory writerFactory = Json.createWriterFactory(properties);
        StringWriter sw = new StringWriter();
        JsonWriter jsonWriter = writerFactory.createWriter(sw);
        jsonWriter.writeObject(dataManagerJSO);
        jsonWriter.close();

        // INIT THE WRITER
        OutputStream os = new FileOutputStream(filePath);
        JsonWriter jsonFileWriter = Json.createWriter(os);
        jsonFileWriter.writeObject(dataManagerJSO);
        String prettyPrinted = sw.toString();
        PrintWriter pw = new PrintWriter(filePath);
        pw.write(prettyPrinted);
        pw.close();
    }
    
}
