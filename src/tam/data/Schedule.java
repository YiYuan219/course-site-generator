/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tam.data;

import java.time.LocalDate;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 *
 * @author Yi Yuan
 */
public class Schedule {
    private StringProperty type;
    private StringProperty date;
    private StringProperty title;
    private StringProperty topic;
    private String time;
    private String link;
    private String criteria;
    private LocalDate local;
    
    public Schedule(String initType, String initDate,String initTime,
                    String initTitle, String initTopic,String initLink,String initCriteria){

        type = new SimpleStringProperty(initType);
        date=new SimpleStringProperty(initDate);
        title=new SimpleStringProperty(initTitle);
        topic=new SimpleStringProperty(initTopic);
        time=initTime;
        link=initLink;
        criteria=initCriteria;
    }
    public Schedule(String initType,String initDate,String initTitle,String initTopic){
        type = new SimpleStringProperty(initType);
        date=new SimpleStringProperty(initDate);
        title=new SimpleStringProperty(initTitle);
        topic=new SimpleStringProperty(initTopic);
    }
    public String getDay(String s){
        String[] t=s.split("/");
        return t[1];         
    }
    public String getMonth(String s){
        String[] t=s.split("/");
        return t[0];
    }
    
    public String getYear(String s){
        String[] t=s.split("/");
        return t[2];
    }
    public String getTime() {
        return time;
    }
    public void setTime(String time) {
        this.time = time;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getCriteria() {
        return criteria;
    }

    public void setCriteria(String criteria) {
        this.criteria = criteria;
    }
    public String getType(){
        return type.get();
    }
    public void setType(String initType){
        type.set(initType);
    }
    public String getDate(){
        return date.get();
    }
    public void setTitle(String initTitle){
        title.set(initTitle);
    }
    public String getTitle(){
        return title.get();
    }
    public void setTopic(String initTopic){
        topic.set(initTopic);
    }
    public String getTopic(){
        return topic.get();
    }
    public void setDate(String initDate){
        date.set(initDate);
    }

    public LocalDate getLocal() {
        return local;
    }

    public void setLocal(LocalDate local) {
        this.local = local;
    }
    

    
    
}
