/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tam.data;

import djf.components.AppDataComponent;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import tam.CSGApp;

/**
 *
 * @author Yi Yuan
 */
public class CourseDetailData {
    CSGApp app;
    String subject;
    String number;
    String semester;
    String year;
    String title;
    String instructorName;
    String instructorHome;
    String exportDir;
    String srcDir;
    String banner;
    String left;
    String right;
    String stylesheet;
    ObservableList<CourseDetail> coursedetail;
    
    public CourseDetailData(CSGApp initApp){
        app=initApp;
        coursedetail=FXCollections.observableArrayList();
    }
    
    public CourseDetailData(String initSubject,String initNumber,String initSemester,
                             String initYear,String initTitle,String initInstructorName,String initInstructorHome,
                            String initExport, String initBanner, String initLeft,String initRight,String initStylesheet){
        subject=initSubject;
        number=initNumber;
        semester=initSemester;
        year=initYear;
        title=initTitle;
        instructorName=initInstructorName;
        instructorHome=initInstructorHome;
        exportDir=initExport;
        banner=initBanner;
        left=initLeft;
        right=initRight;
        stylesheet=initStylesheet;
        coursedetail=FXCollections.observableArrayList();
    }
   
    public CourseDetailData(){
        coursedetail=FXCollections.observableArrayList();
    }
    public ObservableList<CourseDetail> getCoursedetail() {
        return coursedetail;
    }

    public String getSrcDir() {
        return srcDir;
    }

    public void setSrcDir(String srcDir) {
        this.srcDir = srcDir;
    }

    
    
    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getSemester() {
        return semester;
    }

    public void setSemester(String semester) {
        this.semester = semester;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getInstructorName() {
        return instructorName;
    }

    public void setInstructorName(String instructorName) {
        this.instructorName = instructorName;
    }

    public String getInstructorHome() {
        return instructorHome;
    }

    public void setInstructorHome(String instructorHome) {
        this.instructorHome = instructorHome;
    }

    public String getExportDir() {
        return exportDir;
    }

    public void setExportDir(String exportDir) {
        this.exportDir = exportDir;
    }

    public String getBanner() {
        return banner;
    }

    public void setBanner(String banner) {
        this.banner = banner;
    }

    public String getLeft() {
        return left;
    }

    public void setLeft(String left) {
        this.left = left;
    }

    public String getRight() {
        return right;
    }

    public void setRight(String right) {
        this.right = right;
    }

    public String getStylesheet() {
        return stylesheet;
    }

    public void setStylesheet(String stylesheet) {
        this.stylesheet = stylesheet;
    }
    
    
    
    public void addCoursedetail(String initNavbar,String initFilename,String initScript,boolean initCheck){
        CourseDetail course=new CourseDetail(initNavbar,initFilename,initScript,initCheck);
        coursedetail.add(course);
        
    }
    
}
