/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tam.data;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 *
 * @author Yi Yuan
 */
public class Student {
    private StringProperty firstname;
    private StringProperty lastname;
    private StringProperty team;
    private StringProperty role;
    
    
    public Student(String initFirstname,String initLastname,String initTeam,String initRole){
        firstname=new SimpleStringProperty(initFirstname);
        lastname=new SimpleStringProperty(initLastname);
        team=new SimpleStringProperty(initTeam);
        role=new SimpleStringProperty(initRole);
        
    }
    
    
    public String getFirstname() {
        return firstname.get();
    }
    public void setFirstname(String initFirstname){
        firstname.set(initFirstname);
    }
    public String getLastname() {
        return lastname.get();
    }
    public void setLastname(String initLastname){
        lastname.set(initLastname);
    }
    public String getTeam() {
        return team.get();
    }
    public void setTeam(String initTeam){
        team.set(initTeam);
    }
    public String getRole() {
        return role.get();
    }
    public void setRole(String initRole){
        role.set(initRole);
    }
}
