/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tam.data;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 *
 * @author Yi Yuan
 */
public class CourseDetail {

    private StringProperty navbar;
    private StringProperty filename;
    private StringProperty script;
    private Boolean check;
    
    public CourseDetail(String initNavbar,String initFilename,String initScript,boolean initCheck){
  
        navbar=new SimpleStringProperty(initNavbar);
        filename=new SimpleStringProperty(initFilename);
        script=new SimpleStringProperty(initScript);
        check=initCheck;        
    }

    public Boolean getCheck() {
        return check;
    }
    public void setCheck(Boolean check) {
        this.check = check;
    }
    public String  getNavbar(){
        return navbar.get();
    }
    public void setNavbar(String initNavbar){
        navbar.set(initNavbar);
    }
    public String getFilename(){
        return filename.get();
    }
    public void setFilename(String initFilename){
        filename.set(initFilename);
    }
    public String getScript(){
        return script.get();
    }
    public void setScript(String initScript){
        script.set(initScript);
    }
}
