/*
 * To change this license header, choose License Headers in Team Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tam.data;

import djf.components.AppDataComponent;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.paint.Color;
import tam.CSGApp;

/**
 *
 * @author Yi Yuan
 */
public class ProjectData {
    CSGApp app;
    ObservableList<Team> teams;
    ObservableList<Student> students;
    
    public ProjectData(CSGApp initApp){
        app=initApp;
        teams = FXCollections.observableArrayList();
        students = FXCollections.observableArrayList();
        
    }
    
    public ProjectData(){
        teams = FXCollections.observableArrayList();
        students = FXCollections.observableArrayList();
    }

    public void setTeams(ObservableList<Team> teams) {
        this.teams = teams;
    }

    public void setStudents(ObservableList<Student> stu) {
        
        for(Student s:stu){
            this.students.add(s);
        }
       
    }
    
    public ObservableList<Team> getTeams() {
        return teams;
    }

    public ObservableList<Student> getStudents() {
        return students;
    }
    
    public void addTeam(String initName,String initColor,String initTextcolor,String initLink){
        Team p=new Team(initName,initColor,initTextcolor,initLink);
        teams.add(p);   
    }
    
    
    public void removeTeam(String name) {
        for (Team team : teams) {
            if (name.equals(team.getName())) {
                teams.remove(team);
                return;
            }
        }
    }
     
    public void addStudent(String initFirstname,String initLastname,String initTeam,String initRole){
        Student p=new Student(initFirstname,initLastname,initTeam,initRole);
        students.add(p);
    }
    
    public void removeStudent(String firstname,String lastname) {
        for (Student student : students) {
            if ((firstname.equals(student.getFirstname()))&&(lastname.equals(student.getLastname()))) {
                students.remove(student);
                return;
            }
        }
    }
    
    
     public boolean containsStudent(String firstname,String lastname) {
        for (Student stu : students) {
            if (stu.getFirstname().equals(firstname)&&stu.getLastname().equals(lastname)) {
                return true;
            }
        }
        return false;
    }
    
    
    
}
