/*
 * To change this license header, choose License Headers in Team Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tam.data;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.paint.Color;

/**
 *
 * @author Yi Yuan
 */
public class Team {
    private StringProperty name;
    private StringProperty color;
    private StringProperty textcolor;
    private StringProperty link;
    private ObservableList<Student> students;
  

    
    public Team(String initName,String initColor,String initTextcolor,String initLink){
        name=new SimpleStringProperty(initName);
        color=new SimpleStringProperty(initColor);
        textcolor=new SimpleStringProperty(initTextcolor);
        link=new SimpleStringProperty(initLink);
        
        students = FXCollections.observableArrayList();
        
        
    }

//    public Color getC() {
//        return c;
//    }
//
//    public void setC(Color c) {
//        this.c = c;
//    }
//
//    public Color getTc() {
//        return tc;
//    }
//
//    public void setTc(Color tc) {
//        this.tc = tc;
//    }
    
    
    public String getName() {
        return name.get();
    }
    public void setName(String initName){
        name.set(initName);
    }
    public String getColor() {
        return color.get();
    }
    public void setColor(String initColor){
        color.set(initColor);
    }
    public String getTextcolor() {
        return textcolor.get();
    }
    public void setTextcolor(String initTextcolor){
        textcolor.set(initTextcolor);
    }
    public String getLink() {
        return link.get();
    }
    public void setLink(String initLink){
        link.set(initLink);
    }

    public ObservableList<Student> getStudents() {
        return students;
    }

    public void setStudents(ObservableList<Student> students) {
        this.students = students;
    }
    
    public String getRed(String color){
        int red;
        String t=String.valueOf(color.charAt(0));
        t=t+String.valueOf(color.charAt(1));
        red = Integer.valueOf(t, 16 );
        String s = String.valueOf(red);
        return s;
    }
    
    public String getGreen(String color){
        int green;
        String t=String.valueOf(color.charAt(2));
        t=t+String.valueOf(color.charAt(3));
        green=Integer.valueOf(t, 16 );
        String s = String.valueOf(green);
        return s;
    }
    
    public String getBlue(String color){
        int blue;
        String t=String.valueOf(color.charAt(4));
        t=t+String.valueOf(color.charAt(5));
        blue=Integer.valueOf( t, 16 );
        String s = String.valueOf(blue);
        return s;
    }
    
}
