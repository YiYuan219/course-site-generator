/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tam.data;

import djf.components.AppDataComponent;
import java.util.Collections;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import tam.CSGApp;

/**
 *
 * @author Yi Yuan
 */
public class RecitationData {
    CSGApp app;
    ObservableList<Recitation> recitations;

    public ObservableList<Recitation> getRecitations() {
        return recitations;
    }
    
    public RecitationData(CSGApp initApp){
        app=initApp;
        recitations = FXCollections.observableArrayList();
    }
    public RecitationData(){
        recitations=FXCollections.observableArrayList();
    }
    public void addRecitation(String initSection,String initInstructor,String initDaytime,String initLocation,
                      String initTa1,String initTa2){
        Recitation recita=new Recitation(initSection,initInstructor,initDaytime,initLocation,initTa1,initTa2);
        recitations.add(recita);
    }
    
    public void removeRecitation(String section) {
        for (Recitation recitation : recitations) {
            if (section.equals(recitation.getSection())) {
                recitations.remove(recitation);
                return;
            }
        }
    }
    
    
    public boolean containsRecitation(String oldSection) {
        for (Recitation re : recitations) {
            if (re.getSection().equals(oldSection)) {
                return true;
            }
        }
        return false;
    }
    
    
}
