/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tam.data;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 *
 * @author Yi Yuan
 */
public class Recitation {
    private StringProperty section;
    private StringProperty instructor;
    private StringProperty daytime;
    private StringProperty location;
    private StringProperty ta1;
    private StringProperty ta2;
    
    public Recitation(String initSection,String initInstructor,String initDaytime,String initLocation,
                      String initTa1,String initTa2){
        section=new SimpleStringProperty(initSection);
        instructor=new SimpleStringProperty(initInstructor);
        daytime=new SimpleStringProperty(initDaytime);
        location=new SimpleStringProperty(initLocation);
        ta1=new SimpleStringProperty(initTa1);
        ta2=new SimpleStringProperty(initTa2);
    }
    public Recitation(){
    }
    public String getSection(){
        return section.get();
    }
    public void setSection(String initSection) {
        section.set(initSection);
    }
    public String getInstructor(){
        return instructor.get();
    }
    public void setInstructor(String initInstructor){
        instructor.set(initInstructor);
    }
    public String getDaytime(){
        return daytime.get();
    }
    public void setDaytime(String initDaytime){
        daytime.set(initDaytime);
    }
    public String getLocation(){
        return location.get();
    }
    public void setLocation(String initLocation){
        location.set(initLocation);
    }
    public String getTa1(){
        return ta1.get();
    }
    public void setTA1(String initTA1){
        ta1.set(initTA1);
    }
    public String getTa2(){
        return ta2.get();
    }
    public void setTA2(String initTA2){
        ta2.set(initTA2);
    }
   
}
