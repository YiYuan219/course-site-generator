/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tam.data;

import djf.components.AppDataComponent;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import tam.CSGApp;

/**
 *
 * @author Yi Yuan
 */
public class ScheduleData {
    CSGApp app;
    ObservableList<Schedule> schedules;
    String startM;
    String endF;


    
    public ScheduleData(CSGApp initApp){
        app=initApp;
        schedules=FXCollections.observableArrayList();
        startM="0/00/0000";
        endF="0/00/0000";
    }
    
    public ScheduleData(){
        startM="0/00/0000";
        endF="0/00/0000";
        schedules=FXCollections.observableArrayList();
    }
    public String getMonth(String s){
        String[] t=s.split("/");
        return t[0];
    }
    public String getDate(String s){
        String[] t=s.split("/");
        return t[1];
    }
    public String getYear(String s){
        String[] t=s.split("/");
        return t[2];
    }
    public ObservableList<Schedule> getSchedules() {
        return schedules;
    }
    
    
    public void addSchedules(String initType,String initDate,String initTime,String initTitle,String initTopic,String initLink,String initCriteria){
        Schedule sch=new Schedule(initType,initDate,initTime,initTitle,initTopic,initLink,initCriteria);
        schedules.add(sch);
    }
    
    public void removeSchedule(String title) {
        for (Schedule schedule : schedules) {
            if (title.equals(schedule.getTitle())) {
                schedules.remove(schedule);
                return;
            }
        }
    }
    
    public String getStartM() {
        return startM;
    }

    public void setStartM(String startM) {
        this.startM = startM;
    }

    public String getEndF() {
        return endF;
    }

    public void setEndF(String endF) {
        this.endF = endF;
    }
    
}
