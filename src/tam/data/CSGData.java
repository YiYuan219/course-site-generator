/*
 * To change this license header, choose License Headers in Team Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tam.data;

import djf.components.AppDataComponent;
import tam.CSGApp;

/**
 *
 * @author apple
 */
public class CSGData implements AppDataComponent{
    CSGApp app;
    CourseDetailData cdData;
    RecitationData rdData;
    TAData taData;
    ProjectData pdData;
    ScheduleData sdData;
    
    public CSGData(){
        cdData=new CourseDetailData();
        rdData=new RecitationData();
        String n="sss";
        taData=new TAData(n);
        pdData=new ProjectData();
        sdData=new ScheduleData(); 
    }
    
    public CSGData(CSGApp initApp){
        cdData=new CourseDetailData(initApp);
        rdData=new RecitationData(initApp);
        taData=new TAData(initApp);
        pdData=new ProjectData(initApp);
        sdData=new ScheduleData(initApp); 
    }

    public CourseDetailData getCdData() {
        return cdData;
    }

    public RecitationData getRdData() {
        return rdData;
    }

    public TAData getTaData() {
        return taData;
    }

    public ProjectData getPdData() {
        return pdData;
    }

    public ScheduleData getSdData() {
        return sdData;
    }
    
    
    

    @Override
    public void resetData() {
        rdData.getRecitations().clear();
        taData.getOfficeHours().clear();
        taData.getTeachingAssistants().clear();
        pdData.getStudents().clear();
        pdData.getTeams().clear();
        sdData.getSchedules();
        
    }
    
}
