/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tam.jtps;

import jtps.jTPS_Transaction;
import tam.CSGApp;
import tam.data.Recitation;
import tam.data.Student;
import tam.workspace.TAWorkspace;

/**
 *
 * @author apple
 */
public class EditStudent_Transaction implements jTPS_Transaction{
    private String firstname;
    private String lastname;
    private String team;
    private String role;
    
    private CSGApp app;
    private TAWorkspace workspace;
    private Student student;

    public EditStudent_Transaction(CSGApp app,String firstname,String lastname,String team,String role,Student student){
        this.app = app;
        workspace = (TAWorkspace)app.getWorkspaceComponent();
        this.firstname=firstname;
        this.lastname=lastname;
        this.team=team;
        this.role=role;  
        this.student=student;
    }
    
    
    @Override
    public void doTransaction() {
        String newfirstname=workspace.getFnTF().getText();
        String newlastname=workspace.getLnTF().getText();
        String newteam=(String)workspace.getTBox().getValue();
        String newrole=workspace.getRTF().getText();
        student.setFirstname(newfirstname);
        student.setLastname(newlastname);
        student.setTeam(newteam);
        student.setRole(newrole);
        workspace.getSTable().refresh();
    }

    @Override
    public void undoTransaction() {
        student.setFirstname(firstname);
        student.setLastname(lastname);
        student.setRole(role);
        student.setTeam(team);
        workspace.getSTable().refresh();
        
    }
    
}
