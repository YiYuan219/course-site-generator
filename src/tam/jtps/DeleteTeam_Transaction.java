/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tam.jtps;

import java.util.Iterator;
import javafx.collections.ObservableList;
import jtps.jTPS_Transaction;
import tam.CSGApp;
import tam.data.CSGData;
import tam.data.ProjectData;
import tam.data.Student;
import tam.workspace.TAWorkspace;

/**
 *
 * @author apple
 */
public class DeleteTeam_Transaction implements jTPS_Transaction{
    private String name;
    private String color;
    private String textcolor;
    private String link;
    private ObservableList<Student> students;
    private CSGApp app;

    
     public DeleteTeam_Transaction(CSGApp app,String name,String color,String textcolor,String link,ObservableList<Student> students){
        this.app=app;
        this.name=name;
        this.color=color;
        this.textcolor=textcolor;
        this.link=link;
        this.students=students;          
    }
    
    @Override
    public void doTransaction() {
        CSGData d=(CSGData)app.getDataComponent();
        ProjectData data=d.getPdData();
        data.removeTeam(name);
        TAWorkspace workspace=(TAWorkspace)app.getWorkspaceComponent();
        workspace.getTeamOption().remove(name);
        
        ObservableList<Student> stus=data.getStudents();
        
        Iterator<Student> iter = stus.iterator();
                while (iter.hasNext()) {
                    Student stu = iter.next();
                    if (stu.getTeam().equals(name))
                        iter.remove();
                } 
        System.out.println(students.size());
    }

    @Override
    public void undoTransaction() {
        CSGData d=(CSGData)app.getDataComponent();
        ProjectData data=d.getPdData();
        data.addTeam(name, color, textcolor, link);
        TAWorkspace workspace=(TAWorkspace)app.getWorkspaceComponent();
        workspace.getTeamOption().add(name);
        
        data.setStudents(students);
        workspace.getSTable().refresh();
    }
    
    
}
