/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tam.jtps;

import java.util.regex.Pattern;
import jtps.jTPS_Transaction;
import tam.CSGApp;
import tam.data.CSGData;
import tam.data.TAData;
import tam.workspace.TAController;
import tam.workspace.TAWorkspace;

/**
 *
 * @author zhaotingyi
 */
public class TAAdderUR implements jTPS_Transaction{
    
    private String TAName;
    private String TAEmail;
    private CSGApp app;
    private TAWorkspace workspace;
    
    public TAAdderUR(CSGApp app){
        this.app = app;
        workspace = (TAWorkspace)app.getWorkspaceComponent();
        TAName = workspace.getNameTextField().getText();
        TAEmail = workspace.getEmailTextField().getText();
    }

    @Override
    public void doTransaction() {
        CSGData data=(CSGData)app.getDataComponent();
        data.getTaData().addTA(TAName, TAEmail,false);  
    }

    @Override
    public void undoTransaction() {
        CSGData data=(CSGData)app.getDataComponent();
        data.getTaData().removeTA(TAName); 
        //((TAData)app.getDataComponent()).removeTA(TAName);
    }
    
}
