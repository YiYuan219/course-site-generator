/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tam.jtps;

import jtps.jTPS_Transaction;
import tam.CSGApp;
import tam.data.CSGData;
import tam.data.ProjectData;

/**
 *
 * @author apple
 */
public class DeleteStudent_Transaction implements jTPS_Transaction{
    private String firstname;
    private String lastname;
    private String team;
    private String role;
    private CSGApp app;
    
    public DeleteStudent_Transaction(CSGApp app,String firstname,String lastname,String team,String role){
        this.app=app;
        this.firstname=firstname;
        this.lastname=lastname;
        this.team=team;
        this.role=role;
                   
    }

    @Override
    public void doTransaction() {
        CSGData d=(CSGData)app.getDataComponent();
        ProjectData data=d.getPdData();
        data.removeStudent(firstname, lastname);
    }

    @Override
    public void undoTransaction() {
        CSGData d=(CSGData)app.getDataComponent();
        ProjectData data=d.getPdData();
        data.addStudent(firstname, lastname, team, role);
    }
    
    
}
