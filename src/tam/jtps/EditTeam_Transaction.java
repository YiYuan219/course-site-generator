/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tam.jtps;

import javafx.collections.ObservableList;
import javafx.scene.control.ColorPicker;
import javafx.scene.control.TextField;
import jtps.jTPS_Transaction;
import tam.CSGApp;
import tam.data.CSGData;
import tam.data.Student;
import tam.data.Team;
import tam.workspace.TAWorkspace;

/**
 *
 * @author apple
 */
public class EditTeam_Transaction implements jTPS_Transaction{
    
    private String name;
    private String color;
    private String textcolor;
    private String link;
    private String newn;
    private CSGApp app;
    private TAWorkspace workspace;
    private Team team;
    
    public EditTeam_Transaction(CSGApp app,String name,String color,String textcolor,String link,Team team,String newname){
        this.app = app;
        workspace = (TAWorkspace)app.getWorkspaceComponent();
        this.name=name;
        this.color=color;
        this.textcolor=textcolor;
        this.link=link;  
        this.team=team;
        this.newn=newname;
    }

    @Override
    public void doTransaction() {
        CSGData d=(CSGData)app.getDataComponent();
        TextField nameField=workspace.getNF();
        ColorPicker colorPicker=workspace.getColorPicker();
        ColorPicker textPicker=workspace.getTextPicker();
        TextField linkField=workspace.getLTF();
        String newname=nameField.getText();
        String newlink=linkField.getText();
        
        
        String c=colorPicker.getValue().toString();
        String newcolor=c.substring(2, 8);
     
        String  t=textPicker.getValue().toString();
        String newtextcolor=t.substring(2,8);
        team.setName(newname);
        team.setColor(newcolor);
        team.setTextcolor(newtextcolor);
        team.setLink(newlink);
        
        ObservableList<Student> stus=d.getPdData().getStudents();
        for(Student stu:stus){
            if(stu.getTeam().equals(name)){
                stu.setTeam(newname);
            }
        }
        workspace.getTTable().refresh();
        workspace.getSTable().refresh();
        
    }

    @Override
    public void undoTransaction() {
        team.setName(name);
        team.setColor(color);
        team.setTextcolor(textcolor);
        team.setLink(link);
        CSGData d=(CSGData)app.getDataComponent();
        ObservableList<Student> stus=d.getPdData().getStudents();
        for(Student stu:stus){
            if(stu.getTeam().equals(newn)){
                stu.setTeam(name);
            }
        }
        workspace.getSTable().refresh();
        workspace.getTTable().refresh();
    }  
}
