/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tam.jtps;

import jtps.jTPS_Transaction;
import tam.CSGApp;
import tam.data.CSGData;
import tam.data.ProjectData;
import tam.workspace.TAWorkspace;


/**
 *
 * @author apple
 */
public class AddTeam_Transaction implements jTPS_Transaction{
    private String name;
    private String color;
    private String textcolor;
    private String link;
    private CSGApp app;
    
    
    public AddTeam_Transaction(CSGApp app,String name,String color,String textcolor,String link){
        this.app=app;
        this.name=name;
        this.color=color;
        this.textcolor=textcolor;
        this.link=link;
                   
    }

    @Override
    public void doTransaction() {
        CSGData d=(CSGData)app.getDataComponent();
        ProjectData data=d.getPdData();
        data.addTeam(name, color, textcolor, link);
        TAWorkspace workspace=(TAWorkspace)app.getWorkspaceComponent();
        workspace.getTeamOption().add(name);
    }

    @Override
    public void undoTransaction() {
        CSGData d=(CSGData)app.getDataComponent();
        ProjectData data=d.getPdData();
        data.removeTeam(name);
        TAWorkspace workspace=(TAWorkspace)app.getWorkspaceComponent();
        workspace.getTeamOption().remove(name);
    }
    
    
}
