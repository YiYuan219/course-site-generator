/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tam.jtps;

import jtps.jTPS_Transaction;
import tam.CSGApp;
import tam.data.CSGData;
import tam.data.RecitationData;
import tam.data.ScheduleData;

/**
 *
 * @author apple
 */
public class AddSchedule_Transaction implements jTPS_Transaction{
    private String type;
    private String date;
    private String time;
    private String title;
    private String topic;
    private String link;
    private String criteria;
    private CSGApp app;
    
    
    public AddSchedule_Transaction(CSGApp app,String type,String date,String time,String title,String topic,String link,String criteria){
        this.app=app;
        this.type=type;
        this.date=date;
        this.time=time;
        this.title=title;
        this.topic=topic;
        this.link=link;
        this.criteria=criteria;
    }

    @Override
    public void doTransaction() {
        CSGData d=(CSGData)app.getDataComponent();
        ScheduleData data=d.getSdData();
        data.addSchedules(type, date, time, title, topic, link, criteria);
    }

    @Override
    public void undoTransaction() {
        CSGData d=(CSGData)app.getDataComponent();
        ScheduleData data=d.getSdData();
        data.removeSchedule(title);
    }
    
}
