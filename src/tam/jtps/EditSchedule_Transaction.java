/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tam.jtps;

import jtps.jTPS_Transaction;
import tam.CSGApp;
import tam.data.Schedule;
import tam.data.Student;
import tam.workspace.TAWorkspace;

/**
 *
 * @author apple
 */
public class EditSchedule_Transaction implements jTPS_Transaction{
    private String type;
    private String date;
    private String time;
    private String title;
    private String topic;
    private String link;
    private String criteria;
    private CSGApp app;
    private Schedule schedule;
    private TAWorkspace workspace;

    public EditSchedule_Transaction(CSGApp app,String type,String date,String time,String title,String topic, String link, String criteria,Schedule schedule){
        this.app = app;
        workspace = (TAWorkspace)app.getWorkspaceComponent();
        this.type=type;
        this.date=date;
        this.time=time;
        this.title=title;  
        this.topic=topic;
        this.link=link;
        this.criteria=criteria;
        this.schedule=schedule;
    }
    
    
    @Override
    public void doTransaction() {
        String newType=(String)workspace.getTypeBox().getValue();
        String newDate=workspace.getDatePicker().getEditor().getText();
        String newTime = workspace.getTimeText().getText();
        String newTitle=workspace.getTTextField().getText();
        String newTopic=workspace.getTopicText().getText();
        String newLink=workspace.getLinkField().getText();
        String newCriteria=workspace.getCriteriaField().getText();
        schedule.setType(newType);
            schedule.setDate(newDate);
            schedule.setTime(newTime);
            schedule.setTitle(newTitle);
            schedule.setTopic(newTopic);
            schedule.setLink(newLink);
            schedule.setCriteria(newCriteria);
            workspace.getSiTable().refresh();
    }

    @Override
    public void undoTransaction() {
        schedule.setType(type);
        schedule.setDate(date);
        schedule.setTime(time);
        schedule.setTitle(title);
        schedule.setTopic(topic);
        schedule.setLink(link);
        schedule.setCriteria(criteria);
        workspace.getSiTable().refresh();
    }
    
}
