/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tam.jtps;

import jtps.jTPS_Transaction;
import tam.CSGApp;
import tam.data.CSGData;
import tam.data.RecitationData;
import tam.workspace.TAWorkspace;

/**
 *
 * @author Yi Yuan
 */
public class AddRecitation_Transaction implements jTPS_Transaction{

    private String section;
    private String instructor;
    private String daytime;
    private String location;
    private String ta1;
    private String ta2;
    private CSGApp app;
   
    
    
    public AddRecitation_Transaction(CSGApp app,String section,String instructor,String daytime,String location,String ta1,String ta2){
        this.app = app;
        //workspace = (TAWorkspace)app.getWorkspaceComponent();
        this.section=section;
        this.instructor=instructor;
        this.daytime=daytime;
        this.location=location;
        this.ta1=ta1;
        this.ta2=ta2;
        
    }
    
    
    
    @Override
    public void doTransaction() {
        CSGData d=(CSGData)app.getDataComponent();
        RecitationData data=d.getRdData();
        data.addRecitation(section, instructor, daytime, location, ta1, ta2);
    }

    @Override
    public void undoTransaction() {
        CSGData d=(CSGData)app.getDataComponent();
        RecitationData data=d.getRdData();
        data.removeRecitation(section);
    }
    
}
