/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tam.jtps;

import jtps.jTPS_Transaction;
import tam.CSGApp;
import tam.data.Recitation;
import tam.workspace.TAWorkspace;

/**
 *
 * @author apple
 */
public class EditRecitation_Transaction implements jTPS_Transaction{
    private String section;
    private String instructor;
    private String daytime;
    private String location;
    private String ta1;
    private String ta2;
    private CSGApp app;
    private TAWorkspace workspace;
    private Recitation recitation;
    
    public EditRecitation_Transaction(CSGApp app,String section,String instructor,String daytime,String location,String ta1,String ta2,Recitation recitation){
        this.app = app;
        workspace = (TAWorkspace)app.getWorkspaceComponent();
        this.section=section;
        this.instructor=instructor;
        this.daytime=daytime;
        this.location=location;
        this.ta1=ta1;
        this.ta2=ta2;
        this.recitation=recitation;
    }

    @Override
    public void doTransaction() {
        String newSection = workspace.getSectionField().getText();
        String newInstructor = workspace.getInstructorField().getText();
        String newDaytime=workspace.getDaytimeField().getText();
        String newLocation=workspace.getLocation().getText();
        String newTA1=(String)workspace.getSuTA1Box().getValue();
        String newTA2=(String)workspace.getSuTA2Box().getValue();
        recitation.setSection(newSection);
        recitation.setInstructor(newInstructor);
        recitation.setDaytime(newDaytime);
        recitation.setLocation(newLocation);
        recitation.setTA1(newTA1);
        recitation.setTA2(newTA2);
        workspace.getRecitationTable().refresh();
        
    }

    @Override
    public void undoTransaction() {
        recitation.setSection(section);
        recitation.setInstructor(instructor);
        recitation.setDaytime(daytime);
        recitation.setLocation(location);
        recitation.setTA1(ta1);
        recitation.setTA2(ta2);
        workspace.getRecitationTable().refresh();
    }
    
    
}
