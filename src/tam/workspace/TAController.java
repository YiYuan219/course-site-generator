package tam.workspace;

import static djf.settings.AppStartupConstants.CHOOSE_TITLE;
import static djf.settings.AppStartupConstants.EXPORT_TITLE;
import static djf.settings.AppStartupConstants.PATH_WORK;
import djf.ui.AppGUI;
import static tam.TAManagerProp.*;
import djf.ui.AppMessageDialogSingleton;
import djf.ui.AppYesNoCancelDialogSingleton;
import java.io.File;
import java.time.LocalDate;
import static java.time.temporal.TemporalQueries.localDate;
import java.util.ArrayList;
import java.util.Iterator;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.Pane;
import properties_manager.PropertiesManager;
import tam.CSGApp;
import tam.data.TAData;
import tam.data.TeachingAssistant;
import static tam.style.TAStyle.CLASS_HIGHLIGHTED_GRID_CELL;
import static tam.style.TAStyle.CLASS_HIGHLIGHTED_GRID_ROW_OR_COLUMN;
import static tam.style.TAStyle.CLASS_OFFICE_HOURS_GRID_TA_CELL_PANE;
import java.util.regex.Pattern;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.ColorPicker;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.image.Image;
import javafx.scene.paint.Color;
import static javafx.scene.paint.Color.color;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;
import jtps.jTPS;
import jtps.jTPS_Transaction;
import tam.TAManagerProp;
import tam.data.CSGData;
import tam.data.ProjectData;
import tam.data.Recitation;
import tam.data.RecitationData;
import tam.data.Schedule;
import tam.data.ScheduleData;
import tam.data.Student;
import tam.data.Team;
import tam.file.TimeSlot;
import tam.jtps.AddRecitation_Transaction;
import tam.jtps.AddSchedule_Transaction;
import tam.jtps.AddStudent_Transaction;
import tam.jtps.AddTeam_Transaction;
import tam.jtps.DeleteRecitation_Transaction;
import tam.jtps.DeleteSchedule_Transaction;
import tam.jtps.DeleteStudent_Transaction;
import tam.jtps.DeleteTeam_Transaction;
import tam.jtps.EditRecitation_Transaction;
import tam.jtps.EditSchedule_Transaction;
import tam.jtps.EditStudent_Transaction;
import tam.jtps.EditTeam_Transaction;
import tam.jtps.TAAdderUR;
import tam.jtps.TAReplaceUR;
import tam.jtps.TAdeletUR;
import tam.jtps.TAhourschangeUR;
import tam.jtps.TAtoggleUR;

/**
 * This class provides responses to all workspace interactions, meaning
 * interactions with the application controls not including the file
 * toolbar.
 * 
 * @author Richard McKenna
 * @version 1.0
 */
public class TAController {
    static jTPS jTPS = new jTPS();
    // THE APP PROVIDES ACCESS TO OTHER COMPONENTS AS NEEDED
    CSGApp app;

    /**
     * Constructor, note that the app must already be constructed.
     */
    public TAController(CSGApp initApp) {
        // KEEP THIS FOR LATER
        app = initApp;
    }
    
    /**
     * This helper method should be called every time an edit happens.
     */    
    public void markWorkAsEdited() {
        // MARK WORK AS EDITED
        AppGUI gui = app.getGUI();
        gui.getFileController().markAsEdited(gui);
    }
    
    public void bannerChange(){
        TAWorkspace workspace = (TAWorkspace)app.getWorkspaceComponent();
        CSGData d=(CSGData)app.getDataComponent();
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        
        FileChooser fc = new FileChooser();
        String p=d.getCdData().getSrcDir()+"\\images";
        fc.setInitialDirectory(new File(p));
        fc.setTitle(props.getProperty(CHOOSE_TITLE));
        File selectedFile = fc.showOpenDialog(app.getGUI().getWindow());
        String name=selectedFile.getName();
        String path=selectedFile.getPath();
        Image banner=new Image("file:"+path);
        workspace.getYaleIV1().setImage(banner);
        d.getCdData().setBanner(name);
        
    }
    
    public void leftChange(){
        TAWorkspace workspace = (TAWorkspace)app.getWorkspaceComponent();
        CSGData d=(CSGData)app.getDataComponent();
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        
        FileChooser fc = new FileChooser();
        String p=d.getCdData().getSrcDir()+"\\images";
        fc.setInitialDirectory(new File(p));
        fc.setTitle(props.getProperty(CHOOSE_TITLE));
        File selectedFile = fc.showOpenDialog(app.getGUI().getWindow());
        String name=selectedFile.getName();
        String path=selectedFile.getPath();
       
        Image banner=new Image("file:"+path);
        workspace.getYaleIV2().setImage(banner);
        d.getCdData().setLeft(name);
        
    }
    
    public void rightChange(){
        TAWorkspace workspace = (TAWorkspace)app.getWorkspaceComponent();
        CSGData d=(CSGData)app.getDataComponent();
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        
        FileChooser fc = new FileChooser();
        String p=d.getCdData().getSrcDir()+"\\images";
        fc.setInitialDirectory(new File(p));
        fc.setTitle(props.getProperty(CHOOSE_TITLE));
        File selectedFile = fc.showOpenDialog(app.getGUI().getWindow());
        String name=selectedFile.getName();
        String path=selectedFile.getPath();

        Image banner=new Image("file:"+path);
        workspace.getCSIV().setImage(banner);
        d.getCdData().setRight(name);
    }
    
    public void changeSiteTemplate(){
        TAWorkspace workspace = (TAWorkspace)app.getWorkspaceComponent();
        CSGData d=(CSGData)app.getDataComponent();
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        
        DirectoryChooser fc = new DirectoryChooser();
        fc.setInitialDirectory(new File("./work/template"));
        fc.setTitle(props.getProperty(EXPORT_TITLE));
        File selectedFile = fc.showDialog(app.getGUI().getWindow());
        String srcpath=selectedFile.getPath();
        d.getCdData().setSrcDir(srcpath);
        workspace.getSelectTemple().setText(srcpath);
        System.out.println(srcpath);
        
        
        StyleSheetOption(srcpath);
        
        
    }
    
    public void changeExportDir(){
        TAWorkspace workspace = (TAWorkspace)app.getWorkspaceComponent();
        CSGData d=(CSGData)app.getDataComponent();
        
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        DirectoryChooser fc = new DirectoryChooser();
        fc.setInitialDirectory(new File(PATH_WORK));
        fc.setTitle(props.getProperty(EXPORT_TITLE));
        File selectedFile = fc.showDialog(app.getGUI().getWindow());
        String exportpath=selectedFile.getPath();
        d.getCdData().setExportDir(exportpath);
        workspace.getDirLabel().setText(exportpath);
    }
    
    /**
     * This method responds to when the user requests to add
     * a new TA via the UI. Note that it must first do some
     * validation to make sure a unique name and email address
     * has been provided.
     */
    public void handleAddTA() {
        // WE'LL NEED THE WORKSPACE TO RETRIEVE THE USER INPUT VALUES
        TAWorkspace workspace = (TAWorkspace)app.getWorkspaceComponent();
        TextField nameTextField = workspace.getNameTextField();
        TextField emailTextField = workspace.getEmailTextField();
        String name = nameTextField.getText();
        String email = emailTextField.getText();
        
        // WE'LL NEED TO ASK THE DATA SOME QUESTIONS TOO
        CSGData d=(CSGData)app.getDataComponent();
        TAData data =d.getTaData();
        
        // WE'LL NEED THIS IN CASE WE NEED TO DISPLAY ANY ERROR MESSAGES
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        
        // DID THE USER NEGLECT TO PROVIDE A TA NAME?
        if (name.isEmpty()) {
	    AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
	    dialog.show(props.getProperty(MISSING_TA_NAME_TITLE), props.getProperty(MISSING_TA_NAME_MESSAGE));            
        }
        // DID THE USER NEGLECT TO PROVIDE A TA EMAIL?
        else if (email.isEmpty()) {
	    AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
	    dialog.show(props.getProperty(MISSING_TA_EMAIL_TITLE), props.getProperty(MISSING_TA_EMAIL_MESSAGE));                        
        }
        // DOES A TA ALREADY HAVE THE SAME NAME OR EMAIL?
        else if (data.containsTA(name, email)) {
	    AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
	    dialog.show(props.getProperty(TA_NAME_AND_EMAIL_NOT_UNIQUE_TITLE), props.getProperty(TA_NAME_AND_EMAIL_NOT_UNIQUE_MESSAGE));                                    
        }
        else if (!Pattern.compile("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@" + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$").matcher(email).matches()){
	    AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
	    dialog.show(props.getProperty(TA_EMAIL_INVALID_TITLE), props.getProperty(TA_EMAIL_INVALID_MESSAGE));
        }
        // EVERYTHING IS FINE, ADD A NEW TA
        else {
            
            // ADD THE NEW TA TO THE DATA
            jTPS_Transaction addTAUR = new TAAdderUR(app);
            jTPS.addTransaction(addTAUR);
            
            // CLEAR THE TEXT FIELDS
            nameTextField.setText("");
            emailTextField.setText("");
            
            // AND SEND THE CARET BACK TO THE NAME TEXT FIELD FOR EASY DATA ENTRY
            nameTextField.requestFocus();
            
            // WE'VE CHANGED STUFF
            markWorkAsEdited();
            workspace.getTeacher1().add(name);
        }
    }

    /**
     * This function provides a response for when the user presses a
     * keyboard key. Note that we're only responding to Delete, to remove
     * a TA.
     * 
     * @param code The keyboard code pressed.
     */
    public void handleKeyPress(KeyCode code) {
        // DID THE USER PRESS THE DELETE KEY?
        if (code == KeyCode.DELETE) {
            // GET THE TABLE
            TAWorkspace workspace = (TAWorkspace)app.getWorkspaceComponent();
            TableView taTable = workspace.getTATable();
            
            // IS A TA SELECTED IN THE TABLE?
            Object selectedItem = taTable.getSelectionModel().getSelectedItem();
            if (selectedItem != null) {
                // GET THE TA AND REMOVE IT
                TeachingAssistant ta = (TeachingAssistant)selectedItem;
                String taName = ta.getName();
                CSGData d=(CSGData)app.getDataComponent();
                
                TAData data = d.getTaData();
                
                jTPS_Transaction deletUR = new TAdeletUR(app, taName);
                jTPS.addTransaction(deletUR);
                
                // AND BE SURE TO REMOVE ALL THE TA'S OFFICE HOURS
                // WE'VE CHANGED STUFF
                markWorkAsEdited();
            }
        }
    }

    /**
     * This function provides a response for when the user clicks
     * on the office hours grid to add or remove a TA to a time slot.
     * 
     * @param pane The pane that was toggled.
     */
    public void handleCellToggle(Pane pane) {
        // GET THE TABLE
        TAWorkspace workspace = (TAWorkspace)app.getWorkspaceComponent();
        TableView taTable = workspace.getTATable();
        
        // IS A TA SELECTED IN THE TABLE?
        Object selectedItem = taTable.getSelectionModel().getSelectedItem();
        if (selectedItem != null) {
            // GET THE TA
            TeachingAssistant ta = (TeachingAssistant)selectedItem;
            String taName = ta.getName();
            CSGData d=(CSGData)app.getDataComponent();
            TAData data = d.getTaData();
            String cellKey = pane.getId();
            
            // AND TOGGLE THE OFFICE HOURS IN THE CLICKED CELL
            jTPS_Transaction toggleUR = new TAtoggleUR(taName, cellKey, data);
            jTPS.addTransaction(toggleUR);
            
            // WE'VE CHANGED STUFF
            markWorkAsEdited();
        }
    }
    
    void handleGridCellMouseExited(Pane pane) {
        String cellKey = pane.getId();
        CSGData d = (CSGData)app.getDataComponent();
        TAData data=d.getTaData();
        
        int column = Integer.parseInt(cellKey.substring(0, cellKey.indexOf("_")));
        int row = Integer.parseInt(cellKey.substring(cellKey.indexOf("_") + 1));
        TAWorkspace workspace = (TAWorkspace)app.getWorkspaceComponent();

        Pane mousedOverPane = workspace.getTACellPane(data.getCellKey(column, row));
        mousedOverPane.getStyleClass().clear();
        mousedOverPane.getStyleClass().add(CLASS_OFFICE_HOURS_GRID_TA_CELL_PANE);

        // THE MOUSED OVER COLUMN HEADER
        Pane headerPane = workspace.getOfficeHoursGridDayHeaderPanes().get(data.getCellKey(column, 0));
        headerPane.getStyleClass().remove(CLASS_HIGHLIGHTED_GRID_ROW_OR_COLUMN);

        // THE MOUSED OVER ROW HEADERS
        headerPane = workspace.getOfficeHoursGridTimeCellPanes().get(data.getCellKey(0, row));
        headerPane.getStyleClass().remove(CLASS_HIGHLIGHTED_GRID_ROW_OR_COLUMN);
        headerPane = workspace.getOfficeHoursGridTimeCellPanes().get(data.getCellKey(1, row));
        headerPane.getStyleClass().remove(CLASS_HIGHLIGHTED_GRID_ROW_OR_COLUMN);
        
        // AND NOW UPDATE ALL THE CELLS IN THE SAME ROW TO THE LEFT
        for (int i = 2; i < column; i++) {
            cellKey = data.getCellKey(i, row);
            Pane cell = workspace.getTACellPane(cellKey);
            cell.getStyleClass().remove(CLASS_HIGHLIGHTED_GRID_ROW_OR_COLUMN);
            cell.getStyleClass().add(CLASS_OFFICE_HOURS_GRID_TA_CELL_PANE);
        }

        // AND THE CELLS IN THE SAME COLUMN ABOVE
        for (int i = 1; i < row; i++) {
            cellKey = data.getCellKey(column, i);
            Pane cell = workspace.getTACellPane(cellKey);
            cell.getStyleClass().remove(CLASS_HIGHLIGHTED_GRID_ROW_OR_COLUMN);
            cell.getStyleClass().add(CLASS_OFFICE_HOURS_GRID_TA_CELL_PANE);
        }
    }

    void handleGridCellMouseEntered(Pane pane) {
        String cellKey = pane.getId();
        CSGData d= (CSGData)app.getDataComponent();
        TAData data=d.getTaData();       
        int column = Integer.parseInt(cellKey.substring(0, cellKey.indexOf("_")));
        int row = Integer.parseInt(cellKey.substring(cellKey.indexOf("_") + 1));
        TAWorkspace workspace = (TAWorkspace)app.getWorkspaceComponent();
        
        // THE MOUSED OVER PANE
        Pane mousedOverPane = workspace.getTACellPane(data.getCellKey(column, row));
        mousedOverPane.getStyleClass().clear();
        mousedOverPane.getStyleClass().add(CLASS_HIGHLIGHTED_GRID_CELL);
        
        // THE MOUSED OVER COLUMN HEADER
        Pane headerPane = workspace.getOfficeHoursGridDayHeaderPanes().get(data.getCellKey(column, 0));
        headerPane.getStyleClass().add(CLASS_HIGHLIGHTED_GRID_ROW_OR_COLUMN);
        
        // THE MOUSED OVER ROW HEADERS
        headerPane = workspace.getOfficeHoursGridTimeCellPanes().get(data.getCellKey(0, row));
        headerPane.getStyleClass().add(CLASS_HIGHLIGHTED_GRID_ROW_OR_COLUMN);
        headerPane = workspace.getOfficeHoursGridTimeCellPanes().get(data.getCellKey(1, row));
        headerPane.getStyleClass().add(CLASS_HIGHLIGHTED_GRID_ROW_OR_COLUMN);
        
        // AND NOW UPDATE ALL THE CELLS IN THE SAME ROW TO THE LEFT
        for (int i = 2; i < column; i++) {
            cellKey = data.getCellKey(i, row);
            Pane cell = workspace.getTACellPane(cellKey);
            cell.getStyleClass().add(CLASS_HIGHLIGHTED_GRID_ROW_OR_COLUMN);
        }

        // AND THE CELLS IN THE SAME COLUMN ABOVE
        for (int i = 1; i < row; i++) {
            cellKey = data.getCellKey(column, i);
            Pane cell = workspace.getTACellPane(cellKey);
            cell.getStyleClass().add(CLASS_HIGHLIGHTED_GRID_ROW_OR_COLUMN);
        }
    }
    
    public void Undo(){
        jTPS.undoTransaction();
        markWorkAsEdited();
    }
    public void Redo(){
        jTPS.doTransaction();
        markWorkAsEdited();
    }
    
    public void changeTime(){
        CSGData d=(CSGData)app.getDataComponent();
        TAData data = d.getTaData();
        TAWorkspace workspace = (TAWorkspace)app.getWorkspaceComponent();
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        ComboBox comboBox1 = workspace.getOfficeHour(true);
        ComboBox comboBox2 = workspace.getOfficeHour(false);
        int startTime = data.getStartHour();
        int endTime = data.getEndHour();
        int newStartTime = comboBox1.getSelectionModel().getSelectedIndex();
        int newEndTime = comboBox2.getSelectionModel().getSelectedIndex();
        if(newStartTime > endTime || newEndTime < startTime){
            comboBox1.getSelectionModel().select(startTime);
            comboBox2.getSelectionModel().select(endTime);
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show(props.getProperty(TAManagerProp.START_OVER_END_TITLE.toString()), props.getProperty(TAManagerProp.START_OVER_END_MESSAGE.toString()));
            return;
        }
        ArrayList<TimeSlot> officeHours = TimeSlot.buildOfficeHoursList(data);
        if(officeHours.isEmpty()){
            workspace.getOfficeHoursGridPane().getChildren().clear();
            data.initHours("" + newStartTime, "" + newEndTime);
        }
        String firsttime = officeHours.get(0).getTime();
        int firsthour = Integer.parseInt(firsttime.substring(0, firsttime.indexOf('_')));
        if(firsttime.contains("pm"))
            firsthour += 12;
        if(firsttime.contains("12"))
            firsthour -= 12;
        String lasttime = officeHours.get(officeHours.size() - 1).getTime();
        int lasthour = Integer.parseInt(lasttime.substring(0, lasttime.indexOf('_')));
        if(lasttime.contains("pm"))
            lasthour += 12;
        if(lasttime.contains("12"))
            lasthour -= 12;
        if(firsthour < newStartTime || lasthour + 1 > newEndTime){
            AppYesNoCancelDialogSingleton yesNoDialog = AppYesNoCancelDialogSingleton.getSingleton();
            yesNoDialog.show(props.getProperty(TAManagerProp.OFFICE_HOURS_REMOVED_TITLE.toString()), props.getProperty(TAManagerProp.OFFICE_HOURS_REMOVED_MESSAGE).toString());
            String selection = yesNoDialog.getSelection();
            if (!selection.equals(AppYesNoCancelDialogSingleton.YES)){
                comboBox1.getSelectionModel().select(startTime);
                comboBox2.getSelectionModel().select(endTime);
                return;
            }
        }
        
        jTPS_Transaction changeTimeUR = new TAhourschangeUR(app);
        jTPS.addTransaction(changeTimeUR);
        
        markWorkAsEdited();
    }
    
    public void changeExistTA(){
        TAWorkspace workspace = (TAWorkspace)app.getWorkspaceComponent();
        TableView taTable = workspace.getTATable();
        Object selectedItem = taTable.getSelectionModel().getSelectedItem();
        CSGData d=(CSGData)app.getDataComponent();
        TAData data = d.getTaData();
        TeachingAssistant ta = (TeachingAssistant)selectedItem;
        String name = ta.getName();
        String newName = workspace.getNameTextField().getText();
        String newEmail = workspace.getEmailTextField().getText();
        jTPS_Transaction replaceTAUR = new TAReplaceUR(app);
        jTPS.addTransaction(replaceTAUR);
        markWorkAsEdited();
    }
    
    public void loadTAtotext(){
        TAWorkspace workspace = (TAWorkspace)app.getWorkspaceComponent();
        TableView taTable = workspace.getTATable();
        Object selectedItem = taTable.getSelectionModel().getSelectedItem();
        if(selectedItem != null){
            TeachingAssistant ta = (TeachingAssistant)selectedItem;
            String name = ta.getName();
            String email = ta.getEmail();
            workspace.getNameTextField().setText(name);
            workspace.getEmailTextField().setText(email);
        }
    }
    
    public void loadRecitationToText(){
        TAWorkspace workspace = (TAWorkspace)app.getWorkspaceComponent();
        TableView recitationTable=workspace.getRecitationTable();
        Object selectedItem=recitationTable.getSelectionModel().getSelectedItem();
        if(selectedItem != null){
            Recitation recitation=(Recitation)selectedItem;
            String section=recitation.getSection();
            String instructor=recitation.getInstructor();
            String daytime=recitation.getDaytime();
            String location=recitation.getLocation();
            String ta1=recitation.getTa1();
            String ta2=recitation.getTa2();    
            workspace.getSectionField().setText(section);
            workspace.getInstructorField().setText(instructor);
            workspace.getLocation().setText(location);
            workspace.getDaytimeField().setText(daytime);
            workspace.getSuTA1Box().setValue(ta1);
            workspace.getSuTA2Box().setValue(ta2);
        }
        
    }
    public void loadScheduleToText(){
        TAWorkspace workspace = (TAWorkspace)app.getWorkspaceComponent();
        TableView siTable=workspace.getSiTable();
        Object selectedItem = siTable.getSelectionModel().getSelectedItem();
        if(selectedItem != null){
            Schedule schedule=(Schedule)selectedItem;
            String type=schedule.getType();
            String date=schedule.getDate();
            
            String time=schedule.getTime();
            String title=schedule.getTitle();
            String topic=schedule.getTopic();
            String link=schedule.getLink();
            String criteria=schedule.getCriteria();
            
            String[] d=date.split("/");
            String month=d[0];
            String day=d[1];
            String year=d[2];
            int m=Integer.valueOf(month);
            int da=Integer.valueOf(day);
            int y=Integer.valueOf(year);
           
            workspace.getTypeBox().setValue(type);
            
            LocalDate ld=LocalDate.of(y,m,da);
            workspace.getDatePicker().setValue(ld);
            workspace.getTimeText().setText(time);
            workspace.getTTextField().setText(title);
            workspace.getTopicText().setText(topic);
            workspace.getLinkField().setText(link);
            workspace.getCriteriaField().setText(criteria);
        }
    }
    public void loadTeamtoText(){
        TAWorkspace workspace = (TAWorkspace)app.getWorkspaceComponent();
        TableView teamTable=workspace.getTTable();
        Object selectedItem = teamTable.getSelectionModel().getSelectedItem();
        if(selectedItem != null){
            Team team=(Team)selectedItem;
            String name=team.getName();
            String link=team.getLink();
            String colorred=team.getRed(team.getColor());
            String colorgreen=team.getGreen(team.getColor());
            String colorblue=team.getBlue(team.getColor());
            String textred=team.getRed(team.getTextcolor());
           
            String textgreen=team.getGreen(team.getTextcolor());
            String textblue=team.getBlue(team.getTextcolor());

            workspace.getNF().setText(name);
            workspace.getLTF().setText(link);
            workspace.getColorPicker().setValue(Color.rgb(Integer.valueOf(colorred), Integer.valueOf(colorgreen), Integer.valueOf(colorblue)));
            workspace.getTextPicker().setValue(Color.rgb(Integer.valueOf(textred), Integer.valueOf(textgreen), Integer.valueOf(textblue)));
        }
        
    }
    public void loadStudentToText(){
        TAWorkspace workspace = (TAWorkspace)app.getWorkspaceComponent();
        TableView studentTable=workspace.getSTable();
        Object selectedItem = studentTable.getSelectionModel().getSelectedItem();
        if(selectedItem != null){
            Student student=(Student)selectedItem;
            String firstname=student.getFirstname();
            String lastname=student.getLastname();
            String role=student.getRole();
            String team=student.getTeam();
            workspace.getFnTF().setText(firstname);
            workspace.getLnTF().setText(lastname);
            workspace.getRTF().setText(role);
            workspace.getTBox().setValue(team);
            
        }
        
        
    }
    
    public void addRecitation(){
        
        CSGData d=(CSGData)app.getDataComponent();
        RecitationData data =d.getRdData();
        TAWorkspace workspace = (TAWorkspace)app.getWorkspaceComponent();
        TextField sectionTextField = workspace.getSectionField();
        TextField instructorTextField = workspace.getInstructorField();
        TextField daytimeField=workspace.getDaytimeField();
        TextField locationField=workspace.getLocation();
        ComboBox ta1B=workspace.getSuTA1Box();
        ComboBox ta2B=workspace.getSuTA2Box();
        
        String section = sectionTextField.getText();
        String instuctor = instructorTextField.getText();
        String daytime=daytimeField.getText();
        String location=locationField.getText();
        String ta1=(String) ta1B.getValue();
        String ta2=(String) ta2B.getValue();
        
        
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        
        if (section.isEmpty()) {
	    AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
	    dialog.show(props.getProperty(MISSING_RECITATION_SECTION_TITLE), props.getProperty(MISSING_RECITATION_SECTION_MESSAGE));            
        }
        else if(daytime.isEmpty()){
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
	    dialog.show(props.getProperty(MISSING_RECITATION_DAYTIME_TITLE), props.getProperty(MISSING_RECITATION_DAYTIME_MESSAGE));
        }
        else if(location.isEmpty()){
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
	    dialog.show(props.getProperty(MISSING_RECITATION_LOCATION_TITLE), props.getProperty(MISSING_RECITATION_LOCATION_MESSAGE));
        }
        else if (data.containsRecitation(section)) {
	    AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
	    dialog.show(props.getProperty(RECITATION_SECTION_NOT_UNIQUE_TITLE), props.getProperty(RECITATION_SECTION_NOT_UNIQUE_MESSAGE));                                    
        }
        else{
            
            jTPS_Transaction addrecitation= new AddRecitation_Transaction(app,section,instuctor,daytime,location,ta1,ta2);
            jTPS.addTransaction(addrecitation);
            //data.addRecitation(section, instuctor, daytime, location, ta1, ta2);
            clearRecitationField();
        }
        
        
            
    }
    
    
    public void addSchedule(){
        CSGData d=(CSGData)app.getDataComponent();
        ScheduleData data =d.getSdData();
        
        TAWorkspace workspace = (TAWorkspace)app.getWorkspaceComponent();
        ComboBox typeBox=workspace.getTypeBox();
        DatePicker dateP=workspace.getDatePicker();
        TextField timeField=workspace.getTimeText();
        TextField titleField=workspace.getTTextField();
        TextField topicField=workspace.getTopicText();
        TextField linkField=workspace.getLinkField();
        TextField criteriaField=workspace.getCriteriaField();
        
        String type=(String)typeBox.getValue();
        String date=dateP.getEditor().getText();
        String time=timeField.getText();
        String title=titleField.getText();
        String topic=topicField.getText();
        String link=linkField.getText();
        String criteria=criteriaField.getText();
        
        PropertiesManager props = PropertiesManager.getPropertiesManager();    
        if (type==null) {
	    AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
	    dialog.show(props.getProperty(MISSING_SCHEDULE_TYPE_TITLE), props.getProperty(MISSING_SCHEDULE_TYPE_MESSAGE));            
        }
        
        else if(date.isEmpty()){
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
	    dialog.show(props.getProperty(MISSING_SCHEDULE_DATE_TITLE), props.getProperty(MISSING_SCHEDULE_DATE_MESSAGE));  
        }
        
        else{
            
            
            jTPS_Transaction addschedule= new AddSchedule_Transaction(app,type,date,time,title,topic,link,criteria);
            jTPS.addTransaction(addschedule);
            //data.addSchedules(type, date, time, title, topic, link, criteria);
            
            clearScheduleField();
        }
           
    }
    
    public void addTeam(){
        CSGData d=(CSGData)app.getDataComponent();
        ProjectData data =d.getPdData();
        TAWorkspace workspace = (TAWorkspace)app.getWorkspaceComponent();
        
        TextField nameField=workspace.getNF();
        ColorPicker colorPicker=workspace.getColorPicker();
        ColorPicker textPicker=workspace.getTextPicker();
        TextField linkField=workspace.getLTF();
        
        String name=nameField.getText();
        String link=linkField.getText();
      
        String c=colorPicker.getValue().toString();
        String color=c.substring(2, 8);
     
        String  t=textPicker.getValue().toString();
        String textcolor=t.substring(2,8);
        
        PropertiesManager props = PropertiesManager.getPropertiesManager();    
        if (name==null) {
	    AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
	    dialog.show(props.getProperty(MISSING_TEAM_NAME_TITLE), props.getProperty(MISSING_TEAM_NAME_MESSAGE));            
        }
        else if(color==null){
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
	    dialog.show(props.getProperty(MISSING_TEAM_COLOR_TITLE), props.getProperty(MISSING_TEAM_COLOR_MESSAGE)); 
        }
        else if(textcolor==null){
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
	    dialog.show(props.getProperty(MISSING_TEAM_TEXTCOLOR_TITLE), props.getProperty(MISSING_TEAM_TEXTCOLOR_MESSAGE)); 
        }
        else if(link==null){
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
	    dialog.show(props.getProperty(MISSING_TEAM_LINK_TITLE), props.getProperty(MISSING_TEAM_LINK_MESSAGE)); 
        }
        
        else{
            
            jTPS_Transaction addteam= new AddTeam_Transaction(app,name,color,textcolor,link);
            jTPS.addTransaction(addteam);
            //data.addTeam(name, color, textcolor, link);
            //workspace.getTeamOption().add(name);
            clearTeamField();
        }
    }
    
    public void addStudent(){
        
        CSGData d=(CSGData)app.getDataComponent();
        ProjectData data =d.getPdData();
        TAWorkspace workspace = (TAWorkspace)app.getWorkspaceComponent();
        
        TextField fnField=workspace.getFnTF();
        TextField lnField=workspace.getLnTF();
        
        ComboBox teamBox=workspace.getTBox();
        TextField roleField =workspace.getRTF();
        String firstname=fnField.getText();
        String lastname=lnField.getText();
        
        String team=(String)teamBox.getValue();
        String role=roleField.getText();
        
        PropertiesManager props = PropertiesManager.getPropertiesManager();    
        if (firstname.equals("")) {
	    AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
	    dialog.show(props.getProperty(MISSING_STUDENT_FIRSTNAME_TITLE), props.getProperty(MISSING_STUDENT_FIRSTNAME_MESSAGE));            
        }
        else if(lastname.equals("")){
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
	    dialog.show(props.getProperty(MISSING_STUDENT_LASTNAME_TITLE), props.getProperty(MISSING_STUDENT_LASTNAME_MESSAGE)); 
        }
        else if(team==null){
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
	    dialog.show(props.getProperty(MISSING_STUDENT_TEAM_TITLE), props.getProperty(MISSING_STUDENT_TEAM_MESSAGE)); 
        }
        else if(role.equals("")){
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
	    dialog.show(props.getProperty(MISSING_STUDENT_ROLE_TITLE), props.getProperty(MISSING_STUDENT_ROLE_MESSAGE)); 
        }
        else if(data.containsStudent(firstname, lastname)){
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
	    dialog.show(props.getProperty(STUDENT_FIRST_LAST_NAME_NOT_UNIQUE_TITLE), props.getProperty(STUDENT_FIRST_LAST_NAME_NOT_UNIQUE_MESSAGE)); 
        }
        else{
            
            jTPS_Transaction addstudent= new AddStudent_Transaction(app,firstname,lastname,team,role);
            jTPS.addTransaction(addstudent);
            //data.addStudent(firstname, lastname, team, role);
            clearStudentField();
        }
        
        
    }
    
    public void changeExistRecitation(){
        TAWorkspace workspace = (TAWorkspace)app.getWorkspaceComponent();
        TableView recitationTable = workspace.getRecitationTable();
        Object selectedItem = recitationTable.getSelectionModel().getSelectedItem();
        CSGData d=(CSGData)app.getDataComponent();
        RecitationData data = d.getRdData();
        Recitation recitation=(Recitation)selectedItem;         
    
        String newSection = workspace.getSectionField().getText();
        String newInstructor = workspace.getInstructorField().getText();
        String newDaytime=workspace.getDaytimeField().getText();
        String newLocation=workspace.getLocation().getText();
        String newTA1=(String)workspace.getSuTA1Box().getValue();
        String newTA2=(String)workspace.getSuTA2Box().getValue();
        
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        
        
        if (newSection.isEmpty()) {
	    AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
	    dialog.show(props.getProperty(MISSING_RECITATION_SECTION_TITLE), props.getProperty(MISSING_RECITATION_SECTION_MESSAGE));            
        }
        else if(newDaytime.isEmpty()){
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
	    dialog.show(props.getProperty(MISSING_RECITATION_DAYTIME_TITLE), props.getProperty(MISSING_RECITATION_DAYTIME_MESSAGE));
        }
        else if(newLocation.isEmpty()){
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
	    dialog.show(props.getProperty(MISSING_RECITATION_LOCATION_TITLE), props.getProperty(MISSING_RECITATION_LOCATION_MESSAGE));
        }
        else{
//            recitation.setSection(newSection);
//            recitation.setInstructor(newInstructor);
//            recitation.setDaytime(newDaytime);
//            recitation.setLocation(newLocation);
//            recitation.setTA1(newTA1);
//            recitation.setTA2(newTA2);
            jTPS_Transaction editrecitation= new EditRecitation_Transaction(app,recitation.getSection(),recitation.getInstructor(),
                    recitation.getDaytime(),recitation.getLocation(),recitation.getTa1(),recitation.getTa2(),recitation);
            jTPS.addTransaction(editrecitation);


            workspace.getRecitationTable().refresh();
            clearRecitationField();
            markWorkAsEdited();
        }
      
        
        
        //data.addRecitation(newSection, newInstructor, newDaytime, newLocation, newTA1, newTA2);
        
        
        
        //jTPS_Transaction replaceTAUR = new TAReplaceUR(app);
        //jTPS.addTransaction(replaceTAUR);
        
    }
    
    
    
    public void changeExistSchedule(){
        TAWorkspace workspace = (TAWorkspace)app.getWorkspaceComponent();
        TableView scheduleTable = workspace.getSiTable();
        Object selectedItem = scheduleTable.getSelectionModel().getSelectedItem();
        CSGData d=(CSGData)app.getDataComponent();
        ScheduleData data = d.getSdData();
        Schedule schedule=(Schedule)selectedItem;
       
             
        String newType=(String)workspace.getTypeBox().getValue();
        String newDate=workspace.getDatePicker().getEditor().getText();
        String newTime = workspace.getTimeText().getText();
        String newTitle=workspace.getTTextField().getText();
        String newTopic=workspace.getTopicText().getText();
        String newLink=workspace.getLinkField().getText();
        String newCriteria=workspace.getCriteriaField().getText();
        
        PropertiesManager props = PropertiesManager.getPropertiesManager(); 
        
        if (newType==null) {
	    AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
	    dialog.show(props.getProperty(MISSING_SCHEDULE_TYPE_TITLE), props.getProperty(MISSING_SCHEDULE_TYPE_MESSAGE));            
        }
        
        else if(newDate.isEmpty()){
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
	    dialog.show(props.getProperty(MISSING_SCHEDULE_DATE_TITLE), props.getProperty(MISSING_SCHEDULE_DATE_MESSAGE));  
        }
        
        else{
//            schedule.setType(newType);
//            schedule.setDate(newDate);
//            schedule.setTime(newTime);
//            schedule.setTitle(newTitle);
//            schedule.setTopic(newTopic);
//            schedule.setLink(newLink);
//            schedule.setCriteria(newCriteria);

            jTPS_Transaction editschedule= new EditSchedule_Transaction(app,schedule.getType(),schedule.getDate(),schedule.getTime(),schedule.getTitle(),schedule.getTopic(),schedule.getLink(),schedule.getCriteria(),schedule);
            jTPS.addTransaction(editschedule);
            workspace.getSiTable().refresh();
            clearScheduleField();
            markWorkAsEdited();
        }
        
    }
    
    public void changeExistTeam(){
            
        TAWorkspace workspace = (TAWorkspace)app.getWorkspaceComponent();
        TableView teamTable = workspace.getTTable();
        Object selectedItem = teamTable.getSelectionModel().getSelectedItem();
        CSGData d=(CSGData)app.getDataComponent();
        ProjectData data = d.getPdData();
        Team team=(Team)selectedItem;
        
//        String oldName=team.getName();
//        data.removeTeam(oldName);
//        
//        
//        String newName=workspace.getNF().getText();
//        Color color=workspace.getColorPicker().getValue();
//        double r=color.getRed();
//        int red=(int) (r*255);
//        double g=color.getGreen();
//        int green=(int)(g*255);
//        double b=color.getBlue();
//        int blue=(int)(b*255);
//        String newColor=String.format("#%02x%02x%02x", red,green,blue);
//        
//        String newLink=workspace.getLTF().getText();
        TextField nameField=workspace.getNF();
        ColorPicker colorPicker=workspace.getColorPicker();
        ColorPicker textPicker=workspace.getTextPicker();
        TextField linkField=workspace.getLTF();
        
        String name=nameField.getText();
        String link=linkField.getText();
        
        
        String c=colorPicker.getValue().toString();
        String color=c.substring(2, 8);
     
        String  t=textPicker.getValue().toString();
        String textcolor=t.substring(2,8);
        
        PropertiesManager props = PropertiesManager.getPropertiesManager();    
        if (name==null) {
	    AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
	    dialog.show(props.getProperty(MISSING_TEAM_NAME_TITLE), props.getProperty(MISSING_TEAM_NAME_MESSAGE));            
        }
        else if(color==null){
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
	    dialog.show(props.getProperty(MISSING_TEAM_COLOR_TITLE), props.getProperty(MISSING_TEAM_COLOR_MESSAGE)); 
        }
        else if(textcolor==null){
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
	    dialog.show(props.getProperty(MISSING_TEAM_TEXTCOLOR_TITLE), props.getProperty(MISSING_TEAM_TEXTCOLOR_MESSAGE)); 
        }
        else if(link==null){
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
	    dialog.show(props.getProperty(MISSING_TEAM_LINK_TITLE), props.getProperty(MISSING_TEAM_LINK_MESSAGE)); 
        }
        
        else{
//            team.setName(name);
//            team.setColor(color);
//            team.setTextcolor(textcolor);
//            team.setLink(link);

            jTPS_Transaction editteam= new EditTeam_Transaction(app,team.getName(),team.getColor(),team.getTextcolor(),team.getLink(),team,name);
            jTPS.addTransaction(editteam);
            workspace.getTTable().refresh();
            clearTeamField();
        }
        
    }
    
    public void changeExistStudent(){
        
        TAWorkspace workspace = (TAWorkspace)app.getWorkspaceComponent();
        TableView studentTable = workspace.getSTable();
        Object selectedItem = studentTable.getSelectionModel().getSelectedItem();
        CSGData d=(CSGData)app.getDataComponent();
        ProjectData data = d.getPdData();
        Student student=(Student)selectedItem;
        
        String firstname=workspace.getFnTF().getText();
        String lastname=workspace.getLnTF().getText();
        String team=(String)workspace.getTBox().getValue();
        String role=workspace.getRTF().getText();
        
        
        PropertiesManager props = PropertiesManager.getPropertiesManager();    
        if (firstname.equals("")) {
	    AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
	    dialog.show(props.getProperty(MISSING_STUDENT_FIRSTNAME_TITLE), props.getProperty(MISSING_STUDENT_FIRSTNAME_MESSAGE));            
        }
        else if(lastname.equals("")){
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
	    dialog.show(props.getProperty(MISSING_STUDENT_LASTNAME_TITLE), props.getProperty(MISSING_STUDENT_LASTNAME_MESSAGE)); 
        }
        else if(team==null){
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
	    dialog.show(props.getProperty(MISSING_STUDENT_TEAM_TITLE), props.getProperty(MISSING_STUDENT_TEAM_MESSAGE)); 
        }
        else if(role.equals("")){
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
	    dialog.show(props.getProperty(MISSING_STUDENT_ROLE_TITLE), props.getProperty(MISSING_STUDENT_ROLE_MESSAGE)); 
        }
        else if(data.containsStudent(firstname, lastname)){
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
	    dialog.show(props.getProperty(STUDENT_FIRST_LAST_NAME_NOT_UNIQUE_TITLE), props.getProperty(STUDENT_FIRST_LAST_NAME_NOT_UNIQUE_MESSAGE)); 
        }
        else{
           
            jTPS_Transaction editstudent= new EditStudent_Transaction(app,student.getFirstname(),student.getLastname(),student.getTeam(),student.getRole(),student);
            jTPS.addTransaction(editstudent);
            
//            student.setFirstname(firstname);
//            student.setLastname(lastname);
//            student.setRole(role);
//            student.setTeam(team);
            workspace.getSTable().refresh();
            clearStudentField();
        }
        
  
    }
    
    
    public void deleteTA(){
        // GET THE TABLE
        TAWorkspace workspace = (TAWorkspace)app.getWorkspaceComponent();
        TableView taTable = workspace.getTATable();
        
        
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        AppYesNoCancelDialogSingleton yesNoDialog = AppYesNoCancelDialogSingleton.getSingleton();
        yesNoDialog.show(props.getProperty(TAManagerProp.TA_REMOVE_TITLE.toString()), props.getProperty(TAManagerProp.TA_REMOVE_TITLE).toString());
        String selection = yesNoDialog.getSelection();
        
        if(selection.equals(AppYesNoCancelDialogSingleton.YES)){
            // IS A TA SELECTED IN THE TABLE?
            Object selectedItem = taTable.getSelectionModel().getSelectedItem();
            if (selectedItem != null) {
             // GET THE TA AND REMOVE IT
                TeachingAssistant ta = (TeachingAssistant)selectedItem;
                String taName = ta.getName();
                CSGData d=(CSGData)app.getDataComponent();  
                TAData data = d.getTaData();  
                jTPS_Transaction deletUR = new TAdeletUR(app, taName);
                jTPS.addTransaction(deletUR);
                
                // AND BE SURE TO REMOVE ALL THE TA'S OFFICE HOURS
                // WE'VE CHANGED STUFF
            
                ObservableList<Recitation> recitations=d.getRdData().getRecitations();
                
                for(Recitation rec:recitations){
                    if((rec.getTa1()!=null)&&(rec.getTa1().equals(taName))){
                        rec.setTA1("");
                    }
                    if((rec.getTa1()!=null)&&(rec.getTa2().equals(taName))){
                        rec.setTA2("");
                    }
                }
                workspace.getRecitationTable().refresh();    
                workspace.getTeacher1().remove(taName);
                markWorkAsEdited();
            }
        }
        
        
    }
    
    
    
    public void deleteRecitation(){
        TAWorkspace workspace = (TAWorkspace)app.getWorkspaceComponent();
        TableView recitationTable = workspace.getRecitationTable();
        
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        AppYesNoCancelDialogSingleton yesNoDialog = AppYesNoCancelDialogSingleton.getSingleton();
        yesNoDialog.show(props.getProperty(TAManagerProp.RECITATION_REMOVE_TITLE.toString()), props.getProperty(TAManagerProp.RECITATION_REMOVE_MESSAGE).toString());
        String selection = yesNoDialog.getSelection();
        if(selection.equals(AppYesNoCancelDialogSingleton.YES)){
            Object selectedItem = recitationTable.getSelectionModel().getSelectedItem();
            if (selectedItem != null) {
                // GET THE TA AND REMOVE IT
                Recitation recitation = (Recitation)selectedItem;
                String section = recitation.getSection();
                String instuctor=recitation.getInstructor();
                String daytime=recitation.getDaytime();
                String location=recitation.getLocation();
                String ta1=recitation.getTa1();
                String ta2=recitation.getTa2();
                CSGData d=(CSGData)app.getDataComponent();
                
                RecitationData data = d.getRdData();
                
                jTPS_Transaction deleterecitation= new DeleteRecitation_Transaction(app,section,instuctor,daytime,location,ta1,ta2);
                jTPS.addTransaction(deleterecitation);
                
                
                //data.removeRecitation(section);
                clearRecitationField();
            
                
                //jTPS_Transaction deletUR = new TAdeletUR(app, taName);
                //jTPS.addTransaction(deletUR);
                
                // AND BE SURE TO REMOVE ALL THE TA'S OFFICE HOURS
                // WE'VE CHANGED STUFF
                markWorkAsEdited();
            }   
        }
        
        
        
    }
    
    public void deleteSchedule(){
        
        TAWorkspace workspace = (TAWorkspace)app.getWorkspaceComponent();
        TableView scheduleTable = workspace.getSiTable();
        
        
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        AppYesNoCancelDialogSingleton yesNoDialog = AppYesNoCancelDialogSingleton.getSingleton();
        yesNoDialog.show(props.getProperty(TAManagerProp.SCHEDULE_REMOVE_TITLE.toString()), props.getProperty(TAManagerProp.SCHEDULE_REMOVE_MESSAGE).toString());
        String selection = yesNoDialog.getSelection();
        
        if(selection.equals(AppYesNoCancelDialogSingleton.YES)){
            Object selectedItem = scheduleTable.getSelectionModel().getSelectedItem();
            if (selectedItem != null) {
                // GET THE TA AND REMOVE IT
                Schedule schedule = (Schedule)selectedItem;
                String title = schedule.getTitle();
 
                CSGData d=(CSGData)app.getDataComponent();
                
                
                
                jTPS_Transaction deleteschedule= new DeleteSchedule_Transaction(app,schedule.getType(),schedule.getDate(),schedule.getTime(),schedule.getTitle(),schedule.getTopic(),schedule.getLink(),schedule.getCriteria());
                jTPS.addTransaction(deleteschedule);
                
                
//                ScheduleData data = d.getSdData();
//                data.removeSchedule(title);
            
                
                // AND BE SURE TO REMOVE ALL THE TA'S OFFICE HOURS
                // WE'VE CHANGED STUFF
                markWorkAsEdited();
                clearScheduleField();
            }       
        }
        
    }
    
    
    public void deleteTeam(){
        TAWorkspace workspace = (TAWorkspace)app.getWorkspaceComponent();
        TableView teamTable = workspace.getTTable();
        
        
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        AppYesNoCancelDialogSingleton yesNoDialog = AppYesNoCancelDialogSingleton.getSingleton();
        yesNoDialog.show(props.getProperty(TAManagerProp.TEAM_REMOVE_TITLE.toString()), props.getProperty(TAManagerProp.TEAM_REMOVE_TITLE).toString());
        String selection = yesNoDialog.getSelection();
        
        if(selection.equals(AppYesNoCancelDialogSingleton.YES)){
           Object selectedItem = teamTable.getSelectionModel().getSelectedItem();
            if(selectedItem!=null){
                Team team = (Team)selectedItem;
                String name = team.getName();
                CSGData d=(CSGData)app.getDataComponent();   
                ProjectData data = d.getPdData();
                
                ObservableList<Student> students=d.getPdData().getStudents();
                ObservableList<Student> copy = FXCollections.observableArrayList();
                
                for(Student stu:students){
                    copy.add(stu);
                }
               
                
                jTPS_Transaction deleteteam= new DeleteTeam_Transaction(app,name,team.getColor(),team.getTextcolor(),team.getLink(),copy);
                jTPS.addTransaction(deleteteam);
                
                workspace.getSTable().refresh();
                //data.removeTeam(name);
                
                
                
//                Iterator<Student> iter = students.iterator();
//                while (iter.hasNext()) {
//                    Student stu = iter.next();
//                    if (stu.getTeam().equals(name))
//                        iter.remove();
//                }          
            } 
        }
        
        
    }
    
    public void deleteStudents(){
        TAWorkspace workspace = (TAWorkspace)app.getWorkspaceComponent();
        TableView studentTable = workspace.getSTable();
        
        
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        AppYesNoCancelDialogSingleton yesNoDialog = AppYesNoCancelDialogSingleton.getSingleton();
        yesNoDialog.show(props.getProperty(TAManagerProp.STUDENT_REMOVE_TITLE.toString()), props.getProperty(TAManagerProp.STUDENT_REMOVE_TITLE).toString());
        String selection = yesNoDialog.getSelection();
        
        if(selection.equals(AppYesNoCancelDialogSingleton.YES)){
            Object selectedItem = studentTable.getSelectionModel().getSelectedItem();
            if(selectedItem!=null){
                Student student = (Student)selectedItem;
                String firstname = student.getFirstname() ;
                String lastname=student.getLastname();
            
                jTPS_Transaction deletestudent= new DeleteStudent_Transaction(app,firstname,lastname,student.getTeam(),student.getRole());
                jTPS.addTransaction(deletestudent);
                
//                CSGData d=(CSGData)app.getDataComponent();  
//                ProjectData data = d.getPdData();
//                data.removeStudent(firstname, lastname);
//                clearStudentField();
            }
        }
        
        
    }
    
    public void clearRecitationField(){
        TAWorkspace workspace = (TAWorkspace)app.getWorkspaceComponent();
        
        workspace.getSectionField().clear();
        workspace.getInstructorField().clear();
        workspace.getDaytimeField().clear();
        workspace.getLocation().clear();
        workspace.getSuTA1Box().getSelectionModel().select(null);
        workspace.getSuTA2Box().getSelectionModel().select(null);
    }
    
    public void clearScheduleField(){
        TAWorkspace workspace = (TAWorkspace)app.getWorkspaceComponent();
        
        workspace.getTypeBox().getSelectionModel().select(null);
        workspace.getDatePicker().setValue(null);
        workspace.getTimeText().clear();
        workspace.getTTextField().clear();
        workspace.getTopicText().clear();
        workspace.getLinkField().clear();
        workspace.getCriteriaField().clear();
    }
    
    public void clearTeamField(){
        TAWorkspace workspace = (TAWorkspace)app.getWorkspaceComponent();
        
        workspace.getNF().clear();
        workspace.getLTF().clear(); 
        workspace.getColorPicker().setValue(null);
        workspace.getTextPicker().setValue(null);
    }
    
    public void clearStudentField(){
        TAWorkspace workspace = (TAWorkspace)app.getWorkspaceComponent();
        
        workspace.getFnTF().clear();
        workspace.getLnTF().clear();
        workspace.getTBox().getSelectionModel().select(null);
        workspace.getRTF().clear();
   
    }
    
    public void changeStartDay(LocalDate oldDate,LocalDate newDate){
        TAWorkspace workspace = (TAWorkspace)app.getWorkspaceComponent();
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        boolean exist;   
        LocalDate startDate=workspace.getSmPicker().getValue();
        LocalDate endDate=workspace.getEfPciker().getValue();
        exist=checkExist(startDate,endDate);
        if(endDate.isBefore(startDate)){
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show(props.getProperty(TAManagerProp.START_OVER_END_TITLE.toString()), props.getProperty(TAManagerProp.START_OVER_END_MESSAGE.toString()));
            //workspace.setListen();
        
        }
        else if(exist){             
            AppYesNoCancelDialogSingleton yesNoDialog = AppYesNoCancelDialogSingleton.getSingleton();
            yesNoDialog.show(props.getProperty(TAManagerProp.CHANGE_DAY_TITLE.toString()), props.getProperty(TAManagerProp.CHANGE_DAY_MESSAGE).toString());
            String selection = yesNoDialog.getSelection();
            if(selection.equals(AppYesNoCancelDialogSingleton.NO)){
                workspace.setListenStart(false);
                workspace.getSmPicker().setValue(oldDate);
                workspace.setListenStart(true);
            }
            else
            {
                workspace.setListenStart(false);
                workspace.getSmPicker().setValue(newDate);
                workspace.setListenStart(true);
            }
        }
        
    }
    
    public void changeEndDay(LocalDate oldDate,LocalDate newDate){
        TAWorkspace workspace = (TAWorkspace)app.getWorkspaceComponent();
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        boolean exist;   
        LocalDate startDate=workspace.getSmPicker().getValue();
        LocalDate endDate=workspace.getEfPciker().getValue();
        exist=checkExist(startDate,endDate);
        if(endDate.isBefore(startDate)){
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show(props.getProperty(TAManagerProp.START_OVER_END_TITLE.toString()), props.getProperty(TAManagerProp.START_OVER_END_MESSAGE.toString()));
            //workspace.setListen();
        
        }
        else if(exist){             
            AppYesNoCancelDialogSingleton yesNoDialog = AppYesNoCancelDialogSingleton.getSingleton();
            yesNoDialog.show(props.getProperty(TAManagerProp.CHANGE_DAY_TITLE.toString()), props.getProperty(TAManagerProp.CHANGE_DAY_MESSAGE).toString());
            String selection = yesNoDialog.getSelection();
            if(selection.equals(AppYesNoCancelDialogSingleton.NO)){
                workspace.setListenEnd(false);
                workspace.getEfPciker().setValue(oldDate);
                workspace.setListenEnd(true);
            }
            else
            {
                workspace.setListenEnd(false);
                workspace.getEfPciker().setValue(newDate);
                workspace.setListenEnd(true);
            }
        }
        
    }
    
    public void changeDataDate(LocalDate oldDate,LocalDate newDate){
        TAWorkspace workspace = (TAWorkspace)app.getWorkspaceComponent();
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        boolean exist;   
        LocalDate startDate=workspace.getSmPicker().getValue();
        LocalDate endDate=workspace.getEfPciker().getValue();
        exist=checkInRange(startDate,endDate);
        
        
        if(exist){             
            AppYesNoCancelDialogSingleton yesNoDialog = AppYesNoCancelDialogSingleton.getSingleton();
            yesNoDialog.show(props.getProperty(TAManagerProp.CHANGE_DAY_TITLE.toString()), props.getProperty(TAManagerProp.CHANGE_DAY_MESSAGE).toString());
            String selection = yesNoDialog.getSelection();
            if(selection.equals(AppYesNoCancelDialogSingleton.NO)){
                workspace.setListenDate(false);
                workspace.getDatePicker().setValue(oldDate);
                workspace.setListenDate(true);
            }
            else
            {
                workspace.setListenDate(false);
                workspace.getDatePicker().setValue(newDate);
                workspace.setListenDate(true);
            }
        }
        
        
        
    }
    
    
    public boolean checkInRange(LocalDate sd,LocalDate ed){
        boolean exist=false;
        TAWorkspace workspace = (TAWorkspace)app.getWorkspaceComponent();
        LocalDate date=workspace.getDatePicker().getValue();
        if((date!=null)&&((date.isBefore(sd)||date.isAfter(ed)))){
            exist=true;
           
        }  
        return exist;
    }
    
    public boolean checkExist(LocalDate sd,LocalDate ed){
        boolean exist=false;
        CSGData d=(CSGData)app.getDataComponent();
        ObservableList<Schedule> schedules=d.getSdData().getSchedules();
                
            for(Schedule sch:schedules){
                String day=sch.getDay(sch.getDate());
                String month=sch.getMonth(sch.getDate());
                String year=sch.getYear(sch.getDate());
                LocalDate date=LocalDate.of(Integer.valueOf(year),Integer.valueOf(month),Integer.valueOf(day));
                if(date.isBefore(sd)||date.isAfter(ed)){
                    exist=true;
                    
                    return exist;
                }
     
            } 
            
        return exist;
        
    }
    
    
    public void StyleSheetOption(String path){
        TAWorkspace workspace=(TAWorkspace)app.getWorkspaceComponent();
        workspace.getStylesheetOption().clear();
        String filepath=path+"\\css";
        File file=new File(filepath);
        File[] files=file.listFiles();
        
        for(File f:files){
            String name=f.getName();
            workspace.getStylesheetOption().add(name);
        
        }
    }
    
    
    
}