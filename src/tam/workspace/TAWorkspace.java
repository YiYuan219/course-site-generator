package tam.workspace;

import djf.components.AppDataComponent;
import djf.components.AppWorkspaceComponent;
import static djf.settings.AppPropertyType.EXPORT_ICON;
import static djf.settings.AppPropertyType.EXPORT_TOOLTIP;
import static djf.settings.AppPropertyType.SAVE_UNSAVED_WORK_MESSAGE;
import static djf.settings.AppPropertyType.SAVE_UNSAVED_WORK_TITLE;
import static djf.settings.AppStartupConstants.FILE_PROTOCOL;
import static djf.settings.AppStartupConstants.PATH_IMAGES;
import djf.ui.AppMessageDialogSingleton;
import djf.ui.AppYesNoCancelDialogSingleton;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import static javafx.collections.FXCollections.observableList;
import tam.CSGApp;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.ColorPicker;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DateCell;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.SplitPane;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableColumn.CellDataFeatures;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.control.cell.CheckBoxTableCell;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.util.Callback;
import properties_manager.PropertiesManager;
import tam.TAManagerProp;
import static tam.TAManagerProp.ADD_BUTTON_TEXT;
import static tam.TAManagerProp.ADD_ICON;
import static tam.TAManagerProp.ADD_TOOLTIP;
import static tam.TAManagerProp.CHANGE_ICON;
import static tam.TAManagerProp.CHANGE_TOOLTIP;
import static tam.TAManagerProp.CLEAR_ICON;
import static tam.TAManagerProp.CLEAR_TOOLTIP;
import static tam.TAManagerProp.MISSING_TA_NAME_MESSAGE;
import static tam.TAManagerProp.MISSING_TA_NAME_TITLE;
import static tam.TAManagerProp.REMOVE_ICON;
import static tam.TAManagerProp.REMOVE_TOOLTIP;
import static tam.TAManagerProp.UPDATE_BUTTON_TEXT;
import tam.data.CSGData;
import tam.data.CourseDetail;
import tam.data.CourseDetailData;
import tam.data.Team;
import tam.data.Recitation;
import tam.data.RecitationData;
import tam.data.Schedule;
import tam.data.ScheduleData;
import tam.data.Student;
import tam.style.TAStyle;
import tam.data.TAData;
import tam.data.TeachingAssistant;
import tam.file.TimeSlot;

/**
 * This class serves as the workspace component for the TA Manager
 * application. It provides all the user interface controls in 
 * the workspace area.
 * 
 * @author Richard McKenna
 */
public class TAWorkspace extends AppWorkspaceComponent {
    
    // THIS PROVIDES US WITH ACCESS TO THE APP COMPONENTS
    CSGApp app;
    boolean add;
    boolean addrecitation;
    boolean addschedule;
    boolean addteam;
    boolean addstudent;
    boolean listenStart;
    boolean listenEnd;
    boolean listenDate;

    
    // THIS PROVIDES RESPONSES TO INTERACTIONS WITH THIS WORKSPACE
    TAController controller;

    
    //For the tabPane
    TabPane tabPane;
    Tab cdTab;
    Tab taTab;
    Tab rdTab;
    Tab pdTab;
    Tab sdTab;
    // NOTE THAT EVERY CONTROL IS PUT IN A BOX TO HELP WITH ALIGNMENT
    
    // FOR THE HEADER ON THE LEFT
    HBox tasHeaderBox;
    Label tasHeaderLabel;
    
    // FOR THE TA TABLE
    TableView<TeachingAssistant> taTable;
    TableColumn<TeachingAssistant, String> nameColumn;
    TableColumn<TeachingAssistant, String> emailColumn;
    TableColumn<TeachingAssistant, Boolean> underColumn;

    // THE TA INPUT
    HBox addBox;
    TextField nameTextField;
    TextField emailTextField;
    Button addButton;
    Button clearButton;
    Button removeButton;

    // THE HEADER ON THE RIGHT
    HBox officeHoursHeaderBox;
    Label officeHoursHeaderLabel;
    
    ObservableList<String> time_options;
    ComboBox comboBox1;
    ComboBox comboBox2;
    
    // THE OFFICE HOURS GRID
    GridPane officeHoursGridPane;
    HashMap<String, Pane> officeHoursGridTimeHeaderPanes;
    HashMap<String, Label> officeHoursGridTimeHeaderLabels;
    HashMap<String, Pane> officeHoursGridDayHeaderPanes;
    HashMap<String, Label> officeHoursGridDayHeaderLabels;
    HashMap<String, Pane> officeHoursGridTimeCellPanes;
    HashMap<String, Label> officeHoursGridTimeCellLabels;
    HashMap<String, Pane> officeHoursGridTACellPanes;
    HashMap<String, Label> officeHoursGridTACellLabels;
    
    
    
    //For the workspace of Course Detail Tab
    ObservableList stylesheetOption;
    VBox bigBox;
    GridPane courseIBox;
    VBox siteBox;
    GridPane pageBox;
   
    Label couseILabel;
    Label subjectLabel;
    ComboBox subjectBox;
    Label numberLabel;
    ComboBox numberBox;
    Label semesterLabel;
    ComboBox semesterBox;
    Label yearLabel;
    ComboBox yearBox;
    Label titleLabel;
    TextField titleField;
    Label instructionNLabel;
    TextField instructionNField;
    Label instructionHLabel;
    TextField instructionHField;
    Label exportLabel;
    Label dirLabel;
    Button changeButton;
    
    Label siteTemplate;
    Label sentence;
    Label selectTemple;
    TableView spTable;
    
    Button selectButton;
    Label sitePage;
    TableView psTable;
    TableColumn user;
    TableColumn nt;
    TableColumn fn;
    TableColumn script;
    
    Label pageLabel;
    
    Label pageStyleLabel;
    Label bannerLabel;
    ImageView yaleIV1;
    Button change1Button;
    Label leftLabel;
    ImageView yaleIV2;
    Button change2Button;
    Label rightLabel;
    ImageView csIV;
    Button change3Button;
    Label styleLabel;
    ComboBox styleCombo;
    Label noteLabel;
    Label noteContext;
    
    
    //for recitation
    VBox rbigBox;
    HBox firstBox;
    TableView recitationTable;
    GridPane addPane;
    
    Label recitationLabel;
    Button subButton;
    
    TableColumn section;
    TableColumn instructor;
    TableColumn daytime;
    TableColumn location;
    TableColumn ta1;
    TableColumn ta2;
    
    Label addeditLabel;
    Label sectionLabel;
    Label instructorLabel;
    Label daytimeLabel;
    Label locationLabel;
    Label suTA1Label;
    Label suTA2Label;
    TextField sectionField;
    TextField instructorField;
    TextField daytimeField;
    TextField locationField;
    ObservableList techera1;
    ComboBox suTA1Box;
    ComboBox suTA2Box;
    Button addupdatButton;
    Button clearB;
    
    
    //for schedule data tab
    VBox sbigBox;
    Label scheduleLabel;
    GridPane cpane;
    VBox smallBox;
    
    Label calendarLabel;
    Label smLabel;
    DatePicker smPicker;
    Label efLabel;
    DatePicker efPicker;
    
    HBox siBox;
    Label siLabel;
    Button siButton;
    TableView siTable;
    TableColumn typeC;
    TableColumn dateC;
    TableColumn titleC;
    TableColumn topicC;
    GridPane smallPane;
    Label l;
    Label typeLabel;
    ComboBox typeBox;
    Label dateLabel;
    DatePicker datePicker;
    Label timeLabel;
    TextField timeText;
    Label tLabel;
    TextField tTextField;
    Label topicLabel;
    TextField topicField;
    Label linkLabel;
    TextField linkField;
    Label criteriaLabel;
    TextField criteriaField;
    Button one;
    Button two;
    
    //for project data
    VBox pbigBox;
    Label project;
    VBox teamBox;
    VBox stuBox;
  
    HBox tpBox;
    Label teamLabel;
    Button tButton;
    TableView tTable;
    TableColumn nC;
    TableColumn cC;
    TableColumn tC;
    TableColumn lC;
    GridPane tPane;
    Label aeLable;
    Label nL;
    TextField nF;
    Label cL;
    ColorPicker colorPicker;
    Label tcL;
    ColorPicker textPicker;
    Label lL;
    TextField lTF;
    Button addP1Button;
    Button clearP1Button;
    
    HBox spBox;
    Label studentsLabel;
    Button sButton;
    TableView sTable;
    TableColumn fnC;
    TableColumn lnC;
    TableColumn teamC;
    TableColumn rC;
    Label aeLable2;
    GridPane stuPane;
    Label fnLabel;
    TextField fnTF;
    Label lnLabel;
    TextField lnTF;
    Label tpLabel;
    
    ObservableList<String> teamOption;
    ComboBox tBox;
    Label rLabel;
    TextField rTF;
    Button addP2Button;
    Button clearP2Button;
  

    /**
     * The contstructor initializes the user interface, except for
     * the full office hours grid, since it doesn't yet know what
     * the hours will be until a file is loaded or a new one is created.
     */
    public TAWorkspace(CSGApp initApp) {
        // KEEP THIS FOR LATER
        app = initApp;
        add = true;
        addrecitation=true;
        addschedule=true;
        addteam=true;
        addstudent=true;
        listenStart=true;
        listenEnd=true;
        listenDate=true;
        
        

        // WE'LL NEED THIS TO GET LANGUAGE PROPERTIES FOR OUR UI
        PropertiesManager props = PropertiesManager.getPropertiesManager();

        // INIT THE HEADER ON THE LEFT
        tasHeaderBox = new HBox();
        String tasHeaderText = props.getProperty(TAManagerProp.TAS_HEADER_TEXT.toString());
        tasHeaderLabel = new Label(tasHeaderText);
        removeButton = initButton(REMOVE_ICON.toString(),REMOVE_TOOLTIP.toString());
        tasHeaderBox.getChildren().add(tasHeaderLabel);
        tasHeaderBox.getChildren().add(removeButton);

        // MAKE THE TABLE AND SETUP THE DATA MODEL
        taTable = new TableView();
        taTable.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
        CSGData data = (CSGData) app.getDataComponent();
        ObservableList<TeachingAssistant> tableData = data.getTaData().getTeachingAssistants();
        
        underColumn = new TableColumn<TeachingAssistant, Boolean>(props.getProperty(TAManagerProp.UNDERGRAD_TA.toString()));
        underColumn.setPrefWidth(100);
        underColumn.setCellValueFactory(new Callback<CellDataFeatures<TeachingAssistant, Boolean>, ObservableValue<Boolean>>() {
 
        @Override
        public ObservableValue<Boolean> call(CellDataFeatures<TeachingAssistant, Boolean> param) {
            TeachingAssistant teaching = param.getValue();
            SimpleBooleanProperty booleanProp = new SimpleBooleanProperty(teaching.getUndercheck());
            booleanProp.addListener(new ChangeListener<Boolean>() {
                @Override
                public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue,Boolean newValue) {
                    teaching.setUndercheck(newValue);
                    app.getGUI().updateToolbarControls(false);
                    }
            });
            return booleanProp;
        }
        });
        underColumn.setCellFactory(new Callback<TableColumn<TeachingAssistant, Boolean>, TableCell<TeachingAssistant, Boolean>>() {
            @Override
            public TableCell<TeachingAssistant, Boolean> call(TableColumn<TeachingAssistant, Boolean> p) {
                CheckBoxTableCell<TeachingAssistant, Boolean> cell = new CheckBoxTableCell<TeachingAssistant, Boolean>();
                cell.setAlignment(Pos.CENTER);
                return cell; 
            }
        });
        
        
     
        taTable.getColumns().add(underColumn);
        taTable.setEditable(true);

        taTable.setItems(tableData);
        String nameColumnText = props.getProperty(TAManagerProp.NAME_COLUMN_TEXT.toString());
        String emailColumnText = props.getProperty(TAManagerProp.EMAIL_COLUMN_TEXT.toString());       
        nameColumn = new TableColumn(nameColumnText);
        emailColumn = new TableColumn(emailColumnText);
        nameColumn.setCellValueFactory(
                new PropertyValueFactory<TeachingAssistant, String>("name")
        );
        emailColumn.setCellValueFactory(
                new PropertyValueFactory<TeachingAssistant, String>("email")
        );
        taTable.getColumns().add(nameColumn);
        taTable.getColumns().add(emailColumn);

        // ADD BOX FOR ADDING A TA
        String namePromptText = props.getProperty(TAManagerProp.NAME_PROMPT_TEXT.toString());
        String emailPromptText = props.getProperty(TAManagerProp.EMAIL_PROMPT_TEXT.toString());
        String addButtonText = props.getProperty(TAManagerProp.ADD_BUTTON_TEXT.toString());
        String clearButtonText = props.getProperty(TAManagerProp.CLEAR_BUTTON_TEXT.toString());
        nameTextField = new TextField();
        emailTextField = new TextField();
        nameTextField.setPromptText(namePromptText);
        emailTextField.setPromptText(emailPromptText);
        addButton =new Button(addButtonText);
        clearButton =new Button(clearButtonText);
        addBox = new HBox();
        nameTextField.prefWidthProperty().bind(addBox.widthProperty().multiply(.4));
        emailTextField.prefWidthProperty().bind(addBox.widthProperty().multiply(.4));
        addButton.prefWidthProperty().bind(addBox.widthProperty().multiply(.1));
        clearButton.prefWidthProperty().bind(addBox.widthProperty().multiply(.1));
        addBox.getChildren().add(nameTextField);
        addBox.getChildren().add(emailTextField);
        addBox.getChildren().add(addButton);
        addBox.getChildren().add(clearButton);
      

        // INIT THE HEADER ON THE RIGHT
        officeHoursHeaderBox = new HBox();
        String officeHoursGridText = props.getProperty(TAManagerProp.OFFICE_HOURS_SUBHEADER.toString());
        officeHoursHeaderLabel = new Label(officeHoursGridText);
        officeHoursHeaderBox.getChildren().add(officeHoursHeaderLabel);
        
        // THESE WILL STORE PANES AND LABELS FOR OUR OFFICE HOURS GRID
        officeHoursGridPane = new GridPane();
        officeHoursGridTimeHeaderPanes = new HashMap();
        officeHoursGridTimeHeaderLabels = new HashMap();
        officeHoursGridDayHeaderPanes = new HashMap();
        officeHoursGridDayHeaderLabels = new HashMap();
        officeHoursGridTimeCellPanes = new HashMap();
        officeHoursGridTimeCellLabels = new HashMap();
        officeHoursGridTACellPanes = new HashMap();
        officeHoursGridTACellLabels = new HashMap();

        // ORGANIZE THE LEFT AND RIGHT PANES
        VBox leftPane = new VBox();
        leftPane.getChildren().add(tasHeaderBox);        
        leftPane.getChildren().add(taTable);        
        leftPane.getChildren().add(addBox);
        VBox rightPane = new VBox();
        rightPane.getChildren().add(officeHoursHeaderBox);
        
        time_options = FXCollections.observableArrayList(
        props.getProperty(TAManagerProp.TIME_12AM.toString()),
        props.getProperty(TAManagerProp.TIME_1AM.toString()),
        props.getProperty(TAManagerProp.TIME_2AM.toString()),
        props.getProperty(TAManagerProp.TIME_3AM.toString()),
        props.getProperty(TAManagerProp.TIME_4AM.toString()),
        props.getProperty(TAManagerProp.TIME_5AM.toString()),
        props.getProperty(TAManagerProp.TIME_6AM.toString()),
        props.getProperty(TAManagerProp.TIME_7AM.toString()),
        props.getProperty(TAManagerProp.TIME_8AM.toString()),
        props.getProperty(TAManagerProp.TIME_9AM.toString()),
        props.getProperty(TAManagerProp.TIME_10AM.toString()),
        props.getProperty(TAManagerProp.TIME_11AM.toString()),
        props.getProperty(TAManagerProp.TIME_12PM.toString()),
        props.getProperty(TAManagerProp.TIME_1PM.toString()),
        props.getProperty(TAManagerProp.TIME_2PM.toString()),
        props.getProperty(TAManagerProp.TIME_3PM.toString()),
        props.getProperty(TAManagerProp.TIME_4PM.toString()),
        props.getProperty(TAManagerProp.TIME_5PM.toString()),
        props.getProperty(TAManagerProp.TIME_6PM.toString()),
        props.getProperty(TAManagerProp.TIME_7PM.toString()),
        props.getProperty(TAManagerProp.TIME_8PM.toString()),
        props.getProperty(TAManagerProp.TIME_9PM.toString()),
        props.getProperty(TAManagerProp.TIME_10PM.toString()),
        props.getProperty(TAManagerProp.TIME_11PM.toString())
        );
        comboBox1 = new ComboBox(time_options);
        comboBox2 = new ComboBox(time_options);
        
        officeHoursHeaderBox.getChildren().add(comboBox1);
        comboBox1.setPrefHeight(42);
        comboBox1.setPrefWidth(150);
        comboBox1.getSelectionModel().select(data.getTaData().getStartHour());
        comboBox1.valueProperty().addListener(new ChangeListener<String>() {
            @Override public void changed(ObservableValue ov, String t, String t1) {
                if(t != null && t1 != null)
                    if(comboBox1.getSelectionModel().getSelectedIndex() != data.getTaData().getStartHour())
                        controller.changeTime();
            }
        });
        officeHoursHeaderBox.getChildren().add(comboBox2);
        comboBox2.setPrefHeight(42);
        comboBox2.setPrefWidth(150);
        comboBox2.getSelectionModel().select(data.getTaData().getEndHour());
        comboBox2.valueProperty().addListener(new ChangeListener<String>() {
            @Override public void changed(ObservableValue ov, String t, String t1) {
                if(t != null && t1 != null)
                    if(comboBox2.getSelectionModel().getSelectedIndex() != data.getTaData().getEndHour())
                        controller.changeTime();
            }    
        });
        rightPane.getChildren().add(officeHoursGridPane);
        
        
        
       tabPane=new TabPane();
       cdTab=new Tab();
       taTab=new Tab();
       rdTab=new Tab();
       pdTab=new Tab();
       sdTab=new Tab();
       
    ///////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////
       
        
        //course info tab
        //firstbox
        courseIBox=new GridPane();
        couseILabel=new Label(props.getProperty(TAManagerProp.COURSE_TITLE.toString()));
        couseILabel.setStyle("-fx-font-size:14pt;-fx-font-weight: bold");
        courseIBox.add(couseILabel,0,0);
        subjectLabel = new Label(props.getProperty(TAManagerProp.SUBJECT_TITLE.toString()));
        courseIBox.add(subjectLabel,0,1);
        
        ObservableList subjectOption= FXCollections.observableArrayList();
        subjectOption.addAll("CSE","ISE");
        subjectBox=new ComboBox(subjectOption);
        
        courseIBox.add(subjectBox,1,1);
        numberLabel = new Label(props.getProperty(TAManagerProp.NUMBER_TITLE.toString()));
        courseIBox.add(numberLabel,2,1);
        
        ObservableList numberOption= FXCollections.observableArrayList();
        numberOption.addAll("219","308","380");
        numberBox=new ComboBox(numberOption);
        courseIBox.add(numberBox,3,1);
        semesterLabel=new Label(props.getProperty(TAManagerProp.SEMESTER_TITLE.toString()));
        courseIBox.add(semesterLabel,0,2);
        
        ObservableList semesterOption= FXCollections.observableArrayList();
        semesterOption.addAll("Fall","Spring","Summer");
        semesterBox=new ComboBox(semesterOption);
        courseIBox.add(semesterBox,1,2);
        
        yearLabel=new Label(props.getProperty(TAManagerProp.YEAR_TITLE.toString()));
        courseIBox.add(yearLabel,2,2);
        
        ObservableList yearOption= FXCollections.observableArrayList();
        yearOption.addAll("2017","2016");
        yearBox=new ComboBox(yearOption);
        courseIBox.add(yearBox,3,2);

        
        titleLabel=new Label(props.getProperty(TAManagerProp.TITLE_TITLE.toString()));
        courseIBox.add(titleLabel,0,3);
        titleField=new TextField();
        String titlePromptText=props.getProperty(TAManagerProp.TITLE_PROMPT.toString());
        titleField.setPromptText(titlePromptText);
        courseIBox.add(titleField,1,3);
       

        
        instructionNLabel=new Label(props.getProperty(TAManagerProp.INSTRU_NAME_TITLE.toString()));
        courseIBox.add(instructionNLabel,0,4);
        instructionNField=new TextField();
        instructionNField.setPromptText("Richard McKenna");
        courseIBox.add(instructionNField,1,4);
        instructionHLabel=new Label(props.getProperty(TAManagerProp.INSTRU_HOME_TITLE.toString()));
        courseIBox.add(instructionHLabel,0,5);
        instructionHField=new TextField();
        instructionHField.setPromptText("http://www.cs.stonybrook/~richard");
        courseIBox.add(instructionHField,1,5);
        
        exportLabel=new Label(props.getProperty(TAManagerProp.EXPORT_TITLE.toString()));
        courseIBox.add(exportLabel,0,6);
        dirLabel=new Label();
        courseIBox.add(dirLabel,1,6);
        changeButton=new Button(props.getProperty(TAManagerProp.CHANGE_TEXT.toString()));
        changeButton.setStyle("-fx-font-size: 12pt;fx-font-weight: bold");
        
        changeButton.setOnAction(e -> {
            controller.changeExportDir();
        });
        
        
        courseIBox.add(changeButton,2,6);
        courseIBox.setHgap(10);
        courseIBox.setVgap(5);
        courseIBox.setStyle("-fx-border-color: black;-fx-background-color:#e6e6ff");

        //second box
        siteBox=new VBox();
        siteTemplate=new Label(props.getProperty(TAManagerProp.SITE_TEMPLATE_TITLE.toString()));
        siteTemplate.setStyle("-fx-font-size:14pt;-fx-font-weight: bold");
        sentence=new Label(props.getProperty(TAManagerProp.SENTENCE_TEXT.toString()));
        selectTemple=new Label();
        
        
        selectButton=new Button();
        selectButton.setText(props.getProperty(TAManagerProp.SELECT_BUTTON_TEXT.toString()));
        
       
        
        
        sitePage=new Label(props.getProperty(TAManagerProp.SITE_PAGE_TITLE.toString()));
        psTable=new TableView();
        psTable.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
        String ntColumn=props.getProperty(TAManagerProp.NAVBAR_TITLE.toString());
        String fnColumn=props.getProperty(TAManagerProp.FILE_NAME_TITLE.toString());
        String sColumn=props.getProperty(TAManagerProp.SCRIPT_TITLE.toString());
        //user=new TableColumn(props.getProperty(TAManagerProp.USE_TITLE.toString()));
        //user.setPrefWidth(150);
        nt=new TableColumn(ntColumn);
        nt.setPrefWidth(150);
        fn=new TableColumn(fnColumn);
        fn.setPrefWidth(150);
        script=new TableColumn(sColumn);
        script.setPrefWidth(150);
           //checkbox for the use coloumn
        
        ObservableList<CourseDetail> courseData = data.getCdData().getCoursedetail();
        psTable.setItems(courseData);
        
        nt.setCellValueFactory(
                new PropertyValueFactory<CourseDetail,String>("navbar"));
        fn.setCellValueFactory(
                new PropertyValueFactory<CourseDetail,String>("filename"));
        script.setCellValueFactory(
                new PropertyValueFactory<CourseDetail,String>("script"));
        
        user = new TableColumn<CourseDetail, Boolean>(props.getProperty(TAManagerProp.USE_TITLE.toString()));
        user.setPrefWidth(100);
        user.setCellValueFactory(new Callback<CellDataFeatures<CourseDetail, Boolean>, ObservableValue<Boolean>>() {
 
        @Override
        public ObservableValue<Boolean> call(CellDataFeatures<CourseDetail, Boolean> param) {
            CourseDetail course = param.getValue();
            SimpleBooleanProperty booleanProp = new SimpleBooleanProperty(course.getCheck());
            booleanProp.addListener(new ChangeListener<Boolean>() {
                @Override
                public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue,Boolean newValue) {
                    course.setCheck(newValue);
                    app.getGUI().updateToolbarControls(false);
                    }
            });
            return booleanProp;
        }
        });
        user.setCellFactory(new Callback<TableColumn<CourseDetail, Boolean>, TableCell<CourseDetail, Boolean>>() {
            @Override
            public TableCell<CourseDetail, Boolean> call(TableColumn<CourseDetail, Boolean> p) {
                CheckBoxTableCell<CourseDetail, Boolean> cell = new CheckBoxTableCell<CourseDetail, Boolean>();
                cell.setAlignment(Pos.CENTER);
                return cell; 
            }
        });
        
        psTable.setEditable(true);
        psTable.getColumns().addAll(user,nt,fn,script);
        psTable.setMinWidth(600);
        psTable.setMaxWidth(600);
        psTable.setPrefWidth(600);
        siteBox.getChildren().addAll(siteTemplate,sentence,selectTemple,selectButton,sitePage,psTable);
        siteBox.setSpacing(10);
        siteBox.setStyle("-fx-border-color: black;-fx-background-color:#e6e6ff");
        
        
        //third box
        pageBox=new GridPane();
        pageStyleLabel=new Label(props.getProperty(TAManagerProp.PAGE_STYLE_TITLE.toString()));
        pageStyleLabel.setStyle("-fx-font-size:14pt;-fx-font-weight: bold");
        pageBox.add(pageStyleLabel,0,0);
        bannerLabel=new Label(props.getProperty(TAManagerProp.BANNER_LABEL.toString()));
        pageBox.add(bannerLabel,0,1);
        Image yaleI=new Image("file:./images/"+"Yale.png");
        yaleIV1=new ImageView(yaleI);
        pageBox.add(yaleIV1,1,1);
        change1Button=new Button(props.getProperty(TAManagerProp.CHANGE_TEXT.toString()));
        change1Button.setStyle("-fx-font-size: 12pt;fx-font-weight: bold");
        pageBox.add(change1Button,2,1);
        
        leftLabel=new Label(props.getProperty(TAManagerProp.LEFT_LABEL.toString()));
        pageBox.add(leftLabel,0,2);
        
        change2Button=new Button(props.getProperty(TAManagerProp.CHANGE_TEXT.toString()));
        change2Button.setStyle("-fx-font-size: 12pt;fx-font-weight: bold");
        yaleIV2=new ImageView(yaleI);
        pageBox.add(yaleIV2,1,2);
        pageBox.add(change2Button,2,2);
        rightLabel=new Label(props.getProperty(TAManagerProp.RIGHT_LABEL.toString()));
        pageBox.add(rightLabel,0,3);
        change3Button=new Button(props.getProperty(TAManagerProp.CHANGE_TEXT.toString()));
        change3Button.setStyle("-fx-font-size: 12pt;fx-font-weight: bold");
        pageBox.add(change3Button,2,3);
        Image csI=new Image("file:./images/"+"CS.png");
        csIV=new ImageView(csI);
        pageBox.add(csIV,1,3);
        styleLabel=new Label(props.getProperty(TAManagerProp.STYLE_SHEET_LABEL.toString()));
        pageBox.add(styleLabel,0,4);
        
        stylesheetOption= FXCollections.observableArrayList();
        stylesheetOption.addAll("sea_wolf.css");
        styleCombo=new ComboBox(stylesheetOption);
        pageBox.add(styleCombo,1,4);
        
        noteLabel=new Label(props.getProperty(TAManagerProp.NOTE_TEXT.toString()));
        pageBox.add(noteLabel,0,5);
        noteContext=new Label(props.getProperty(TAManagerProp.NOTE_CONTEXT.toString()));
        pageBox.add(noteContext,1,5);
        pageBox.setHgap(10);
        pageBox.setVgap(5);
        pageBox.setStyle("-fx-border-color: black;-fx-background-color:#e6e6ff");
       
        bigBox=new VBox();
        bigBox.getChildren().addAll(courseIBox,siteBox,pageBox);
        bigBox.setPadding(new Insets(16,16,16,16));
        bigBox.setSpacing(20);
        bigBox.setStyle("-fx-background-color:#ccccff");
        ScrollPane cdSPane=new ScrollPane(bigBox);
        bigBox.prefWidthProperty().bind(cdSPane.widthProperty());
        cdTab.setContent(cdSPane);
        
        titleField.setOnKeyPressed(e->{
            controller.markWorkAsEdited();
        });
        
        instructionNField.setOnKeyPressed(e->{
            controller.markWorkAsEdited();
        });
        
        instructionHField.setOnKeyPressed(e->{
            controller.markWorkAsEdited();
        });
        
        changeButton.setOnAction(e -> {
            controller.changeExportDir();
        });
        selectButton.setOnAction(e->{
            controller.changeSiteTemplate();
        });
        change1Button.setOnAction(e->{
            controller.bannerChange();
        });
        change2Button.setOnAction(e->{
            controller.leftChange();
        });
        change3Button.setOnAction(e->{
            controller.rightChange();
        });
//        subjectBox.valueProperty().addListener(new ChangeListener<String>(){
//            @Override
//            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
//                
//            }
//            
//        });
        subjectBox.setOnAction(e->{
            data.getCdData().setSubject((String)subjectBox.getValue());
        });
        numberBox.setOnAction(e->{
            data.getCdData().setNumber((String)numberBox.getValue());
        });
        semesterBox.setOnAction(e->{
            data.getCdData().setSemester((String)semesterBox.getValue());
        });
        yearBox.setOnAction(e->{
            data.getCdData().setYear((String)yearBox.getValue());
        });
        styleCombo.setOnAction(e->{
            data.getCdData().setStylesheet((String)styleCombo.getValue());
        });
        
        
        
    ///////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////
        
        //for the recitation data tab
        firstBox=new HBox();
        recitationLabel=new Label(props.getProperty(TAManagerProp.RECITATION_LABEL.toString()));
        recitationLabel.setStyle("-fx-font-size:20pt;-fx-font-weight:bold");
        subButton=initButton(REMOVE_ICON.toString(),REMOVE_TOOLTIP.toString());
        firstBox.getChildren().addAll(recitationLabel,subButton);
       
        
        recitationTable=new TableView();
  
        section=new TableColumn(props.getProperty(TAManagerProp.SECTION_LABEL.toString()));
        section.setPrefWidth(200);
        instructor=new TableColumn(props.getProperty(TAManagerProp.INSTRUCTOR_LABEL.toString()));
        instructor.setPrefWidth(200);
        daytime=new TableColumn(props.getProperty(TAManagerProp.DAYTIME_LABEL.toString()));
        daytime.setPrefWidth(200);     
        location=new TableColumn(props.getProperty(TAManagerProp.LOCATION_LABEL.toString()));
        location.setPrefWidth(200);
        ta1=new TableColumn(props.getProperty(TAManagerProp.TA_LABEL.toString()));
        ta1.setPrefWidth(200);
        ta2=new TableColumn(props.getProperty(TAManagerProp.TA_LABEL.toString()));
        ta2.setPrefWidth(200);

        recitationTable.getColumns().addAll(section,instructor,daytime,location,ta1,ta2);

        recitationTable.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
        
        
        ObservableList<Recitation> recitationData=data.getRdData().getRecitations();
        recitationTable.setItems(recitationData);
        
        section.setCellValueFactory(
                new PropertyValueFactory<Recitation,String>("section"));
        instructor.setCellValueFactory(
                new PropertyValueFactory<Recitation,String>("instructor"));
        daytime.setCellValueFactory(
                new PropertyValueFactory<Recitation,String>("daytime"));
        location.setCellValueFactory(
                new PropertyValueFactory<Recitation,String>("location"));
        ta1.setCellValueFactory(
                new PropertyValueFactory<Recitation,String>("ta1"));
        ta2.setCellValueFactory(
                new PropertyValueFactory<Recitation,String>("ta2"));

        recitationTable.setMaxWidth(1200);
        recitationTable.setMinWidth(1200);
        recitationTable.setPrefWidth(1200);
        
        
        addPane=new GridPane();
        addPane.setPrefWidth(1200);
        addPane.setMaxWidth(1200);
        addPane.setMinWidth(1200);

        addPane.setHgap(20);
        addPane.setVgap(5);
        addeditLabel=new Label(props.getProperty(TAManagerProp.ADDEDIT_LABEL.toString()));
        addeditLabel.setStyle("-fx-font-size:12pt;-fx-font-weight:bold");
        addPane.add(addeditLabel, 0, 0);
        sectionLabel=new Label(props.getProperty(TAManagerProp.SECTION_LABEL.toString())+":");
        addPane.add(sectionLabel,0,1);
        sectionField=new TextField();
        addPane.add(sectionField,1,1);
        instructorLabel=new Label(props.getProperty(TAManagerProp.INSTRUCTOR_LABEL.toString())+":");
        addPane.add(instructorLabel,0,2);
        instructorField=new TextField();
        addPane.add(instructorField,1,2);
        daytimeLabel=new Label(props.getProperty(TAManagerProp.DAYTIME_LABEL.toString())+":");
        addPane.add(daytimeLabel,0,3);
        daytimeField=new TextField();
        addPane.add(daytimeField,1,3);
        locationLabel=new Label(props.getProperty(TAManagerProp.LOCATION_LABEL.toString())+":");
        addPane.add(locationLabel,0,4);
        locationField=new TextField();
        addPane.add(locationField,1,4);
        suTA1Label=new Label(props.getProperty(TAManagerProp.SUPTA_LABEL.toString()));
        addPane.add(suTA1Label,0,5);
       
        techera1 = FXCollections.observableArrayList();
        
        suTA1Box=new ComboBox(techera1);
        addPane.add(suTA1Box,1,5);
        suTA2Label=new Label(props.getProperty(TAManagerProp.SUPTA_LABEL.toString()));
        addPane.add(suTA2Label,0,6);
        suTA2Box=new ComboBox(techera1);
        addPane.add(suTA2Box,1,6);
        addupdatButton=new Button(props.getProperty(TAManagerProp.ADD_BUTTON_TEXT.toString()));
        addupdatButton.setStyle("-fx-font-size: 12pt;fx-font-weight: bold");
        addPane.add(addupdatButton,0,7);
       
        clearB=new Button(props.getProperty(TAManagerProp.CLEAR_BUTTON_TEXT.toString()));
        clearB.setStyle("-fx-font-size: 12pt;fx-font-weight: bold");
        addPane.add(clearB,1,7);
        addPane.setStyle("-fx-border-color: black;-fx-background-color:#e6e6ff");
             
        rbigBox=new VBox();
        rbigBox.getChildren().addAll(firstBox,recitationTable,addPane);
        rbigBox.setPadding(new Insets(16,16,16,16));
        rbigBox.setSpacing(20);
        rbigBox.setStyle("-fx-background-color:#ccccff");
        
        ScrollPane rdSPane=new ScrollPane(rbigBox);
        rbigBox.prefWidthProperty().bind(rdSPane.widthProperty());
        rdTab.setContent(rdSPane);
        
        addupdatButton.setOnAction(e -> {
            if(addrecitation){
                controller.addRecitation();
            }
            else{
                controller.changeExistRecitation();
                addupdatButton.setText(props.getProperty(TAManagerProp.ADD_BUTTON_TEXT.toString()));
                //addrecitation = true;
            }
        });
        
        clearB.setOnAction(e -> {
            addupdatButton.setText(props.getProperty(TAManagerProp.ADD_BUTTON_TEXT.toString()));
            addrecitation = true;
            controller.clearRecitationField();
        });
        recitationTable.setOnMouseClicked(e -> {
            addupdatButton.setText(props.getProperty(TAManagerProp.UPDATE_BUTTON_TEXT.toString()));
            addrecitation = false;
            controller.loadRecitationToText();
        });
        subButton.setOnAction(e-> {
            controller.deleteRecitation();
            addrecitation = true;
            addupdatButton.setText(props.getProperty(TAManagerProp.ADD_BUTTON_TEXT.toString()));
            
        });
        
        
        
        
    ///////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////
        
        
        //for schedule data tab
        scheduleLabel=new Label(props.getProperty(TAManagerProp.SCHEDULE_LABEL.toString()));
        scheduleLabel.setStyle("-fx-font-size: 20pt;-fx-font-weight: bold");
        
        cpane=new GridPane();
        cpane.setHgap(20);
        cpane.setVgap(10);
        cpane.setStyle("-fx-border-color: black");
        calendarLabel=new Label(props.getProperty(TAManagerProp.CB_LABEL.toString()));
        calendarLabel.setStyle("-fx-font-size: 12pt;-fx-font-weight: bold");
        cpane.add(calendarLabel, 0, 0);
        smLabel=new Label(props.getProperty(TAManagerProp.START_MON_LABEL.toString()));
        cpane.add(smLabel, 0, 1);
        smPicker=new DatePicker();
        cpane.add(smPicker, 1, 1);
        
//        final Callback<DatePicker,DateCell> noSatAndSun1
//                =new Callback<DatePicker,DateCell>(){
//            @Override
//            public DateCell call(final DatePicker param) {
//                return new DateCell(){
//                    public void updateItem(LocalDate item,boolean empty){
//                        super.updateItem(item, empty);
//                        if(item.getDayOfWeek().equals(DayOfWeek.SATURDAY)||item.getDayOfWeek().equals(DayOfWeek.SUNDAY)){
//                            setDisable(true);
//                        }
//                    }
//                };     
//            }
//                    
//         };
//        smPicker.setDayCellFactory(noSatAndSun1);
        
        efLabel=new Label(props.getProperty(TAManagerProp.END_FRI_LABEL.toString()));
        cpane.add(efLabel, 2, 1);
        efPicker=new DatePicker();
        cpane.add(efPicker, 3, 1);
        
        
        final Callback<DatePicker,DateCell> noSatAndSun
                =new Callback<DatePicker,DateCell>(){
            @Override
            public DateCell call(final DatePicker param) {
                return new DateCell(){
                    public void updateItem(LocalDate item,boolean empty){
                        super.updateItem(item, empty);
                        if(item.getDayOfWeek().equals(DayOfWeek.SATURDAY)||item.getDayOfWeek().equals(DayOfWeek.SUNDAY)){
                            setDisable(true);
                        }
                    }
                };     
            }        
         };
        smPicker.setDayCellFactory(noSatAndSun);
        efPicker.setDayCellFactory(noSatAndSun);
        
        
        
        
        
        cpane.setStyle("-fx-border-color: black;-fx-background-color:#e6e6ff");
        
        
        smallBox=new VBox();
      
        smallBox.setSpacing(10);
        smallBox.setStyle("-fx-border-color: black");
        siBox=new HBox();
        siLabel=new Label(props.getProperty(TAManagerProp.SCHEDULE_ITEM_LABEL.toString()));
        siLabel.setStyle("-fx-font-size: 12pt;-fx-font-weight: bold");
        siButton=initButton(REMOVE_ICON.toString(),REMOVE_TOOLTIP.toString());
        siBox.getChildren().addAll(siLabel,siButton);
        siTable=new TableView();
        typeC=new TableColumn(props.getProperty(TAManagerProp.TYPE_LABEL.toString()));
        typeC.setPrefWidth(300);
        dateC=new TableColumn(props.getProperty(TAManagerProp.DATE_LABEL.toString()));
        dateC.setPrefWidth(300);
        titleC=new TableColumn(props.getProperty(TAManagerProp.TITLE_LABEL.toString()));
        titleC.setPrefWidth(300);
        topicC=new TableColumn(props.getProperty(TAManagerProp.TOPIC_LABEL.toString()));
        topicC.setPrefWidth(300);
        siTable.setMaxWidth(1200);
        siTable.setMinWidth(1200);
        siTable.setPrefWidth(1200);
        siTable.getColumns().addAll(typeC,dateC,titleC,topicC);
        
        ObservableList<Schedule> scheduleData=data.getSdData().getSchedules();
        siTable.setItems(scheduleData);
        typeC.setCellValueFactory(
                new PropertyValueFactory<Schedule,String>("type"));
        dateC.setCellValueFactory(
                new PropertyValueFactory<Schedule,String>("date"));
        titleC.setCellValueFactory(
                new PropertyValueFactory<Schedule,String>("title"));
        topicC.setCellValueFactory(
                new PropertyValueFactory<Schedule,String>("topic"));
         
        smallPane=new GridPane();
        smallPane.setHgap(20);
        smallPane.setVgap(10);
        l=new Label(props.getProperty(TAManagerProp.ADDEDIT_LABEL.toString()));
        l.setStyle("-fx-font-size: 12pt;-fx-font-weight: bold");
        smallPane.add(l, 0, 0);
        typeLabel=new Label(props.getProperty(TAManagerProp.TYPE_LABEL.toString())+":");
        smallPane.add(typeLabel, 0, 1);
        
        ObservableList type=FXCollections.observableArrayList();
        type.addAll("Holiday","Lecture","HW","Reference","Recitation");
        typeBox=new ComboBox(type);
        smallPane.add(typeBox, 1, 1);
        dateLabel=new Label(props.getProperty(TAManagerProp.DATE_LABEL.toString())+":");
        smallPane.add(dateLabel, 0, 2);
        datePicker=new DatePicker();
        smallPane.add(datePicker,1,2);
        timeLabel=new Label(props.getProperty(TAManagerProp.TIME_LABEL.toString())+":");
        smallPane.add(timeLabel, 0, 3);
        timeText=new TextField();
        smallPane.add(timeText, 1, 3);
        tLabel=new Label(props.getProperty(TAManagerProp.TITLE_LABEL.toString())+":");
        smallPane.add(tLabel, 0, 4);
        tTextField=new TextField();
        smallPane.add(tTextField, 1, 4);
        topicLabel=new Label(props.getProperty(TAManagerProp.TOPIC_LABEL.toString())+":");
        smallPane.add(topicLabel, 0, 5);
        topicField=new TextField();
        smallPane.add(topicField, 1, 5);
        linkLabel=new Label(props.getProperty(TAManagerProp.LINK_LABEL.toString()));
        smallPane.add(linkLabel,0,6);
        linkField=new TextField();
        smallPane.add(linkField,1,6);
        criteriaLabel=new Label(props.getProperty(TAManagerProp.CRITERIA_LABEL.toString())+":");
        smallPane.add(criteriaLabel, 0, 7);
        criteriaField=new TextField();
        smallPane.add(criteriaField, 1, 7);
        one=new Button(props.getProperty(TAManagerProp.ADD_BUTTON_TEXT.toString()));
        one.setStyle("-fx-font-size: 12pt;fx-font-weight: bold");
        smallPane.add(one, 0, 8);
      
        two=new Button(props.getProperty(TAManagerProp.CLEAR_BUTTON_TEXT.toString()));    
        two.setStyle("-fx-font-size: 12pt;fx-font-weight: bold");
        smallPane.add(two, 1, 8);
        
        smallBox.getChildren().addAll(siBox,siTable,smallPane);
        smallBox.setStyle("-fx-border-color: black;-fx-background-color:#e6e6ff");
        
        sbigBox=new VBox();
        sbigBox.getChildren().addAll(scheduleLabel,cpane,smallBox);
        sbigBox.setPadding(new Insets(16,16,16,16));
        sbigBox.setSpacing(20);
        sbigBox.setStyle("-fx-background-color:#ccccff");
        ScrollPane sdSPane=new ScrollPane(sbigBox);
        sbigBox.prefWidthProperty().bind(sdSPane.widthProperty());
        sdTab.setContent(sdSPane);
        
        one.setOnAction(e->{
            if(addschedule){
                controller.addSchedule();
            }
            else{
                controller.changeExistSchedule();
                one.setText(props.getProperty(TAManagerProp.ADD_BUTTON_TEXT.toString()));
                addschedule=true;
            }  
        });
        two.setOnAction(e -> {
            one.setText(props.getProperty(TAManagerProp.ADD_BUTTON_TEXT.toString()));
            //add = true;
            controller.clearScheduleField();
            addschedule=true;
        });
        
        siTable.setOnMouseClicked(e -> {
            one.setText(props.getProperty(TAManagerProp.UPDATE_BUTTON_TEXT.toString()));
            addschedule = false;
            controller.loadScheduleToText();
        });
        
        siButton.setOnAction(e-> {
            controller.deleteSchedule();
        });
        
        smPicker.valueProperty().addListener(new ChangeListener<LocalDate>(){
            
            @Override
            public void changed(ObservableValue<? extends LocalDate> observable, LocalDate oldValue, LocalDate newValue) {
                if(oldValue!=null&&newValue!=null){
                    if(listenStart){
                    controller.changeStartDay(oldValue,newValue);}
                    
                }
            }
            
        });
        
        efPicker.valueProperty().addListener(new ChangeListener<LocalDate>(){
            @Override
            public void changed(ObservableValue<? extends LocalDate> observable, LocalDate oldValue, LocalDate newValue) {
                if(oldValue!=null&&newValue!=null){
                    if(listenEnd){
                        controller.changeEndDay(oldValue,newValue);
                    }
                    
                }
            }
            
        });
        
        datePicker.valueProperty().addListener(new ChangeListener<LocalDate>(){
            @Override
            public void changed(ObservableValue<? extends LocalDate> observable, LocalDate oldValue, LocalDate newValue) {
                if(oldValue!=null||newValue!=null){
                    if(listenDate){
                        controller.changeDataDate(oldValue,newValue);
                    }
                }
                
            }
            
        });
        
        
        

        
    //////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////
    
        //for project data
        project=new Label(props.getProperty(TAManagerProp.PROJECT_LABEL.toString()));
        project.setStyle("-fx-font-size: 20pt;-fx-font-weight: bold");
        
        teamBox=new VBox();
        teamBox.setStyle("-fx-border-color: black;-fx-background-color:#e6e6ff");
        teamBox.setSpacing(10);
        
        tpBox=new HBox();
        teamLabel=new Label(props.getProperty(TAManagerProp.TEAM_LABEL.toString()));
        teamLabel.setStyle("-fx-font-size: 12pt;-fx-font-weight: bold");
        tButton=initButton(REMOVE_ICON.toString(),REMOVE_TOOLTIP.toString());
        tpBox.getChildren().addAll(teamLabel,tButton);
       
        tTable=new TableView();
        nC=new TableColumn(props.getProperty(TAManagerProp.NAME_LABEL.toString()));
        nC.setPrefWidth(300);
        cC=new TableColumn(props.getProperty(TAManagerProp.COBR_LABEL.toString()));
        cC.setPrefWidth(300);
        tC=new TableColumn(props.getProperty(TAManagerProp.TEXT_COLOR_LABEL.toString()));
        tC.setPrefWidth(300);
        lC=new TableColumn(props.getProperty(TAManagerProp.LINK_LABEL.toString()));
        lC.setPrefWidth(300);
        tTable.getColumns().addAll(nC,cC,tC,lC);
        tTable.setMaxWidth(1200);
        tTable.setMinWidth(1200);
        tTable.setPrefWidth(1200);
       
        ObservableList<Team> teamData=data.getPdData().getTeams();
        tTable.setItems(teamData);
        nC.setCellValueFactory(new PropertyValueFactory<Team,String>("name"));        
        cC.setCellValueFactory(new PropertyValueFactory<Team,String>("color"));
        tC.setCellValueFactory(new PropertyValueFactory<Team,String>("textcolor"));
        lC.setCellValueFactory(new PropertyValueFactory<Team,String>("link"));
        
        
        
        tPane=new GridPane();
        tPane.setHgap(20);
        tPane.setVgap(10);
        aeLable=new Label(props.getProperty(TAManagerProp.ADDUPDATE_LABEL.toString()));
        aeLable.setStyle("-fx-font-size: 12pt;-fx-font-weight: bold");
        tPane.add(aeLable, 0, 0);
        nL=new Label(props.getProperty(TAManagerProp.NAME_LABEL.toString())+":");
        tPane.add(nL, 0, 1);
        nF=new TextField();
        tPane.add(nF, 1, 1);
        cL=new Label(props.getProperty(TAManagerProp.COLOR_LABEL.toString()));
        tPane.add(cL, 0, 2);
        colorPicker=new ColorPicker();
        tPane.add(colorPicker, 1, 2);
        tcL=new Label(props.getProperty(TAManagerProp.TEXTCOLOR_LABEL.toString()));
        tPane.add(tcL, 2, 2);
        textPicker=new ColorPicker();
        tPane.add(textPicker,3,2);
        lL=new Label(props.getProperty(TAManagerProp.LINK_LABEL.toString())+":");
        tPane.add(lL, 0, 3);
        lTF=new TextField();
        tPane.add(lTF, 1, 3);
        addP1Button=new Button(props.getProperty(TAManagerProp.ADD_BUTTON_TEXT.toString()));
        addP1Button.setStyle("-fx-font-size: 12pt;fx-font-weight: bold");
        tPane.add(addP1Button, 0, 4);
        
        clearP1Button=new Button(props.getProperty(TAManagerProp.CLEAR_BUTTON_TEXT.toString()));
        clearP1Button.setStyle("-fx-font-size: 12pt;fx-font-weight: bold");
        tPane.add(clearP1Button, 1, 4);
        teamBox.getChildren().addAll(tpBox,tTable,tPane);
        
        stuBox=new VBox();
     
        spBox=new HBox();
        studentsLabel=new Label(props.getProperty(TAManagerProp.STUDENT_LABEL.toString()));
        studentsLabel.setStyle("-fx-font-size: 12pt;-fx-font-weight: bold");
        sButton=initButton(REMOVE_ICON.toString(),REMOVE_TOOLTIP.toString());
        spBox.getChildren().addAll(studentsLabel,sButton);
        
        sTable=new TableView();
        fnC=new TableColumn(props.getProperty(TAManagerProp.FIRST_NAME_LABEL.toString()));
        fnC.setPrefWidth(300);
        lnC=new TableColumn(props.getProperty(TAManagerProp.LAST_NAME_LABEL.toString()));
        lnC.setPrefWidth(300);
        teamC=new TableColumn(props.getProperty(TAManagerProp.TEAM_LABEL.toString()));
        teamC.setPrefWidth(300);
        rC=new TableColumn(props.getProperty(TAManagerProp.ROLE_LABEL.toString()));
        rC.setPrefWidth(300);
        sTable.getColumns().addAll(fnC,lnC,teamC,rC);
        sTable.setMaxWidth(1200);
        sTable.setMinWidth(1200);
        sTable.setPrefWidth(1200);
        
        ObservableList<Student> studentData=data.getPdData().getStudents();
        sTable.setItems(studentData);
        fnC.setCellValueFactory(
                new PropertyValueFactory<Schedule,String>("firstname"));
        lnC.setCellValueFactory(
                new PropertyValueFactory<Schedule,String>("lastname"));
        teamC.setCellValueFactory(
                new PropertyValueFactory<Schedule,String>("team"));
        rC.setCellValueFactory(
                new PropertyValueFactory<Schedule,String>("role"));
        
        
        
        
        stuPane=new GridPane();
        stuPane.setHgap(20);
        stuPane.setVgap(10);
        aeLable2=new Label(props.getProperty(TAManagerProp.ADDEDIT_LABEL.toString()));
        aeLable2.setStyle("-fx-font-size: 12pt;-fx-font-weight: bold");
        stuPane.add(aeLable2,0,0);
        fnLabel=new Label(props.getProperty(TAManagerProp.FIRST_NAME_LABEL.toString())+":");
        stuPane.add(fnLabel,0,1);
        fnTF=new TextField();
        stuPane.add(fnTF,1,1);
        lnLabel=new Label(props.getProperty(TAManagerProp.LAST_NAME_LABEL.toString())+":");
        stuPane.add(lnLabel,0,2);
        lnTF=new TextField();
        stuPane.add(lnTF,1,2);
        tpLabel=new Label(props.getProperty(TAManagerProp.TEAM_LABEL.toString())+":");
        stuPane.add(tpLabel,0,3);
        
        
        teamOption=FXCollections.observableArrayList();
        tBox=new ComboBox(teamOption);
        stuPane.add(tBox,1,3);
        rLabel=new Label(props.getProperty(TAManagerProp.ROLE_LABEL.toString()));
        stuPane.add(rLabel,0,4);
        rTF=new TextField();
        stuPane.add(rTF,1,4);
        addP2Button=new Button(props.getProperty(TAManagerProp.ADD_BUTTON_TEXT.toString()));
        addP2Button.setStyle("-fx-font-size: 12pt;fx-font-weight: bold");
        
        
        stuPane.add(addP2Button,0,5);
        clearP2Button=new Button(props.getProperty(TAManagerProp.CLEAR_BUTTON_TEXT.toString()));
        clearP2Button.setStyle("-fx-font-size: 12pt;fx-font-weight: bold");
        stuPane.add(clearP2Button,1,5);
        
        stuBox.getChildren().addAll(spBox,sTable,stuPane);
        stuBox.setStyle("-fx-border-color: black;-fx-background-color:#e6e6ff");;
          
       
        pbigBox=new VBox();
        pbigBox.getChildren().addAll(project,teamBox,stuBox);
        pbigBox.setPadding(new Insets(16,16,16,16));
        pbigBox.setSpacing(10);
        pbigBox.setStyle("-fx-background-color:#ccccff");
        pbigBox.setSpacing(20);
        ScrollPane pdSPane=new ScrollPane(pbigBox);
        pbigBox.prefWidthProperty().bind(pdSPane.widthProperty());
        pdTab.setContent(pdSPane);
       
        
        
        addP1Button.setOnAction(e->{
            if(addteam){
               controller.addTeam(); 
            }
            else{
                controller.changeExistTeam();
                addP1Button.setText(props.getProperty(TAManagerProp.ADD_BUTTON_TEXT.toString()));
                addteam=true;
            }
            
            
        });
        
        
        addP2Button.setOnAction(e->{
            if(addstudent){
                controller.addStudent();
            }
            else{
                controller.changeExistStudent();
                addP2Button.setText(props.getProperty(TAManagerProp.ADD_BUTTON_TEXT.toString()));
                addstudent=true;
            }
            
        });
          
        
        clearP1Button.setOnAction(e -> {
            addP1Button.setText(props.getProperty(TAManagerProp.ADD_BUTTON_TEXT.toString()));
            //add = true;
            
            //datepicker
            nF.clear();
            lTF.clear();
            //color picker
            //text color picker
        });
        
        
        clearP2Button.setOnAction(e -> {
            addP2Button.setText(props.getProperty(TAManagerProp.ADD_BUTTON_TEXT.toString()));
            controller.clearStudentField();
            addstudent=true;
           
        });
        
        tTable.setOnMouseClicked(e -> {
            addP1Button.setText(props.getProperty(TAManagerProp.UPDATE_BUTTON_TEXT.toString()));
            addteam = false;
            controller.loadTeamtoText();
        });
        
        sTable.setOnMouseClicked(e -> {
            addP2Button.setText(props.getProperty(TAManagerProp.UPDATE_BUTTON_TEXT.toString()));
            addstudent = false;
            controller.loadStudentToText();
            
        });
        
        tButton.setOnAction(e->{
            controller.deleteTeam();
        });
        
        sButton.setOnAction(e->{
            addP2Button.setText(props.getProperty(TAManagerProp.ADD_BUTTON_TEXT.toString()));
            controller.deleteStudents();
            addstudent=true;
        });
        
        
        
        // BOTH PANES WILL NOW GO IN A SPLIT PANE
        SplitPane sPane = new SplitPane(leftPane, new ScrollPane(rightPane));
        workspace = new BorderPane();
        
        // AND PUT EVERYTHING IN THE WORKSPACE
        //((BorderPane) workspace).setCenter(sPane);
        ((BorderPane) workspace).setCenter(tabPane);
        cdTab.setText(props.getProperty(TAManagerProp.CD_TAB_TITLE.toString()));
        cdTab.setStyle("-fx-font-size:12pt;-fx-font-weight: bold");
        taTab.setText(props.getProperty(TAManagerProp.TA_TAB_TITLE.toString()));
        taTab.setStyle("-fx-font-size:12pt;-fx-font-weight: bold");
        rdTab.setText(props.getProperty(TAManagerProp.RD_TAB_TITLE.toString()));
        rdTab.setStyle("-fx-font-size:12pt;-fx-font-weight: bold");
        sdTab.setText(props.getProperty(TAManagerProp.SD_TAB_TITLE.toString()));
        sdTab.setStyle("-fx-font-size:12pt;-fx-font-weight: bold");
        pdTab.setText(props.getProperty(TAManagerProp.PD_TAB_TITLE.toString()));
        pdTab.setStyle("-fx-font-size:12pt;-fx-font-weight: bold");
        tabPane.setTabClosingPolicy(TabPane.TabClosingPolicy.UNAVAILABLE);
        tabPane.getTabs().addAll(cdTab,taTab,rdTab,sdTab,pdTab);
        taTab.setContent(sPane);

        // MAKE SURE THE TABLE EXTENDS DOWN FAR ENOUGH
        taTable.prefHeightProperty().bind(workspace.heightProperty().multiply(1.9));

        // NOW LET'S SETUP THE EVENT HANDLING
        controller = new TAController(app);

        // CONTROLS FOR ADDING TAs
        nameTextField.setOnAction(e -> {
            if(!add)
                controller.changeExistTA();
            else
                controller.handleAddTA();
        });
        emailTextField.setOnAction(e -> {
            if(!add)
                controller.changeExistTA();
            else
                controller.handleAddTA();
        });
        addButton.setOnAction(e -> {
            if(!add)
                controller.changeExistTA();
            else
                controller.handleAddTA();
        });
        clearButton.setOnAction(e -> {
            addButton.setText(props.getProperty(TAManagerProp.ADD_BUTTON_TEXT.toString()));
            add = true;
            nameTextField.clear();
            emailTextField.clear();
            taTable.getSelectionModel().select(null);
        });

        taTable.setFocusTraversable(true);
        taTable.setOnKeyPressed(e -> {
            controller.handleKeyPress(e.getCode());
        });
        taTable.setOnMouseClicked(e -> {
            addButton.setText(props.getProperty(TAManagerProp.UPDATE_BUTTON_TEXT.toString()));
            add = false;
            controller.loadTAtotext();
        });
        
        workspace.setOnKeyPressed(e ->{
            if(e.isControlDown())
                if(e.getCode() == KeyCode.Z)
                    controller.Undo();
                else if(e.getCode() == KeyCode.Y)
                    controller.Redo();
        });
        
        removeButton.setOnAction(e->{
            controller.deleteTA();
        });
    }
    
    
    // WE'LL PROVIDE AN ACCESSOR METHOD FOR EACH VISIBLE COMPONENT
    // IN CASE A CONTROLLER OR STYLE CLASS NEEDS TO CHANGE IT
    
    
    
    public Button initButton(String icon, String tooltip) {
        PropertiesManager props = PropertiesManager.getPropertiesManager();
	
	// LOAD THE ICON FROM THE PROVIDED FILE
        String imagePath = FILE_PROTOCOL + PATH_IMAGES + props.getProperty(icon);
        Image buttonImage = new Image(imagePath);
	
	// NOW MAKE THE BUTTON
        Button button = new Button();
        button.setGraphic(new ImageView(buttonImage));
        Tooltip buttonTooltip = new Tooltip(props.getProperty(tooltip));
        button.setTooltip(buttonTooltip);
	
	// AND RETURN THE COMPLETED BUTTON
        return button;
    }

    public TAController getController() {
        return controller;
    }

    public void setController(TAController controller) {
        this.controller = controller;
    }
    
    

    public boolean isListenStart() {
        return listenStart;
    }

    public void setListenStart(boolean listen) {
        this.listenStart = listen;
    }
    
    public boolean isListenEnd() {
        return listenEnd;
    }

    public void setListenEnd(boolean listen) {
        this.listenEnd = listen;
    }
    
    public boolean isListenDate() {
        return listenDate;
    }

    public void setListenDate(boolean listen) {
        this.listenDate = listen;
    }
    
    
    
    public ObservableList getStylesheetOption() {
        return stylesheetOption;
    }

    //for the course detail data
    public void setStylesheetOption(ObservableList stylesheetOption) {
        this.stylesheetOption = stylesheetOption;
    }

    public TextField getTitleField() {
        return titleField;
    }

    public void setTitleField(TextField titleField) {
        this.titleField = titleField;
    }

    public TextField getInstructionNField() {
        return instructionNField;
    }

    public void setInstructionNField(TextField instructionNField) {
        this.instructionNField = instructionNField;
    }

    public TextField getInstructionHField() {
        return instructionHField;
    }

    public void setInstructionHField(TextField instructionHField) {
        this.instructionHField = instructionHField;
    }

    public ComboBox getSubjectBox() {
        return subjectBox;
    }

    public void setSubjectBox(ComboBox subjectBox) {
        this.subjectBox = subjectBox;
    }

    public ComboBox getNumberBox() {
        return numberBox;
    }

    public void setNumberBox(ComboBox numberBox) {
        this.numberBox = numberBox;
    }

    public ComboBox getSemesterBox() {
        return semesterBox;
    }

    public void setSemesterBox(ComboBox semesterBox) {
        this.semesterBox = semesterBox;
    }

    public ComboBox getYearBox() {
        return yearBox;
    }

    public void setYearBox(ComboBox yearBox) {
        this.yearBox = yearBox;
    }

    public ComboBox getStyleCombo() {
        return styleCombo;
    }

    public void setStyleCombo(ComboBox styleCombo) {
        this.styleCombo = styleCombo;
    }
    
    
    public ImageView getYaleIV1() {
        return yaleIV1;
    }

    public void setYaleIV1(ImageView yaleIV1) {
        this.yaleIV1 = yaleIV1;
    }

    public ImageView getYaleIV2() {
        return yaleIV2;
    }

    public void setYaleIV2(ImageView yaleIV2) {
        this.yaleIV2 = yaleIV2;
    }
    
    
    public ImageView getCSIV() {
        return csIV;
    }

    public void setCSIV(ImageView csIV) {
        this.csIV = csIV;
    }
    
    
    
    
    public Label getSelectTemple() {
        return selectTemple;
    }

    public void setSelectTemple(Label selectTemple) {
        this.selectTemple = selectTemple;
    }
    
    public Label getDirLabel() {
        return dirLabel;
    }

    public void setDirLabel(Label dirLabel) {
        this.dirLabel = dirLabel;
    }
    
    
    //for the recitation data
    
    public ComboBox getSuTA1Box() {
        return suTA1Box;
    }

    public ComboBox getSuTA2Box() {    
        return suTA2Box;
    }

    public ComboBox getComboBox1() {
        return comboBox1;
    }

    public ComboBox getComboBox2() {
        return comboBox2;
    }
    public TextField getSectionField(){
        return sectionField;
    }
    public TextField getInstructorField(){
        return instructorField;
    }
   
    public TextField getDaytimeField(){
        return daytimeField;
    }
    public TextField getLocation(){
        return locationField;
    }
    public TableView getRecitationTable(){
        return recitationTable;
    }
    
    
    //for the schedule data
    public ComboBox getTypeBox(){
        return typeBox;
    }
    public DatePicker getDatePicker(){
        return datePicker;
    }
    public TextField getTimeText(){
        return timeText;
    }
    public TextField getTTextField(){
        return tTextField;
    }
    public TextField getTopicText(){
        return topicField;
    }
    public TextField getLinkField(){
        return linkField;
    }
    public TextField getCriteriaField(){
        return criteriaField;
    }
    public TableView getSiTable(){
        return siTable;
    }
  
    
    
    
    
    //for the project data
    public TextField getNF(){
        return nF;
    }
    public ColorPicker getColorPicker(){
        return colorPicker;
    }
    public ColorPicker getTextPicker(){
        return textPicker;
    }
    public TextField getLTF(){
        return lTF;
    }
    public TextField getFnTF(){
        return fnTF;
    }
    public TextField getLnTF(){
        return lnTF;
    }
    public ComboBox getTBox(){
        return tBox;
    }
    public TextField getRTF(){
        return rTF;
    }
    public TableView getTTable(){
        return tTable;
    }
    public TableView getSTable(){
        return sTable;
    }
    
    
    
    
    public DatePicker getSmPicker(){
        return smPicker;
    }
    
    public DatePicker getEfPciker(){
        return efPicker;
    }
    
    
    
    
    public ObservableList getTeacher1(){
        return techera1;
    }
    
    public ObservableList getTeamOption(){
        return teamOption;
    }
    
    public Tab getCDTab(){
        return cdTab;
    }
    public Tab getTATab(){
        return taTab;
    }
    public Tab getRDTab(){
        return rdTab;
    }
    public Tab getSDTab(){
        return sdTab;
    }
    public Tab getPDTab(){
        return pdTab;
    }
    public HBox getTAsHeaderBox() {
        return tasHeaderBox;
    }

    public Label getTAsHeaderLabel() {
        return tasHeaderLabel;
    }

    public TableView getTATable() {
        return taTable;
    }

    public HBox getAddBox() {
        return addBox;
    }

    public TextField getNameTextField() {
        return nameTextField;
    }

    public TextField getEmailTextField() {
        return emailTextField;
    }

    public Button getAddButton() {
        return addButton;
    }
    
    public Button getClearButton() {
        return clearButton;
    }

    public HBox getOfficeHoursSubheaderBox() {
        return officeHoursHeaderBox;
    }

    public Label getOfficeHoursSubheaderLabel() {
        return officeHoursHeaderLabel;
    }

    public GridPane getOfficeHoursGridPane() {
        return officeHoursGridPane;
    }

    public HashMap<String, Pane> getOfficeHoursGridTimeHeaderPanes() {
        return officeHoursGridTimeHeaderPanes;
    }

    public HashMap<String, Label> getOfficeHoursGridTimeHeaderLabels() {
        return officeHoursGridTimeHeaderLabels;
    }

    public HashMap<String, Pane> getOfficeHoursGridDayHeaderPanes() {
        return officeHoursGridDayHeaderPanes;
    }

    public HashMap<String, Label> getOfficeHoursGridDayHeaderLabels() {
        return officeHoursGridDayHeaderLabels;
    }

    public HashMap<String, Pane> getOfficeHoursGridTimeCellPanes() {
        return officeHoursGridTimeCellPanes;
    }

    public HashMap<String, Label> getOfficeHoursGridTimeCellLabels() {
        return officeHoursGridTimeCellLabels;
    }

    public HashMap<String, Pane> getOfficeHoursGridTACellPanes() {
        return officeHoursGridTACellPanes;
    }

    public HashMap<String, Label> getOfficeHoursGridTACellLabels() {
        return officeHoursGridTACellLabels;
    }
    
    public String getCellKey(Pane testPane) {
        for (String key : officeHoursGridTACellLabels.keySet()) {
            if (officeHoursGridTACellPanes.get(key) == testPane) {
                return key;
            }
        }
        return null;
    }

    public Label getTACellLabel(String cellKey) {
        return officeHoursGridTACellLabels.get(cellKey);
    }

    public Pane getTACellPane(String cellPane) {
        return officeHoursGridTACellPanes.get(cellPane);
    }
    
    public ComboBox getOfficeHour(boolean start){
        if(start)
            return comboBox1;
        return comboBox2;
    }

    public String buildCellKey(int col, int row) {
        return "" + col + "_" + row;
    }

    public String buildCellText(int militaryHour, String minutes) {
        // FIRST THE START AND END CELLS
        int hour = militaryHour;
        if (hour > 12) {
            hour -= 12;
        }
        String cellText = "" + hour + ":" + minutes;
        if (militaryHour < 12) {
            cellText += "am";
        } else {
            cellText += "pm";
        }
        return cellText;
    }

    @Override
    public void resetWorkspace() {
        // CLEAR OUT THE GRID PANE
        officeHoursGridPane.getChildren().clear();
        
        // AND THEN ALL THE GRID PANES AND LABELS
        officeHoursGridTimeHeaderPanes.clear();
        officeHoursGridTimeHeaderLabels.clear();
        officeHoursGridDayHeaderPanes.clear();
        officeHoursGridDayHeaderLabels.clear();
        officeHoursGridTimeCellPanes.clear();
        officeHoursGridTimeCellLabels.clear();
        officeHoursGridTACellPanes.clear();
        officeHoursGridTACellLabels.clear();
        stylesheetOption.clear();
        teamOption.clear();
        techera1.clear();
        subjectBox.setValue(null);
        numberBox.setValue(null);
        semesterBox.setValue(null);
        yearBox.setValue(null);
        titleField.clear();
        instructionNField.clear();
        instructionHField.clear();
       
        smPicker.setValue(null);
        efPicker.setValue(null); 
        
    }
    
    @Override
    public void reloadWorkspace(AppDataComponent dataComponent) {
        CSGData data = (CSGData)dataComponent;   //
        reloadOfficeHoursGrid(data.getTaData());     //
        reloadRecitation(data.getRdData());
        reloadSchedule(data.getSdData());
        reloadCourseDetail(data.getCdData());
        
    }
    public void reloadCourseDetail(CourseDetailData dataComponent){
        
    }
    public void reloadSchedule(ScheduleData dataComponent){
        
        dataComponent.getSchedules().clear();
    }
    public void reloadRecitation(RecitationData dataComponent){
        dataComponent.getRecitations().clear();
    }
    public void reloadOfficeHoursGrid(TAData dataComponent) {        
        ArrayList<String> gridHeaders = dataComponent.getGridHeaders();

        // ADD THE TIME HEADERS
        for (int i = 0; i < 2; i++) {
            addCellToGrid(dataComponent, officeHoursGridTimeHeaderPanes, officeHoursGridTimeHeaderLabels, i, 0);
            dataComponent.getCellTextProperty(i, 0).set(gridHeaders.get(i));
        }
        
        // THEN THE DAY OF WEEK HEADERS
        for (int i = 2; i < 7; i++) {
            addCellToGrid(dataComponent, officeHoursGridDayHeaderPanes, officeHoursGridDayHeaderLabels, i, 0);
            dataComponent.getCellTextProperty(i, 0).set(gridHeaders.get(i));            
        }
        
        // THEN THE TIME AND TA CELLS
        int row = 1;
        for (int i = dataComponent.getStartHour(); i < dataComponent.getEndHour(); i++) {
            // START TIME COLUMN
            int col = 0;
            addCellToGrid(dataComponent, officeHoursGridTimeCellPanes, officeHoursGridTimeCellLabels, col, row);
            dataComponent.getCellTextProperty(col, row).set(buildCellText(i, "00"));
            addCellToGrid(dataComponent, officeHoursGridTimeCellPanes, officeHoursGridTimeCellLabels, col, row+1);
            dataComponent.getCellTextProperty(col, row+1).set(buildCellText(i, "30"));

            // END TIME COLUMN
            col++;
            int endHour = i;
            addCellToGrid(dataComponent, officeHoursGridTimeCellPanes, officeHoursGridTimeCellLabels, col, row);
            dataComponent.getCellTextProperty(col, row).set(buildCellText(endHour, "30"));
            addCellToGrid(dataComponent, officeHoursGridTimeCellPanes, officeHoursGridTimeCellLabels, col, row+1);
            dataComponent.getCellTextProperty(col, row+1).set(buildCellText(endHour+1, "00"));
            col++;

            // AND NOW ALL THE TA TOGGLE CELLS
            while (col < 7) {
                addCellToGrid(dataComponent, officeHoursGridTACellPanes, officeHoursGridTACellLabels, col, row);
                addCellToGrid(dataComponent, officeHoursGridTACellPanes, officeHoursGridTACellLabels, col, row+1);
                col++;
            }
            row += 2;
        }

        // CONTROLS FOR TOGGLING TA OFFICE HOURS
        for (Pane p : officeHoursGridTACellPanes.values()) {
            p.setFocusTraversable(true);
            p.setOnKeyPressed(e -> {
                controller.handleKeyPress(e.getCode());
            });
            p.setOnMouseClicked(e -> {
                controller.handleCellToggle((Pane) e.getSource());
            });
            p.setOnMouseExited(e -> {
                controller.handleGridCellMouseExited((Pane) e.getSource());
            });
            p.setOnMouseEntered(e -> {
                controller.handleGridCellMouseEntered((Pane) e.getSource());
            });
        }
        
        // AND MAKE SURE ALL THE COMPONENTS HAVE THE PROPER STYLE
        TAStyle taStyle = (TAStyle)app.getStyleComponent();
        taStyle.initOfficeHoursGridStyle();
    }
    
    public void addCellToGrid(TAData dataComponent, HashMap<String, Pane> panes, HashMap<String, Label> labels, int col, int row) {       
        // MAKE THE LABEL IN A PANE
        Label cellLabel = new Label("");
        HBox cellPane = new HBox();
        cellPane.setAlignment(Pos.CENTER);
        cellPane.getChildren().add(cellLabel);

        // BUILD A KEY TO EASILY UNIQUELY IDENTIFY THE CELL
        String cellKey = dataComponent.getCellKey(col, row);
        cellPane.setId(cellKey);
        cellLabel.setId(cellKey);
        
        // NOW PUT THE CELL IN THE WORKSPACE GRID
        officeHoursGridPane.add(cellPane, col, row);
        
        // AND ALSO KEEP IN IN CASE WE NEED TO STYLIZE IT
        panes.put(cellKey, cellPane);
        labels.put(cellKey, cellLabel);
        
        // AND FINALLY, GIVE THE TEXT PROPERTY TO THE DATA MANAGER
        // SO IT CAN MANAGE ALL CHANGES
        dataComponent.setCellProperty(col, row, cellLabel.textProperty());        
    }

    @Override
    public void undo() {
        controller.Undo();
    }

    @Override
    public void redo() {
        controller.Redo();
    }
}

