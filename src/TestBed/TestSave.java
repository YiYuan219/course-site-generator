/*
 * To change this license header, choose License Headers in Team Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TestBed;

import java.io.IOException;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import tam.CSGApp;
import tam.data.CSGData;
import tam.data.CourseDetail;
import tam.data.Team;
import tam.data.Recitation;
import tam.data.Schedule;
import tam.data.Student;
import tam.data.TeachingAssistant;
import tam.file.TAFiles;

/**
 *
 * @author Yi Yuan
 */
public class TestSave {
    public static void main(String[] args) throws IOException{
        CSGApp app=new CSGApp();
        CSGData data=new CSGData();
        TAFiles file=new TAFiles(app);
        
        //for course detail
        data.getCdData().setSubject("CSE");
        data.getCdData().setNumber("219");
        data.getCdData().setSemester("Fall");
        data.getCdData().setYear("2017");
        data.getCdData().setTitle("Computer Science 3");
        data.getCdData().setInstructorName("Richard McKenna");
        data.getCdData().setInstructorHome("http://www.cs.stonybrook.edu/~richard");
        data.getCdData().setExportDir("Course\\CSE219\\Summer2017\\public");
        data.getCdData().setBanner("banner.png");
        data.getCdData().setLeft("left.png");
        data.getCdData().setRight("right.png");
        data.getCdData().setStylesheet("sea_wolf.css");
        
        
        CourseDetail c1=new CourseDetail("Home","index.html","HomeBuilder.js",true);
        data.getCdData().getCoursedetail().add(c1);
        CourseDetail c2=new CourseDetail("Syllabus","syllabus.html","SyllabusBuilder.js",true);
        data.getCdData().getCoursedetail().add(c2);
        CourseDetail c3=new CourseDetail("Schedule","schedule.html","ScheduleBuilder.js",true);
        data.getCdData().getCoursedetail().add(c3);
        CourseDetail c4=new CourseDetail("HWs","hws.html","HWsBuilder.js",true);
        data.getCdData().getCoursedetail().add(c4);
        CourseDetail c5=new CourseDetail("Projects","projects.html","ProjectsBuilder.js",false);
        data.getCdData().getCoursedetail().add(c5);
        
        //for teaching assistants
          //for the ta list
        data.getTaData().setEndHour(23);
        data.getTaData().setStartHour(0);
        TeachingAssistant ta=new TeachingAssistant("eee","eeee@eee.com",false);
        data.getTaData().getTeachingAssistants().add(ta);
        TeachingAssistant t1=new TeachingAssistant("bbb","bbbb@bbb.com",true);
        data.getTaData().getTeachingAssistants().add(t1);
        StringProperty name1=new SimpleStringProperty("eee");
        StringProperty name2=new SimpleStringProperty("bbb");
        
          //for the office hour grid
          

        
        
        //data.getTaData().getOfficeHours().put("4_4", name1);
        data.getTaData().getGridHeaders().add("Start time");
        data.getTaData().getGridHeaders().add("End time");
        data.getTaData().getGridHeaders().add("MONDAY");
        data.getTaData().getGridHeaders().add("TUESDAY");
        data.getTaData().getGridHeaders().add("WEDNESDAY");
        data.getTaData().getGridHeaders().add("THURSDAY");
        data.getTaData().getGridHeaders().add("FRIDAY");
        data.getTaData().getOfficeHours().put("0_0",new SimpleStringProperty(" Start Time"));
        data.getTaData().getOfficeHours().put("1_0",new SimpleStringProperty(" End Time"));
        data.getTaData().getOfficeHours().put("2_0",new SimpleStringProperty(" MONDAY"));
        data.getTaData().getOfficeHours().put("3_0",new SimpleStringProperty(" TUESDAY"));
        data.getTaData().getOfficeHours().put("4_0",new SimpleStringProperty(" WEDENSDAY"));
        data.getTaData().getOfficeHours().put("5_0",new SimpleStringProperty(" THURSDAY"));
        data.getTaData().getOfficeHours().put("6_0",new SimpleStringProperty(" FRIDAY"));
        for(int row=1;row<=49;row++){
            for(int col=1;col<7;col++){
                data.getTaData().getOfficeHours().put(col+"_"+row,new SimpleStringProperty(""));
            }
        }
  
        for(int row=1;row<=49;row++){
            if(row%2==0){
                String minutes="30";
                int hour=(row/2)-1;
                String s = getTimeString(hour,minutes);
                data.getTaData().getOfficeHours().put(0+"_"+row,new SimpleStringProperty(s));
            }
            else{
                String minutes="00";
                int hour=(row/2);
                String s = getTimeString(hour,minutes);
                data.getTaData().getOfficeHours().put(0+"_"+row,new SimpleStringProperty(s));
            } 
        }
        data.getTaData().getOfficeHours().put("5_4", name1);
        data.getTaData().getOfficeHours().put("5_7", name1);
        data.getTaData().getOfficeHours().put("6_15", name2);
        
        
        //for recitation
          //for the ta list
        Recitation recitation=new Recitation("R02","McKenna","Wed, 3:30pm-4:23pm","OLD CS 2114","Jane","Joe");
        data.getRdData().getRecitations().add(recitation);
        Recitation recitation1=new Recitation("R05","Banerjee","Tues, 5:30pm-6:23pm","OLD CS 2114","Jan","Jo");
        data.getRdData().getRecitations().add(recitation1);
          
  
        
        //for schedule
        data.getSdData().setStartM("1/23/2017");
        data.getSdData().setEndF("5/19/2017");
        Schedule schedule=new Schedule("Holiday","2/9/2017"," ","SNOW DAY"," ","http://funnybizblog.com/funny-stuff/"," ");
        data.getSdData().getSchedules().add(schedule);
        Schedule s=new Schedule("Lecture","2/14/2017"," ","Lecure 3","Event Programming"," "," ");
        data.getSdData().getSchedules().add(s);
        Schedule t=new Schedule("Holiday","3/13/2017"," ","Spring Break2"," "," "," ");
        data.getSdData().getSchedules().add(t);
        Schedule y=new Schedule("HW","3/27/2017"," ","HW3","UML"," "," ");
        data.getSdData().getSchedules().add(y);
        
       
        //for project
        Team project1=new Team("Atomic Comics","552211","ffffff","http://atomicomic.com");
        data.getPdData().getTeams().add(project1);
        Team pr=new Team("C4 Comics","235399","ffffff","http://c4-comics.appspot.com");
        data.getPdData().getTeams().add(pr);
        Student project2=new Student("Beau","Brummell","Atomic Comics","Lead Designer");
        Student pro3=new Student("Jane","Doe","C4 Comics","Lead Programmer");
        Student pro4=new Student("Noonian","Soong","Atomic Comics","Data Designer");
        data.getPdData().getStudents().add(project2);
        data.getPdData().getStudents().add(pro3);
        data.getPdData().getStudents().add(pro4);
        
     
        String filePath="./work/SiteSaveTest.json";
        String filePath1="./work/SData.json";
        String filePath2="./work/public_html/GridData.json";
        String filePath3="./work/Pro.json";
        String filePath4="./work/TeamStu.json";
        String filePath5="./work/re.json";
        String filePath6="./work/cdd.json";
        
        file.saveData(data, filePath);
        //file.exportSchedleData(data, filePath1);
        //file.exportTaData(data, filePath2);
        //file.exportProjectData(data, filePath3);
        //file.exportTeamAndStudentData(data, filePath4);
        //file.exportRecitationData(data, filePath5);
        file.exportCourseDetailData(data, filePath6);
        
    
    }
    
    public static String getTimeString(int militaryHour, String minutesText) {
        
        // FIRST THE START AND END CELLS
        int hour = militaryHour;
        if (hour > 12) {
            hour -= 12;
        }
        String cellText = "" + hour + ":" + minutesText;
        if (militaryHour < 12) {
            cellText += "am";
        } else {
            cellText += "pm";
        }
        return cellText;
    }
    
   
}

