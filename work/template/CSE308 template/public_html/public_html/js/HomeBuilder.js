/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* global home, syllabus, schedule, hws, project */

var subject;
var number;
var semester;
var year;
var title;




function initHome() {
    var dataFile = "./js/CourseDetailData.json";
    loadData(dataFile);
}


function loadData(jsonFile) {
    $.getJSON(jsonFile, function(json) {
        loadCourseDetail(json); 
    });
}

function loadCourseDetail(data) {
    subject=data.subject;
    number=data.number;
    semester=data.semester;
    year=data.year;
    title=data.title;
    changeBanner();
    changNavbar(data);
    changeRightImage(data);
    changeCSS(data);
    changeLeftImage(data);
    changeInstructorHome(data);
    changeInstructorName(data);
    changeBannerImage(data);
   
    
    
}


function changeBanner(){
    
    newBanner=subject+" "+number+" - "+semester+" "+year+"<br />\n"+title;
    
    $('#banner').html(newBanner);         
}

function changNavbar(data){
  
    var banner="<a href=\"http://www.stonybrook.edu\"><img id=\"bannerimage\" class=\"sbu_navbar\" alt=\"Stony Brook University\" src=\"./images/RichardMcKenna.jpg\" /></a>";
    var home=  "<a class=\"open_nav\" href=\"index.html\" id=\"home_link\">Home</a>";
    var syllabus= "<a class=\"open_nav\" href=\"syllabus.html\" id=\"syllabus_link\">Syllabus</a>";
    var schedule= "<a class=\"open_nav\" href=\"schedule.html\" id=\"schedule_link\">Schedule</a>";
    var hws="<a class=\"open_nav\" href=\"hws.html\" id=\"hws_link\">HWs</a>";
    var project="<a class=\"open_nav\" href=\"projects.html\" id=\"projects_link\">Projects</a>";
    
    $('#navbar').html(banner);    
    if(data.sitepages[0].use){
        $('#navbar').append(home);
    }
    if(data.sitepages[1].use){
        $('#navbar').append(syllabus);
    }
    if(data.sitepages[2].use){
       $('#navbar').append(schedule); 
    }
    if(data.sitepages[3].use){
       $('#navbar').append(hws); 
    }
    if(data.sitepages[4].use){
       $('#navbar').append(project); 
    }
}

function changeRightImage(data){
    var path=data.rightFooterImage;
    $('#rightimage').attr("src","./images/"+path);
}

function changeCSS(data){
    var path=data.stylesheet;
    $('#css').attr("href","./css/"+path);
}

function changeLeftImage(data){
    var path=data.leftFooterImage;
    $('#leftimage').attr("src","./images/"+path);
    
}

function changeBannerImage(data){
    var path=data.bannerSchoolImage;
    $('#bannerimage').attr("src","./images/"+path);

}

function changeInstructorHome(data){
    var home=data.instructorHome;
    $('#link').attr("href",home);

}

function changeInstructorName(data){
    var instructorname=data.instructorName;      
    var ele = document.getElementById( "link");
    ele.innerHTML = instructorname;
    
}




