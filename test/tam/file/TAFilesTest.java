/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tam.file;

import djf.components.AppDataComponent;
import java.io.IOException;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import tam.data.CSGData;

/**
 *
 * @author Yi Yuan
 */
public class TAFilesTest {
    static CSGData data;
    static TAFiles file;
    
    
    
    
    public TAFilesTest() {
    }
    
    @BeforeClass
    public static void setUpClass() throws IOException {
        data=new CSGData();
        file=new TAFiles(null);
        String filePath="./work/SiteSaveTest.json";
        file.loadData(data, filePath);
        
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of loadData method, of class TAFiles.
     */
    @Test
    public void testLoadData() throws Exception {
        //for the course detail
        assertEquals("CSE",data.getCdData().getSubject());
        assertEquals("219",data.getCdData().getNumber());
        assertEquals("Fall",data.getCdData().getSemester());
        assertEquals("2017",data.getCdData().getYear());
        assertEquals("Computer Science 3",data.getCdData().getTitle());
        assertEquals("Richard McKenna",data.getCdData().getInstructorName());
        assertEquals("http://www.cs.stonybrook.edu/~richard",data.getCdData().getInstructorHome());
        assertEquals("Course\\CSE219\\Summer2017\\public",data.getCdData().getExportDir());
        assertEquals("banner.png",data.getCdData().getBanner());
        assertEquals("left.png",data.getCdData().getLeft());
        assertEquals("right.png",data.getCdData().getRight());
        assertEquals("sea_wolf.css",data.getCdData().getStylesheet());
          //for the first site page
       
        assertEquals("Home",data.getCdData().getCoursedetail().get(0).getNavbar());
        assertEquals("index.html",data.getCdData().getCoursedetail().get(0).getFilename());
        assertEquals("HomeBuilder.js",data.getCdData().getCoursedetail().get(0).getScript());
          //for the second site page
        assertEquals("Syllabus",data.getCdData().getCoursedetail().get(1).getNavbar());
        assertEquals("syllabus.html",data.getCdData().getCoursedetail().get(1).getFilename());
        assertEquals("SyllabusBuilder.js",data.getCdData().getCoursedetail().get(1).getScript());
          //for the thirs site page
        assertEquals("Schedule",data.getCdData().getCoursedetail().get(2).getNavbar());
        assertEquals("schedule.html",data.getCdData().getCoursedetail().get(2).getFilename());
        assertEquals("ScheduleBuilder.js",data.getCdData().getCoursedetail().get(2).getScript());
          //for the fourth site page
        assertEquals("HWs",data.getCdData().getCoursedetail().get(3).getNavbar());
        assertEquals("hws.html",data.getCdData().getCoursedetail().get(3).getFilename());
        assertEquals("HWsBuilder.js",data.getCdData().getCoursedetail().get(3).getScript());
          //for the fifth site page
        assertEquals("Projects",data.getCdData().getCoursedetail().get(4).getNavbar());
        assertEquals("projects.html",data.getCdData().getCoursedetail().get(4).getFilename());
        assertEquals("ProjectsBuilder.js",data.getCdData().getCoursedetail().get(4).getScript());
        
        //for the recitation
          //for the first recitation
        assertEquals("R02",data.getRdData().getRecitations().get(0).getSection());
        assertEquals("McKenna",data.getRdData().getRecitations().get(0).getInstructor());
        assertEquals("Wed 3:30pm-4:23pm",data.getRdData().getRecitations().get(0).getDaytime());
        assertEquals("OLD CS 2114",data.getRdData().getRecitations().get(0).getLocation());
        assertEquals("Jane",data.getRdData().getRecitations().get(0).getTa1());
        assertEquals("Joe",data.getRdData().getRecitations().get(0).getTa2());
           //for the second rectation
        assertEquals("R05",data.getRdData().getRecitations().get(1).getSection());
        assertEquals("Banerjee",data.getRdData().getRecitations().get(1).getInstructor());
        assertEquals("Tues 5:30pm-6:23pm",data.getRdData().getRecitations().get(1).getDaytime());
        assertEquals("OLD CS 2114",data.getRdData().getRecitations().get(1).getLocation());
        assertEquals("Jan",data.getRdData().getRecitations().get(1).getTa1());
        assertEquals("Jo",data.getRdData().getRecitations().get(1).getTa2());
        
        //for the schedule
          //for the first schedule
        assertEquals("Holiday",data.getSdData().getSchedules().get(0).getType());
        assertEquals("2/9/2017",data.getSdData().getSchedules().get(0).getDate());
        assertEquals(" ",data.getSdData().getSchedules().get(0).getTime());
        assertEquals("SNOW DAY",data.getSdData().getSchedules().get(0).getTitle());
        assertEquals(" ",data.getSdData().getSchedules().get(0).getTopic());
        assertEquals("http://funnybizblog.com/funny-stuff/",data.getSdData().getSchedules().get(0).getLink());
        assertEquals(" ",data.getSdData().getSchedules().get(0).getCriteria());
          //for the second schedule
        assertEquals("Lecture",data.getSdData().getSchedules().get(1).getType());
        assertEquals("2/14/2017",data.getSdData().getSchedules().get(1).getDate());
        assertEquals(" ",data.getSdData().getSchedules().get(1).getTime());
        assertEquals("Lecure 3",data.getSdData().getSchedules().get(1).getTitle());
        assertEquals("Event Programming",data.getSdData().getSchedules().get(1).getTopic());
        assertEquals(" ",data.getSdData().getSchedules().get(1).getLink());
        assertEquals(" ",data.getSdData().getSchedules().get(1).getCriteria());
          //for the third schedule
        assertEquals("Holiday",data.getSdData().getSchedules().get(2).getType());
        assertEquals("3/13/2017",data.getSdData().getSchedules().get(2).getDate());
        assertEquals(" ",data.getSdData().getSchedules().get(2).getTime());
        assertEquals("Spring Break2",data.getSdData().getSchedules().get(2).getTitle());
        assertEquals(" ",data.getSdData().getSchedules().get(2).getTopic());
        assertEquals(" ",data.getSdData().getSchedules().get(2).getLink());
        assertEquals(" ",data.getSdData().getSchedules().get(2).getCriteria());
          //for the fourth schedule
        assertEquals("HW",data.getSdData().getSchedules().get(3).getType());
        assertEquals("3/27/2017",data.getSdData().getSchedules().get(3).getDate());
        assertEquals(" ",data.getSdData().getSchedules().get(3).getTime());
        assertEquals("HW3",data.getSdData().getSchedules().get(3).getTitle());
        assertEquals("UML",data.getSdData().getSchedules().get(3).getTopic());
        assertEquals(" ",data.getSdData().getSchedules().get(3).getLink());
        assertEquals(" ",data.getSdData().getSchedules().get(3).getCriteria());
        
        //for the project
          //for the first teams
        assertEquals("Atomic Comics",data.getPdData().getTeams().get(0).getName());
        assertEquals("552211",data.getPdData().getTeams().get(0).getColor());
        assertEquals("ffffff",data.getPdData().getTeams().get(0).getTextcolor());
        assertEquals("http://atomicomic.com",data.getPdData().getTeams().get(0).getLink());
          //for the second teams
        assertEquals("C4 Comics",data.getPdData().getTeams().get(1).getName());
        assertEquals("235399",data.getPdData().getTeams().get(1).getColor());
        assertEquals("ffffff",data.getPdData().getTeams().get(1).getTextcolor());
        assertEquals("http://c4-comics.appspot.com",data.getPdData().getTeams().get(1).getLink());
          
          //for the first students
        assertEquals("Beau",data.getPdData().getStudents().get(0).getFirstname());
        assertEquals("Brummell",data.getPdData().getStudents().get(0).getLastname());
        assertEquals("Atomic Comics",data.getPdData().getStudents().get(0).getTeam());
        assertEquals("Lead Designer",data.getPdData().getStudents().get(0).getRole());
          //for the decond studen
        assertEquals("Jane",data.getPdData().getStudents().get(1).getFirstname());
        assertEquals("Doe",data.getPdData().getStudents().get(1).getLastname());
        assertEquals("C4 Comics",data.getPdData().getStudents().get(1).getTeam());
        assertEquals("Lead Programmer",data.getPdData().getStudents().get(1).getRole());
          //for the third students
        assertEquals("Noonian",data.getPdData().getStudents().get(2).getFirstname());
        assertEquals("Soong",data.getPdData().getStudents().get(2).getLastname());
        assertEquals("Atomic Comics",data.getPdData().getStudents().get(2).getTeam());
        assertEquals("Data Designer",data.getPdData().getStudents().get(2).getRole());
        
        
    }

    /**
     * Test of saveData method, of class TAFiles.
     */
    @Test
    public void testSaveData() throws Exception {
        
    }

    /**
     * Test of importData method, of class TAFiles.
     */
    @Test
    public void testImportData() throws Exception {
        
    }

    /**
     * Test of exportData method, of class TAFiles.
     */
    @Test
    public void testExportData() throws Exception {
        
    }
    
}
